import { ChatActions } from '../actions/payloads/chatActions';

const initialState = {
    lobby: {
        messages: [],
        textInput: '',
    },
};

export default function(state = initialState, action){
    switch (action.type){
    case ChatActions.ON_LOBBY_CHAT_INPUT_UPDATE:
        return {
            ...state,
            lobby: updateTextInput(state.lobby, action.payload),
        };
    case ChatActions.ON_LOBBY_CHAT_LOAD:
        return {
            ...state,
            lobby: updateMessageHistory(state.lobby, action.payload),
        };
    case ChatActions.ON_NEW_LOBBY_MESSAGE:
        return {
            ...state,
            lobby: updateNewMessage(state.lobby, action.payload),
        };

    default:
        return state;
    }
}

function updateTextInput(chatState, newTextInputValue){
    return {
        ...chatState,
        textInput: newTextInputValue,
    };
}

function updateMessageHistory(chatState, messageHistory){
    return {
        ...chatState,
        messages: messageHistory,
    };
}

function updateNewMessage(chatState, newMessage){
    return {
        ...chatState,
        messages: [
            ...chatState.messages,
            newMessage,
        ],
    };
}
