import { ProfileActions } from '../actions/payloads/profileActions';
import { SessionActions } from '../actions/payloads/sessionActions';

const initialState = {
    isLoggedIn: false,
    isWebSocketConnected: false,
    userID: undefined,
    userName: '',
};

export default function sessionReducer(state = initialState, { type, payload }){
    switch (type){
    case ProfileActions.UPDATE_PROFILE:
        return {
            ...state,
            userID: payload.userID,
            userName: payload.userName,
        };
    case SessionActions.ON_FIREBASE_USER_UPDATE:
        return {
            ...state,
            isLoggedIn: payload,
        };
    case SessionActions.ON_WEBSOCKET_UPDATE:
        return {
            ...state,
            isWebSocketConnected: payload,
        };
    case SessionActions.ON_USER_UPDATE:
        return {
            ...state,
            userID: payload.id,
            userName: payload.name,
        };

    default:
        return state;
    }
}
