import { keyBy } from 'lodash';
import { SetupActions } from '../actions/payloads/setupActions';

const initialState = {
    factionMap: {},
    factionRoleMap: {},
    hiddenMap: {},
    modifiers: getDefaultModifiers(),
    setupHiddens: [],
};

function onHiddenSpawnDelete(state, { hiddenID, hiddenSpawnID }){
    const oldHidden = state.hiddenMap[hiddenID];
    const updatedHidden = {
        ...oldHidden,
        spawns: oldHidden.spawns.filter(sp => sp.id !== hiddenSpawnID),
    };
    return {
        ...state,
        hiddenMap: {
            ...state.hiddenMap,
            [hiddenID]: updatedHidden,
        },
    };
}

export default function setupReducer(state = initialState, { type, payload }){
    switch (type){
    case SetupActions.ON_FACTION_ABILITY_CREATE:
        return {
            ...state,
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: {
                    ...state.factionMap[payload.factionID],
                    abilities: [
                        ...state.factionMap[payload.factionID].abilities,
                        payload.ability,
                    ],
                },
            },
        };
    case SetupActions.ON_FACTION_ABILITY_DELETE:
        return {
            ...state,
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: {
                    ...state.factionMap[payload.factionID],
                    abilities: state.factionMap[payload.factionID].abilities
                        .filter(ability => ability.id !== payload.abilityID),
                },
            },
        };
    case SetupActions.ON_FACTION_CREATE:
        return {
            ...state,
            factionMap: {
                ...state.factionMap,
                [payload.id]: payload,
            },
        };
    case SetupActions.ON_FACTION_DELETE:
        return onFactionDelete(state, payload);
    case SetupActions.ON_FACTION_ENEMY_ADD:
        return {
            ...state,
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: addEnemyID(
                    state.factionMap, payload.factionID, payload.enemyFactionID,
                ),
                [payload.enemyFactionID]: addEnemyID(
                    state.factionMap, payload.enemyFactionID, payload.factionID,
                ),
            },
        };
    case SetupActions.ON_FACTION_ENEMY_DELETE:
        return {
            ...state,
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: deleteEnemyID(
                    state.factionMap, payload.factionID, payload.enemyFactionID,
                ),
                [payload.enemyFactionID]: deleteEnemyID(
                    state.factionMap, payload.enemyFactionID, payload.factionID,
                ),
            },
        };
    case SetupActions.ON_FACTION_MODIFIER_UPDATE:
        return {
            ...state,
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: {
                    ...state.factionMap[payload.factionID],
                    modifiers: state.factionMap[payload.factionID].modifiers.map(modifier => {
                        if(modifier.name !== payload.name)
                            return { ...modifier };
                        return {
                            ...modifier,
                            value: payload.value,
                        };
                    }),
                },
            },
        };
    case SetupActions.ON_FACTION_ROLE_CREATE:
        return {
            ...state,
            factionRoleMap: {
                ...state.factionRoleMap,
                [payload.id]: payload,
            },
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: {
                    ...state.factionMap[payload.factionID],
                    factionRoles: [
                        ...state.factionMap[payload.factionID].factionRoles,
                        payload,
                    ],
                },
            },
        };
    case SetupActions.ON_FACTION_ROLE_DELETE:
        return onFactionRoleDelete(state, payload);
    case SetupActions.ON_FACTION_ROLE_UPDATE:
        return {
            ...state,
            factionRoleMap: {
                ...state.factionRoleMap,
                [payload.id]: payload,
            },
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: {
                    ...state.factionMap[payload.factionID],
                    factionRoles: state.factionMap[payload.factionID].factionRoles
                        .map(factionRole => (factionRole.id === payload.id
                            ? payload : factionRole)),
                },
            },
        };
    case SetupActions.ON_FACTION_UPDATE:
        return {
            ...state,
            factionMap: {
                ...state.factionMap,
                [payload.factionID]: {
                    ...state.factionMap[payload.factionID],
                    description: payload.description,
                    name: payload.name,
                },
            },
        };
    case SetupActions.ON_HIDDEN_CREATE:
        return {
            ...state,
            hiddenMap: {
                ...state.hiddenMap,
                [payload.id]: payload,
            },
        };
    case SetupActions.ON_HIDDEN_SPAWNS_CREATE:
        return {
            ...state,
            hiddenMap: {
                ...state.hiddenMap,
                ...payload.reduce((acc, { hiddenID, ...spawn }) => ({
                    ...acc,
                    [hiddenID]: {
                        ...state.hiddenMap[hiddenID],
                        spawns: [
                            ...state.hiddenMap[hiddenID].spawns,
                            spawn,
                        ],
                    },
                }), {}),
            },
        };
    case SetupActions.ON_HIDDEN_SPAWN_DELETE:
        return onHiddenSpawnDelete(state, payload);
    case SetupActions.ON_SETUP_CHANGE:
        return {
            ...payload,
        };
    case SetupActions.ON_SETUP_HIDDEN_ADD:
        return {
            ...state,
            setupHiddens: [
                ...state.setupHiddens.filter(sh => sh.id !== payload.id),
                payload,
            ],
        };
    case SetupActions.ON_SETUP_HIDDEN_REMOVE:
        return {
            ...state,
            setupHiddens: state.setupHiddens
                .filter(setupHidden => setupHidden.id !== payload),
        };
    case SetupActions.ON_SETUP_MODIFIER_UPDATE:
        return {
            ...state,
            modifiers: state.modifiers.map(modifier => (
                isSameModifier(modifier, payload)
                    ? { ...modifier, value: payload.value }
                    : modifier)),
        };
    default:
        return state;
    }
}

function getDefaultModifiers(){
    return [{
        name: 'SELF_VOTE',
        value: false,
    }];
}

function isSameModifier(modifier, payload){
    return modifier.name === payload.name;
}

function onFactionRoleDelete(state, factionRoleID){
    const newFactionRoleMap = { ...state.factionRoleMap };
    const oldFactionRole = newFactionRoleMap[factionRoleID];
    delete newFactionRoleMap[factionRoleID];
    return {
        ...state,
        factionRoleMap: newFactionRoleMap,
        factionMap: {
            ...state.factionMap,
            [oldFactionRole.factionID]: {
                ...state.factionMap[oldFactionRole.factionID],
                factionRoles: state.factionMap[oldFactionRole.factionID].factionRoles
                    .filter(factionRole => factionRole.id !== factionRoleID),
            },
        },
        hiddenMap: Object.fromEntries(
            Object.entries(state.hiddenMap)
                .map(([hiddenID, hidden]) => [
                    hiddenID, filterOutFactionRoleID(hidden, factionRoleID)]),
        ),
    };
}

function onFactionDelete(state, deletedFactionID){
    const noDeletedFactionID = factionID => factionID !== deletedFactionID;
    const factions = Object.values(state.factionMap)
        .filter(({ id }) => deletedFactionID !== id)
        .map(f => ({
            ...f,
            checkableIDs: f.checkableIDs.filter(noDeletedFactionID),
            enemyIDs: f.enemyIDs.filter(noDeletedFactionID),
        }));
    return {
        ...state,
        factionMap: keyBy(factions, 'id'),
        factionRoleMap: keyBy(
            Object.values(state.factionRoleMap).filter(fr => fr.factionID !== deletedFactionID),
            'id',
        ),
        hiddenMap: getHiddenMapWithDeletedFaction(state, deletedFactionID),
    };
}

function addEnemyID(factionMap, factionID, enemyFactionID){
    const faction = factionMap[factionID];
    return {
        ...faction,
        enemyIDs: [
            ...faction.enemyIDs,
            enemyFactionID,
        ],
    };
}

function deleteEnemyID(factionMap, factionID, enemyFactionID){
    const faction = factionMap[factionID];
    return {
        ...faction,
        enemyIDs: faction.enemyIDs.filter(fID => fID !== enemyFactionID),
    };
}

function filterOutFactionRoleID(hidden, factionRoleID){
    return {
        ...hidden,
        spawns: hidden.spawns.filter(spawn => spawn.factionRoleID !== factionRoleID),
    };
}

function getHiddenMapWithDeletedFaction(state, factionID){
    const hiddens = Object.values(state.hiddenMap).map(hidden => ({
        ...hidden,
        spawns: hidden.spawns
            .filter(spawn => state.factionRoleMap[spawn.factionRoleID].factionID !== factionID),
    }));
    return keyBy(hiddens, 'id');
}
