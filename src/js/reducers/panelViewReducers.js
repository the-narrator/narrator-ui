import { PanelViewActions } from '../actions/payloads/panelViewActions';
import { SetupActions } from '../actions/payloads/setupActions';

import { PANEL_VIEWS, VOTE_SORT_TYPES } from '../util/constants';


const initialState = {
    entityID: null,
    entityType: null,
    factionID: undefined,

    voteSort: VOTE_SORT_TYPES.ALPHA,
};

export default function panelViewReducer(state = initialState, action){
    switch (action.type){
    case PanelViewActions.ON_HIDE_ENTITY:
        if(state.entityType === PANEL_VIEWS.FACTION_ROLE)
            return {
                ...state,
                entityID: state.factionID,
                entityType: PANEL_VIEWS.FACTION,
            };
        if(state.entityType === PANEL_VIEWS.HIDDEN)
            return {
                ...state,
                entityID: null,
                entityType: PANEL_VIEWS.HIDDEN,
            };
        return {
            ...state,
            entityID: null,
            entityType: null,
        };
    case PanelViewActions.ON_SHOW_FACTION_ROLE:
        return {
            ...state,
            entityID: action.payload.factionRoleID,
            entityType: PANEL_VIEWS.FACTION_ROLE,
            factionID: action.payload.factionID,
        };
    case PanelViewActions.ON_SHOW_HIDDEN:
        return {
            ...initialState,
            entityID: action.payload,
            entityType: PANEL_VIEWS.HIDDEN,
        };
    case PanelViewActions.ON_SHOW_FACTION:
        return {
            ...initialState,
            entityType: PANEL_VIEWS.FACTION,
            entityID: action.payload,
        };
    case PanelViewActions.ON_SHOW_PLAYER:
        return {
            ...initialState,
            entityID: action.payload,
            entityType: PANEL_VIEWS.PLAYER,
        };
    case PanelViewActions.ON_VOTE_SORT:
        return {
            ...state,
            voteSort: action.payload,
        };
    case SetupActions.ON_SETUP_CHANGE:
        return handleOnSetupChange(state, action.payload);
    case SetupActions.ON_FACTION_ROLE_DELETE:
        return handleFactionRoleDelete(state, action.payload);
    case SetupActions.ON_FACTION_DELETE:
        return initialState;
    default:
        return state;
    }
}

function handleOnSetupChange(state, newSetup){
    if(state.entityType === PANEL_VIEWS.FACTION && !newSetup.factionMap[state.entityID])
        return {
            ...state,
            entityID: initialState.entityID,
            entityType: initialState.entityType,
        };
    if(state.entityType === PANEL_VIEWS.FACTION_ROLE && !newSetup.factionRoleMap[state.entityID]){
        if(newSetup.factionMap[state.factionID])
            return {
                ...state,
                entityType: PANEL_VIEWS.FACTION,
                entityID: state.factionID,
            };
        return {
            ...state,
            entityID: initialState.entityID,
            entityType: initialState.entityType,
        };
    }
    if(state.entityType === PANEL_VIEWS.HIDDEN && !newSetup.hiddenMap[state.entityID])
        return {
            ...state,
            entityID: initialState.entityID,
            entityType: initialState.entityType,
        };
    return state;
}

function handleFactionRoleDelete(state, factionRoleID){
    if(state.entityType !== PANEL_VIEWS.FACTION_ROLE || state.entityID !== factionRoleID)
        return state;
    return {
        ...state,
        entityType: PANEL_VIEWS.FACTION,
        entityID: state.factionID,
    };
}
