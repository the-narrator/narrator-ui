/* eslint-env browser */
import thunk from 'redux-thunk';

import { applyMiddleware, combineReducers, createStore } from 'redux';
import actionDialogState from './actionDialogReducers';
import chatState from './chatReducers';
import gameState from './gameReducers';
import lobbiesState from './lobbiesReducers';
import panelViewState from './panelViewReducers';
import profileState from './profileReducers';
import session from './sessionReducers';
import setupState from './setupReducers';
import setupsState from './setupsReducers';

import condorcet from '../condorcet/condorcetReducer';


const appState = combineReducers({
    actionDialogState,
    chatState,
    gameState,
    lobbiesState,
    panelViewState,
    profileState,
    session,
    setupState,
    setupsState,

    rankings: condorcet,
});

export default createStore(
    appState, applyMiddleware(thunk),
);
