import { LobbyActions } from '../actions/payloads/lobbyActions';


const initialState = {
    lobbies: [],
};

export default function lobbiesReducer(state = initialState, { type, payload }){
    switch (type){
    case LobbyActions.ON_LOBBY_CREATE:
        return {
            ...state,
            lobbies: [
                ...state.lobbies,
                payload,
            ],
        };
    case LobbyActions.ON_LOBBY_GET:
        return {
            ...state,
            lobbies: payload,
        };
    case LobbyActions.ON_LOBBY_DELETE:
        return {
            ...state,
            lobbies: state.lobbies.filter(lobby => lobby.id !== payload),
        };
    default:
        return state;
    }
}
