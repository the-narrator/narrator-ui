import React from 'react';
import { VOTE_SORT_TYPES } from '../../util/constants';


const voteSortDefinitions = [{
    voteSortType: VOTE_SORT_TYPES.ALPHA,
    className: 'alpha-up',
}, {
    voteSortType: VOTE_SORT_TYPES.VOTE_COUNT,
    className: 'amount-up',
}];

export default function VoteCountSorter({ voteSort, setVoteSort }){
    return (
        <div className="voteCountSortContainer">
            {
                voteSortDefinitions.map(definition => (
                    <i
                        className={getClassName(definition, voteSort)}
                        key={definition.voteSortType}
                        onClick={() => setVoteSort(definition.voteSortType)}
                    />
                ))
            }
        </div>
    );
}

function getClassName(definition, voteSort){
    const baseClassName = `voteCountSorter fas fa-sort-${definition.className}`;
    if(voteSort !== definition.voteSortType)
        return baseClassName;
    return `${baseClassName} selectedVoteSorter`;
}
