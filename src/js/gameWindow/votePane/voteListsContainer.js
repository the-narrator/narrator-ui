import React from 'react';

import { getVoteAction, getVoteCount } from './voteUtils';
import { NOT_VOTING } from '../../util/constants';
import VotersListContainer from './votersListContainer';


export default function VoteListsContainer({
    playerName, voters, onVoteSubmit, onUnvoteSubmit, canSelfVote, profile,
}){
    const voteCount = getVoteCount(playerName, voters);
    return (
        <div className="voteListContainer">
            <div className="voteListHeaderContainer">
                <div className="voteListActionContainer">
                    <span className="voteListCount">{voteCount}</span>
                </div>
                <span className="voteListHeaderTitle">{playerName}</span>
            </div>
            <hr className="votersSeparator" />
            {voters.length
                ? (
                    <VotersListContainer
                        onUnvoteSubmit={onUnvoteSubmit}
                        profileName={profile.name}
                        voters={voters}
                        votedName={playerName}
                    />
                )
                : (<span className="voteNoVoters">No voters</span>)}
            {canAddVote(playerName, profile, canSelfVote) && (
                <>
                    <hr className="votersSeparator voteButtonSeparator" />
                    <div className="voteAddContainer">
                        <div className="voteListActionContainer">
                            <i className="far fa-square voteCheckableIcon voteIcon" />
                        </div>
                        <span
                            className="voteAddText voteTrialButton"
                            onClick={onVoteSubmit}
                        >
                            Add vote
                        </span>
                    </div>
                </>
            )}
        </div>
    );
}

function canAddVote(playerName, profile, canSelfVote){
    if(playerName === NOT_VOTING)
        return false;
    const voteAction = getVoteAction(profile);
    const isVotingForPlayer = voteAction && voteAction.playerNames.includes(playerName);
    if(isVotingForPlayer)
        return false;
    if(canSelfVote)
        return true;
    return profile.name !== playerName;
}
