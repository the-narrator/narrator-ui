import { get } from 'lodash';

import { NOT_VOTING, VOTE_VIEW_TYPES } from '../../util/constants';


export function getVoteAction(profile){
    const actions = get(profile, ['roleCard', 'actions'], []);
    return actions.find(action => action.command.toLowerCase() === 'vote');
}

export function getVoteCount(playerName, voters){
    return playerName === NOT_VOTING
        ? voters.length
        : voters.reduce((acc, voter) => (voter.status === VOTE_VIEW_TYPES.PENDING_CONFIRM
            ? 0
            : voter.votePower) + acc, 0);
}
