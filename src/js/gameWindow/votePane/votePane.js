import React from 'react';
import { connect } from 'react-redux';

import VoteCountSorter from './voteCountSorter';
import VoteListsContainer from './voteListsContainer';
import VotePaneSubheaderPopout from './votePaneSubheaderPopout';
import { getVoteAction, getVoteCount } from './voteUtils';

import { onVoteSortAction } from '../../actions/payloads/panelViewActions';
import { onVoteDeleteAction } from '../../actions/payloads/gameActions';

import {
    NOT_VOTING,
    VOTE_SORT_TYPES,
    VOTE_SYSTEM_PLURALITY,
    VOTE_VIEW_TYPES,
} from '../../util/constants';

import { submit as submitAction, cancel as cancelAction } from '../../services/actionService';
import { getGameModifierValue, getSetupModifierValue } from '../../util/modifierUtil';


function VotePane({
    playerMap, phase, profile, voteSort, setVoteSort, voteInfo, deleteVote, canSelfVote,
    voteSystemType, trialLength,
}){
    const votedToVoters = [
        ...voteInfo.allowedTargets.map(playerName => ({
            playerName,
            voters: getVotersOf(
                voteInfo.voterToVotes, playerName, profile,
            ),
        })),
        { playerName: NOT_VOTING, voters: getNotVotingPlayers(playerMap, voteInfo) },
    ].map(voteTally => mapVotePower(voteTally, playerMap));
    votedToVoters.sort((v1, v2) => arraySortFunc(v1, v2, voteSort));
    return (
        <>
            <span className="voteMainHeader">{getHeaderText(voteInfo, trialLength)}</span>
            <VotePaneSubheaderPopout
                phase={phase}
                playerMap={playerMap}
                trialedPlayerName={voteInfo.trialedPlayerName}
            />
            <VoteCountSorter setVoteSort={setVoteSort} voteSort={voteSort} />
            <div className="voteListContainers">
                {
                    votedToVoters.map(({ playerName, voters }) => (
                        <VoteListsContainer
                            canSelfVote={canSelfVote}
                            key={playerName}
                            playerName={playerName}
                            profile={profile}
                            onVoteSubmit={() => submitVote(profile, playerName, voteSystemType)}
                            onUnvoteSubmit={
                                () => submitCancelVote(profile, playerName,
                                    deleteVote,
                                    voteInfo.trialedPlayerName)
                            }
                            voters={voters}
                        />
                    ))
                }
            </div>
        </>
    );
}

const mapStateToProps = ({
    panelViewState, gameState, profileState, setupState,
}) => ({
    canSelfVote: getSetupModifierValue('SELF_VOTE', setupState, false),
    playerMap: gameState.playerMap,
    phase: gameState.phase,
    profile: profileState,
    trialLength: getGameModifierValue('TRIAL_LENGTH', gameState, 0),
    voteInfo: gameState.voteInfo,
    voteSort: panelViewState.voteSort,
    voteSystemType: getGameModifierValue('VOTE_SYSTEM', gameState, false),
});

const mapDispatchToProps = {
    setVoteSort: onVoteSortAction,
    deleteVote: onVoteDeleteAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(VotePane);

function getHeaderText(voteInfo, trialLength){
    if(voteInfo.trialedPlayerName)
        return `${voteInfo.trialedPlayerName} is on trial!`;
    if(trialLength > 0)
        return 'Votes for Trial';
    return 'Votes for Elimination';
}

function getNotVotingPlayers(playerMap, voteInfo){
    const { voterToVotes } = voteInfo;
    const votingPlayers = Object.keys(voterToVotes).filter(voter => voterToVotes[voter].length);
    return Object.values(playerMap)
        .filter(player => !votingPlayers.includes(player.name) && !player.flip);
}

function mapVotePower({ playerName, voters }, playerMap){
    return {
        playerName,
        voters: voters
            .map(voter => ({
                name: voter.name,
                status: voter.status,
                votePower: playerMap[voter.name].votePower,
            })),
    };
}

function arraySortFunc(voteTally1, voteTally2, voteSort){
    const playerName1 = voteTally1.playerName;
    const playerName2 = voteTally2.playerName;
    if(playerName1 === NOT_VOTING)
        return 1;
    if(playerName2 === NOT_VOTING)
        return -1;
    const voteCount1 = getVoteCount(playerName1, voteTally1.voters);
    const voteCount2 = getVoteCount(playerName2, voteTally2.voters);
    if(voteSort === VOTE_SORT_TYPES.ALPHA || voteCount1 === voteCount2)
        return playerName1.localeCompare(playerName2);
    return voteCount2 - voteCount1;
}

function getVotersOf(voterToVotes, votedPlayerName, profile){
    const voters = Object.entries(voterToVotes)
        .filter(([, votedPlayerNames]) => votedPlayerNames.includes(votedPlayerName))
        .map(([voterName]) => voterName);
    const publicVotes = Array.from(new Set(voters));
    const voteAction = getVoteAction(profile) || { playerNames: [] };
    const inputtedVotes = voteAction.playerNames.includes(votedPlayerName) ? [profile.name] : [];
    const totalVotes = Array.from(new Set([...publicVotes, ...inputtedVotes]));
    return totalVotes.map(voterName => ({
        name: voterName,
        status: getVoteStatus(voterName, publicVotes, inputtedVotes, [profile.name]),
    }));
}

function getVoteStatus(voterName, publicVotes, inputtedVotes, controlledVoters){
    if(publicVotes.includes(voterName)){
        if(inputtedVotes.includes(voterName) || !controlledVoters.includes(voterName))
            return VOTE_VIEW_TYPES.IN_SYNC;
        return VOTE_VIEW_TYPES.PENDING_CANCEL;
    }
    return VOTE_VIEW_TYPES.PENDING_CONFIRM;
}

function submitVote(profile, playerName, voteSystemType){
    const targets = getVoteTargets(profile, playerName, voteSystemType);
    submitAction({
        command: 'vote',
        targets,
    });
}

function getVoteTargets(profile, playerName, voteSystemType){
    const priorVoteAction = getVoteAction(profile);
    if(VOTE_SYSTEM_PLURALITY === voteSystemType || !priorVoteAction)
        return [playerName];
    return [...priorVoteAction.playerNames, playerName];
}

async function submitCancelVote(profile, playerName, deleteVote, trialedPlayerName){
    const priorVoteAction = getVoteAction(profile);
    const targets = priorVoteAction.playerNames.filter(name => name !== playerName);
    if(targets.length){
        submitAction({
            command: 'vote',
            targets,
        });
    }else{
        await cancelAction(0, 'vote');
        if(!trialedPlayerName)
            deleteVote(profile.name);
    }
}
