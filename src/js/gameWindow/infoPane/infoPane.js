import React from 'react';
import { connect } from 'react-redux';

import SetupHiddensContainer
    from '../../lobbyWindow/components/setupEditorPane/setupHiddensContainer';
import GameDetails from './gameDetails';
import GraveyardContainer from './graveyard/graveyardContainer';
import {
    onShowFactionRoleAction,
    onShowHiddenAction,
} from '../../actions/payloads/panelViewActions';


function InfoPane({
    setupState, gameState, showFactionRole, showHidden,
}){
    return (
        <>
            <div className="gameEntitiesContainer">
                <SetupHiddensContainer
                    className="darkenedSetupPanel"
                    canRemoveSetupHiddens={false}
                    titleText="Roles List"
                    playerCount={Object.values(gameState.playerMap).length}
                    setup={setupState}
                    showFactionRole={showFactionRole}
                    showHidden={showHidden}
                />
                <GraveyardContainer />
            </div>
            <GameDetails />
        </>
    );
}

const mapStateToProps = ({ setupState, gameState }) => ({
    setupState,
    gameState,
});

const mapDispatchToProps = {
    showFactionRole: onShowFactionRoleAction,
    showHidden: onShowHiddenAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoPane);
