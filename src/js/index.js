import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import { BrowserRouter } from 'react-router-dom';

import store from './reducers/store';
import App from './App';

import InfoPane from './gameWindow/infoPane/infoPane';
import VotePane from './gameWindow/votePane/votePane';

import { onHideEntityAction } from './actions/payloads/panelViewActions';
import { onLobbyLeaveAction } from './actions/payloads/profileActions';

let socketConnectingPromise = null;

render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
);

render(
    <Provider store={store}>
        <InfoPane />
    </Provider>,
    document.getElementById('game_info_pane'),
);

render(
    <Provider store={store}>
        <VotePane />
    </Provider>,
    document.getElementById('vote_recap_pane'),
);

/* eslint-env jquery, browser */
/* global SimpleBar */

const helpers = require('./util/browserHelpers');
const requests = require('./util/browserRequests');

const actionService = require('./services/actionService');
const chatService = require('./services/chatService');
const gameService = require('./services/gameService');
const gameUserService = require('./services/gameUserService');
const phaseService = require('./services/phaseService');
const playerService = require('./services/playerService');
const setupService = require('./services/setupService');
const soundService = require('./services/soundService');
const storageService = require('./services/storageService');

const chatView = require('./views/chatView');
const playerView = require('./views/playerView');
const roleInfoView = require('./views/roleInfoView');

const { ArrayList } = require('./util/arrayList');
const { GameState } = require('./models/gameState');
const roleImages = require('./util/roleImages');
const { Targetable } = require('./util/targetable');
const selectOptions = require('./util/selectOptions');

const NORM_TEXT_COLOR = '#F2F2F2';

const UNIQUE = true;
let user = null;

let right = null;
let left = null;
let savedOpts = [null, null, null];
let selected = null;
let selected2 = null; // for jailors left and right selected lists
let deadSelected = null;
let currentAction = null;

const seenFeedbacks = [];

const J = {
    HTML_IDS: {
        TOP_SELECT: 'teamSelect',
        MID_SELECT: 'midSelect',
        BOT_SELECT: 'tailor_spinner',
    },
};

const SETTINGS_KEYS = ['narration_enabled', 'chat_ping', 'music_enabled', 'day_music_enabled',
    'night_music_enabled',
];

function resetGameState(){
    window.gameState = new GameState();

    $('#chat_tabs').empty();
    $('#chat_ticker_new_message').text('');
    $('#chat_ticker_expander i').removeClass('unread_ticker');
    $('#last_will_area').val('');
    $('#last_will_area').hide();
    $('#last_will_button').text('Last Will');
    $('.last_will_item').css('display', ''); // when you die, i hide it in the graveyard, and changing the class later doesn't unhide it.

    window.gameState.warning = Promise.resolve();
    window.gameState.speech = Promise.resolve();

    if(!helpers.storageEnabled())
        SETTINGS_KEYS.forEach(value => {
            window.gameState[value] = false;
        });
}

resetGameState();

function isOnMain(){
    return $('#main').is(':visible');
}

function isTablet(){
    return $('#tablet').css('color') === 'rgb(255, 165, 0)';
}

(function($){
    $.fn.hasScrollBar = function(){
        return this.get(0).scrollWidth > this.width();
    };
}(jQuery));

function setRead(e){
    pushServerUnread(e.attr('name'));
    if(!getUnread())
        $('#chat_ticker_expander i').removeClass('unread_ticker');
    e.removeClass('unread_ticker');
}

function pushServerUnread(newLogName){
    webSend({
        message: 'setReadChat',
        setReadChat: newLogName,
    });
    window.gameState.unreadChats[newLogName] = 0;
}

function getPuppets(){
    if(!window.gameState.role || !window.gameState.role.puppets)
        return [];
    return window.gameState.role.puppets.map(puppetName => puppetName.replace(/ vote/ig, ''));
}

function hasPuppets(){
    return getPuppets().length;
}

function onCurrentDayChat(){
    return window.gameState.chatKeys[$('.activeTab').attr('name')] === 'everyone';
}

function getPuppetList(){
    const puppets = getPuppets();
    if(window.gameState.chatKeys[`Day ${window.gameState.dayNumber}`] === 'everyone')
        puppets.push(window.gameState.role.displayName);

    return puppets;
}

function getVisiblePuppetList(){
    const vPuppets = [];
    $('#puppetList li').each(function(){
        vPuppets.push($(this).text());
    });

    return vPuppets;
}

function puppetChangeClick(e){
    e = $(this);
    e.parent().hide();
    if(e.hasClass('selectedPuppet'))
        return;
    $('.selectedPuppet').removeClass('selectedPuppet');

    e.addClass('selectedPuppet');
    setPuppetTalk(e.text());
}

function reloadPuppetChoices(){
    const chatName = $('.activeTab').attr('name');

    const vPuppets = getVisiblePuppetList();

    $('#puppetList').empty();
    const puppets = getPuppetList();

    if(!chatName || helpers.equalStringLists(puppets, vPuppets))
        return;

    puppets.forEach((puppetName, i) => {
        const fa = $('<i>').addClass('fa fa-commenting puppetIco');

        const li = $('<li>');
        li.text(puppetName);
        li.append(fa);
        li.click(puppetChangeClick);

        if(i === puppets.length - 1)
            li.addClass('selectedPuppet');

        $('#puppetList').append(li);
    });
}


function tabClick(){
    const e = $(this);
    setRead(e);

    if(e.hasClass('activeTab') && !helpers.isMobile())
        return;
    $('.activeTab').removeClass('activeTab');

    e.addClass('activeTab');

    chatView.setScrolledToBottom();
    $('#messages').empty();

    chatService.setActiveChat(e.attr('name'));

    const key = window.gameState.chatKeys[e.attr('name')];

    if(key && !window.gameState.integrations.includes('sc2mafia'))
        showChatInput();
    else
        chatView.hideChatInput();

    const cName = e.attr('name') || '';

    if(hasPuppets() && onCurrentDayChat() && cName.toLowerCase().indexOf('day ') !== -1)
        $('body').addClass('puppetsShowing');
    else
        $('body').removeClass('puppetsShowing');


    $('#puppetList').hide();

    if(key)
        setPuppetTalk(window.gameState.profile.name);
    else if(hasPuppets())
        setPuppetTalk(getPuppets()[0]);


    reloadPuppetChoices();

    if(helpers.isMobile())
        switchToChatView(key || (hasPuppets() && onCurrentDayChat()));
}

function setPuppetTalk(name){
    $('#puppetText').text(name);
}

function showChatInput(){
    $('body').addClass('chat_active');
    $('.chat_input').show();
}

function switchToChatView(inputShowing){
    hideLeftPane();
    $('#chat_tabs_wrapper').hide();
    $('.chat_pane').show();
    if(!inputShowing)
        $('#chat_ticker_pane').show();
    else
        $('#chat_ticker_pane').hide();
    $('#chat_ticker_expander').show();
}

function addTab(chatObject){
    const chatHeader = chatObject.chatName;
    const div = $('<div>').addClass('chatTab');
    div.attr('name', chatHeader);
    div.attr('id', chatHeader.replace(/\s/g, ''));
    div.append($('<div>').addClass('chatPointer'));

    // mobile portion
    const aMobile = $('<a>').addClass('mobileChatLabel');
    const iMobile = $('<i>').addClass(chatObject.chatType);
    aMobile.append(iMobile);
    aMobile.append(' ');
    aMobile.append(chatHeader);
    div.append(aMobile);


    // tablet portion
    const aTablet = $('<a>');


    const iTablet = $('<i>').addClass(chatObject.chatType);
    iTablet.addClass('chatTabIcon');
    if(chatHeader.startsWith('Day'))
        iTablet.text(chatHeader.replace('Day', ''));
    else if(chatHeader.startsWith('Night'))
        iTablet.text(chatHeader.replace('Night', ''));
    div.append(iTablet);


    if(chatObject.chatDay && isTablet())
        aTablet.append(chatObject.chatDay);
    else
        aTablet.append(chatHeader);

    iTablet.append(aTablet);


    div.click(tabClick);


    window.gameState.chats[chatHeader] = {
        messages: [],
        voteCount: -1,
    };
    let oldMessage;
    const chats = window.gameState.chats;
    for(let i = 0; chats.null && i < chats.null.messages.length; i++){
        oldMessage = window.gameState.chats.null.messages[i];
        window.gameState.chats[chatHeader].messages.push(oldMessage);
    }

    const sl = new SimpleBar($('#chat_tabs')[0]);
    if(helpers.isMobile())
        $(sl.getContentElement()).prepend(div);
    else
        $(sl.getContentElement()).append(div);


    sl.recalculate();
}

function addToChat(message){
    if(typeof(message) !== 'object')
        message = {
            text: message,
        };

    if(message.text && !message.text.length)
        return;
    if(soundService.defaultSound('chat_ping'))
        $('#newChatMessageAudio')[0].play();

    const currentChatID = $('.activeTab').attr('id') || null;
    if(!message.chat){ // belongs to a particular chat
        helpers.log(message, 'no chat');
        throw message;
    }

    let returning;
    for(let i = 0; i < message.chat.length; i++){
        const chatHeader = message.chat[i];
        if(!window.gameState.chats[chatHeader])
            return helpers.log(`chat error${chatHeader}`, message);

        window.gameState.chats[chatHeader].messages.push(message);
        const activeHeaderElement = $('.activeTab');
        if(activeHeaderElement.length > 0){
            const activeHeader = activeHeaderElement.first().attr('name');
            if(activeHeader === chatHeader)
                chatService.chatQueue.push(message);
        }
        const messageType = message.messageType;
        if(messageType === 'VoteAnnouncement' || messageType === 'DeathAnnouncement')
            window.gameState.chats[chatHeader].voteCount++;

        if(!currentChatID || chatHeader.replace(' ', '') === currentChatID)
            returning = [chatHeader, message];
    }
    return returning;

    // }else{
    //     if(message.messageType === 'Feedback'){
    //         window.gameState.feedback.push(message);
    //     }
    //     var ret;
    //     for(chatHeader in window.gameState.chats){
    //         window.gameState.chats[chatHeader].messages.push(message);
    //         if(!currentChatID || chatHeader.replace(" ", "") === currentChatID)
    //             ret = chatHeader
    //     }
    //     chatQueue.push(message);
    //     return [ret, message];
    // }
}

function webSend(o){
    if(window.socket){
        window.socket.send(JSON.stringify(o));
        return;
    }
    connectWebSocket().then(() => {
        window.socket.send(JSON.stringify(o));
    });
}

$('form').submit(() => false);

function setImage(ele, name){
    if(name)
        name = name.toLowerCase();
    const imageElement = new Image();

    imageElement.onload = function(){
        ele.find('img').remove();
        ele.prepend(imageElement);
    };
    imageElement.onerror = function(){
        // image did not load

        const err = new Image();
        err.src = '/rolepics/citizen.png';

        ele.find('img').remove();
        ele.prepend(err);
    };
    imageElement.src = `/rolepics/${name}.png`;
    ele.addClass('roleCardImage');
}

function setGraveYard(){
    const graveyard = window.gameState.graveyard;

    const rolesListLength = window.gameState.setup.setupHiddens.length;
    const deadLength = graveyard.length;
    $('.peopleRemaining').text(rolesListLength - deadLength);
}

function setCommandsLabel(label){
    if(label === 'Gun')
        label = 'Give Gun';
    else if(label === 'Armor')
        label = 'Give Armor';
    else if(label === 'SpyAbility')
        label = 'Spy on';
    else if(label === 'Build')
        label = 'Build Room';
    else if(label === 'Plan')
        label = 'Death Reveal';
    else if(label === 'Murder')
        label = 'Order Hit';
    else if(label === 'Jail' && !window.gameState.isDay())
        label = 'Execute';
    else if(label === 'Votesteal')
        label = 'Redirect Vote';
    else if(label === 'Compare')
        label = 'Compare Alignment';

    $('#playerList_header span').html(label);
}

function getSpinnerOptions(spinnerID){
    const isTop = spinnerID === J.HTML_IDS.TOP_SELECT;
    const isMid = spinnerID === J.HTML_IDS.MID_SELECT;
    const isBot = spinnerID === J.HTML_IDS.BOT_SELECT;

    function onCraftSelect(e, optIndex, oldVal, newVal){
        const vestChange = (optIndex === 1 && getAction() === 'craft')
            || (optIndex === 2 && isSelectedDigCommand('craft'));

        if(selected.size() || left || right)
            if(oldVal !== 'no' && newVal !== 'no'){
                sendAction();
            }else if(oldVal === 'no' && newVal !== 'no' && selected.size()){
                cancelAction();

                if(vestChange)
                    left = selected.get(0);
                else // if(optIndex === 0 && getAction() === 'craft'), or
                    right = selected.get(0);
            }else if(oldVal !== 'no' && newVal === 'no'){
                if(left && right){
                    let name;
                    if(vestChange)
                        name = left;
                    else
                        name = right;

                    left = right = null;
                    selected.add(name);
                    sendAction();
                }else if(right && !vestChange){
                    selected.add(right);
                    right = null;
                    sendAction();
                }else if(left && vestChange){
                    selected.add(left);
                    left = null;
                    sendAction();
                }else if(left && !vestChange){
                    setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
                    left = null;
                }else{
                    setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
                    right = null;
                }
            }
    }

    function onTailorSelect(e, optIndex, oldVal, newVal){
        const teamChange = (optIndex === 0 && getAction() === 'suit')
            || (optIndex === 1 && isSelectedDigCommand('suit'));
        let idToChange;
        if(optIndex)
            idToChange = J.HTML_IDS.BOT_SELECT;
        else
            idToChange = J.HTML_IDS.MID_SELECT;

        if(teamChange){
            savedOpts[optIndex] = newVal;
            const spinner = $(`.${idToChange}`);
            const options = getSpinnerOptions(idToChange).vals;
            appendPlayerSelectOptions(spinner, options);
        }
        if(selected.size() !== 0)
            sendAction();
    }

    function onDigCommandChange(e, optIndex, oldVal, newVal){
        savedOpts[optIndex] = newVal;
        $(`.${J.HTML_IDS.MID_SELECT}`).remove();
        $(`.${J.HTML_IDS.BOT_SELECT}`).remove();

        newVal = newVal.toLowerCase();
        oldVal = oldVal.toLowerCase();
        if(newVal === 'burn' || newVal === 'alert' || newVal === 'commute')
            sendAction();
        else if(oldVal === 'burn' || oldVal === 'alert' || oldVal === 'commute')
            cancelAction();


        let spinObject = getSpinnerOptions(J.HTML_IDS.MID_SELECT);
        let options = spinObject.vals;
        if(options && options.length){
            const spinner = getBaseSpinner(J.HTML_IDS.MID_SELECT);
            appendPlayerSelectOptions(spinner, options);
            savedOpts[1] = options[0].val;
            addOptionChangeListener(spinner, spinObject.spinFunc);
            $(`.${J.HTML_IDS.TOP_SELECT}`).after(spinner);
        }

        spinObject = getSpinnerOptions(J.HTML_IDS.BOT_SELECT);
        options = spinObject.vals;
        if(options && options.length){
            const spinner = getBaseSpinner(J.HTML_IDS.BOT_SELECT);
            appendPlayerSelectOptions(spinner, options);
            addOptionChangeListener(spinner, spinObject.spinFunc);
            $(`.${J.HTML_IDS.MID_SELECT}`).after(spinner);
        }


        if(selected.size() !== 0)
            sendAction();
    }

    const action = getAction();
    let spinFunc;

    if(action === 'craft' || (isSelectedDigCommand('craft') && !isTop))
        spinFunc = onCraftSelect;
    else if(action === 'suit' || (isSelectedDigCommand('suit') && !isTop))
        spinFunc = onTailorSelect;
    else if(visibleAbilityIs('dig') && isTop)
        spinFunc = onDigCommandChange;
    else
        spinFunc = sendAction;


    let options = [];
    if(isTop){
        const command = capitalizeFirstLetter(getAction());
        if(!window.gameState.playerLists[command]){
            options = [];
        }else if(command !== 'Dig' || deadSelected.size()){
            options = window.gameState.playerLists[command].options;
            if(options)
                options = Object.values(options);
            else
                options = [];
        }else{
            options = [];
        }
    }else if(isMid && savedOpts[0]){
        const command = capitalizeFirstLetter(getAction());
        if(!window.gameState.playerLists[command]){
            options = [];
        }else if(command !== 'Dig' || deadSelected.size()){
            options = window.gameState.playerLists[command].options;
            if(!options || $.isEmptyObject(options))
                options = [];

            if($.isEmptyObject(options))
                options = [];
            else
                options = Object.values(options[savedOpts[0]].map);
        }else{
            options = [];
        }
    }else if(isBot && savedOpts[0] && savedOpts[1]){
        const command = capitalizeFirstLetter(getAction());
        if(!window.gameState.playerLists[command]){
            options = [];
        }else if(command !== 'Dig' || deadSelected.size()){
            options = window.gameState.playerLists[command].options;
            if(!options || $.isEmptyObject(options))
                options = [];
            else if(options[savedOpts[0]])
                options = options[savedOpts[0]].map;
            else
                options = Object.values(options)[0].map;


            // should be mid options now

            if(!$.isEmptyObject(options) && options[savedOpts[1]])
                options = Object.values(options[savedOpts[1]].map);
            else
                options = [];
        }else{
            options = [];
        }
    }

    if(!options)
        options = [];

    options.sort((x, y) => {
        if(x.val === 'real' || y.val === 'no')
            return -1;
        if(x.val === 'no' || y.val === 'real')
            return 1;
        return 0;
    });

    return {
        vals: options,
        spinFunc,
        spinnerID,
    };
}

function optionExists(val, optSpinner){
    let valName;
    if(typeof(val) === 'string')
        valName = val;
    else
        valName = val.val;
    const qString = `.${optSpinner} option[value='${valName}']`;
    const element = $(qString);
    if(typeof(val) === 'string')
        return element.length === 1;
    return element.length && element.text() === (val.name || val.val);
}

function capitalizeFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function isSelectedDigCommand(command){
    if(typeof(command) === 'object')
        return command.filter(isSelectedDigCommand).length;
    if(!command || typeof(command) !== 'string')
        return false;
    command = command.toLowerCase();
    return command === ($(`.${J.HTML_IDS.TOP_SELECT}`).val() || savedOpts[0] || '').toLowerCase();
}

function getBaseSpinner(spinnerIdentifier){
    const spinner = $('<select>').addClass('playerList_wrapper');
    spinner.addClass('action_targetable_element');
    spinner.addClass(spinnerIdentifier);
    return spinner;
}

function refreshSpinners(){
    const selectables = $('#player_list');
    const gameState = window.gameState;
    if(gameState.isDay() || gameState.isOver() || !gameState.started || gameState.isObserver)
        return selectables.find('select').remove();

    const spinnerIDs = [J.HTML_IDS.TOP_SELECT, J.HTML_IDS.MID_SELECT, J.HTML_IDS.BOT_SELECT];

    for(let index = 0; index < spinnerIDs.length; index++){
        const spinObject = getSpinnerOptions(spinnerIDs[index]);
        if(!spinObject.vals.length){
            $(`.${spinObject.spinnerID}`).remove();
            continue;
        }

        let optsExistsAlready = true;
        spinObject.vals.forEach(value => {
            optsExistsAlready = optsExistsAlready && optionExists(value, spinObject.spinnerID);
        });

        if(optsExistsAlready)
            continue;


        $(`.${spinObject.spinnerID}`).remove();
        const spinner = getBaseSpinner(spinObject.spinnerID);

        const options = spinObject.vals;
        if(!savedOpts[index])
            savedOpts[index] = options[0].val || options[0];


        appendPlayerSelectOptions(spinner, options);
        addOptionChangeListener(spinner, spinObject.spinFunc);

        const selects = $('#player_list').find('select');
        if(selects.length)
            selects.last().after(spinner);
        else
            $('#player_list').prepend(spinner);


        if(!savedOpts[index])
            savedOpts[index] = spinner.val();
        else
            delayedSpinnerSet(spinObject.spinnerID, savedOpts[index]);
    }
}

function appendPlayerSelectOptions(spinner, options){
    spinner.empty();
    let option;
    for(let i = 0; i < options.length; i++){
        option = $('<option>').text(options[i].name || options[i].val);
        option.css('color', options[i].color || 'black');
        option.attr('name', options[i].name || options[i].val);
        option.val(options[i].val || options[i]);
        spinner.append(option);
    }
}

function addOptionChangeListener(spinner, funcToCall){
    spinner.on('change', function(e){
        e = $(this);
        const newVal = e.val();
        let optIndex;
        if(e.hasClass(J.HTML_IDS.TOP_SELECT))
            optIndex = 0;
        else if(e.hasClass(J.HTML_IDS.MID_SELECT))
            optIndex = 1;
        else
            optIndex = 2;

        const oldVal = savedOpts[optIndex];
        if(oldVal === newVal)
            return;
        funcToCall(e, optIndex, oldVal, newVal);

        savedOpts[optIndex] = newVal;
    });
}

let actionIndex = 0;
function breadPruning(bread){
    const playerLists = window.gameState.playerLists;

    let breadCount = window.gameState.role.breadCount;
    const color = window.gameState.role.roleColor;
    const rules = window.gameState.factions[color].rules;
    const knowsTeams = rules.indexOf('Knows the identity of teammates') !== -1;
    const isBaker = hasAbility('Baker');
    const teammateNames = (window.gameState.role.roleTeam || []).map(val => val.teamAllyName);

    let givingBreadToNonteammate = false;
    let submittedBreadActionPassed = false;

    window.gameState.actions.actionList.forEach((action, i) => {
        if(action.command.toLowerCase() === 'bread')
            if(teammateNames.indexOf(action.playerNames[0]) === -1){
                givingBreadToNonteammate = true;
            }else if(i === actionIndex){
                submittedBreadActionPassed = true;
            }else{
                breadCount--;
            }
    });

    const canGiveToTeammate = !isBaker || !knowsTeams || givingBreadToNonteammate
        || submittedBreadActionPassed || breadCount > 0;

    const visible = [];
    playerLists[bread].players.forEach(pObject => {
        if(canGiveToTeammate || !teammateNames.includes(pObject.playerName))
            visible.push(pObject);
    });

    return visible;
}

function refreshBreadButton(actionName){
    const actions = window.gameState.actions;
    let sends = 0;
    let i = 0;
    for(; actions && i < actions.actionList.length; i++){
        const action = actions.actionList[i];

        if(action.command === 'send')
            sends++;
    }
    // i = # of submitted actions
    i -= sends;

    const breadButton = $('#add_action');

    let showBreadButton = true;
    if(window.gameState.isOver())
        showBreadButton = false;
    else if(!window.gameState.playerLists[actionName])
        showBreadButton = false;
    else if(i === 0 || !window.gameState.playerLists[actionName].canAddAction)
        showBreadButton = false;
    else if(actionIndex === actions.actionList.length)
        showBreadButton = false;
    if(showBreadButton)
        breadButton.show();
    else
        breadButton.hide();
}

function isSplitClick(name, command){
    if(command === 'votesteal')
        return true;
    if(command === 'control')
        return true;
    if(command === 'bounty' && window.gameState.role.jokerKills)
        return true;
    if(command === 'craft')
        return true;
    if(command === 'charge' && window.gameState.role.electroExtraCharge && actionIndex === 0)
        return true;
    if(command === 'dig'){
        if(!isAlive(name) || !savedOpts[0])
            return false;
        const opt = savedOpts[0].toLowerCase();
        if(opt === 'control' || opt === 'craft' || opt === 'charge')
            return true;
    }
    return false;
}

function refreshPlayers(){
    if(!window.gameState.isStarted)
        return;
    const gameState = window.gameState;
    const playerLists = gameState.playerLists;
    const playerListType = playerLists.type[gameState.commandsIndex % playerLists.type.length]
        || '';

    refreshBreadButton(playerListType);

    window.gameState.visiblePlayers = [];
    if(window.gameState.isOver()){
        window.gameState.visiblePlayers = Object.values(window.gameState.playerMap);
    }else if(playerListType.toLowerCase() === 'bread'){
        window.gameState.visiblePlayers = breadPruning(playerListType);
    }else if(playerListType.toLowerCase() === 'spyability'){
        const opts = Object.values(window.gameState.playerLists[playerListType].options);
        for(let i = 0; opts && i < opts.length; i++)
            window.gameState.visiblePlayers.push({
                playerName: opts[i].name,
            });
    }else if(playerListType && !window.gameState.endedNight()){
        const players = playerLists[playerListType].players;
        for(let i = 0; players && i < players.length; i++)
            gameState.visiblePlayers.push(players[i]);
    }else if(window.gameState.endedNight()){
        const someoneEndedNight = Object.values(window.gameState.playerMap)
            .some(player => player.endedNight && player.name !== window.gameState.profile.name);

        let players = Object.values(window.gameState.playerMap)
            .filter(player => player.name !== window.gameState.profile.name);
        if(someoneEndedNight)
            players = players.filter(player => !player.endedNight);
        else
            players = players.filter(player => !player.flip);
        window.gameState.visiblePlayers = players;
    }else if(window.gameState.profile.isJailed){
        window.gameState.visiblePlayers = [];
    }else{
        window.gameState.visiblePlayers = Object.values(window.gameState.playerMap)
            .filter(player => !player.flip);
    }

    const abilityName = playerListType.toLowerCase();

    // getAction will now work AFTER the commands label is called.
    if(window.gameState.isOver())
        setCommandsLabel('Game Over');
    else if(window.gameState.profile.isJailed)
        setCommandsLabel('You are jailed!');
    else if(!abilityName)
        setCommandsLabel('The Living');
    else if(!window.gameState.endedNight())
        setCommandsLabel(playerListType);
    else if(window.gameState.endedNight())
        setCommandsLabel('Waiting on');
    else
        setCommandsLabel('Waiting for day start.');

    const command = getAction();

    currentAction = null;
    if(gameState.actions){
        const actionList = window.gameState.actions.actionList;
        const size = actionList.length;
        const lastAction = actionList[size - 1];
        if(command === 'send' && lastAction && lastAction.command.toLowerCase() === 'send')
            currentAction = actionList[size - 1];
        else if(actionIndex < actionList.length)
            currentAction = actionList[actionIndex];
    }

    const togglable = gameState.visiblePlayers.length === 0
        && command !== 'currently sleeping'
        && command !== 'you are jailed!'
        && !gameState.endedNight();
    if(togglable){
        let name1;
        let name2;
        let activated;

        if(command === 'recruitment'){
            name1 = 'Confirm';
            name2 = 'Decline';
            activated = window.gameState.role.acceptingRecruitment;
        }else{
            activated = currentAction;
            name1 = getOnText(command);
            name2 = getOffText(command);
        }

        if(activated){
            gameState.visiblePlayers.push({
                playerName: name1,
                playerActive: true,
            });
            gameState.visiblePlayers.push({
                playerName: name2,
                playerActive: true,
            });
        }else{
            gameState.visiblePlayers.push({
                playerName: name1,
                playerActive: true,
            });
            gameState.visiblePlayers.push({
                playerName: name2,
                playerActive: true,
            });
        }
    }


    const selectables = $('#player_list');

    if(!selected && gameState.started){
        selected = new ArrayList();
        selected2 = new ArrayList();
        deadSelected = new ArrayList();
        savedOpts = [null, null, null];
    }
    if(currentAction){
        if(currentAction.option)
            savedOpts[0] = currentAction.option;
        if(currentAction.option2)
            savedOpts[1] = currentAction.option2;
        if(currentAction.option3)
            savedOpts[2] = currentAction.option3;
    }

    selectables.find('li').remove();

    refreshSpinners();

    for(let i = 0; i < gameState.visiblePlayers.length; i++){
        const player = gameState.visiblePlayers[i];
        const playerName = player.playerName || player.name;
        const text = playerName;

        const isComputer = player.isComputer;
        const isSplit = isSplitClick(playerName, command);
        const hoverable = !window.gameState.isOver() && abilityName;
        const targetable = new Targetable({
            name: playerName,
            isComputer,
            userID: player.userID,
        }, text, isSplit, hoverable);
        playerView.saveTargetable(targetable);
        if(!gameState.endedNight() && !gameState.isOver())
            if(gameState.isAlive === true
                || hasAbility('Ghost')){ // === true is because it could be equal to 'Ghost'
                const classesToFind = '.right_targetable_click, .left_targetable_click';
                const element = targetable.elm.find(classesToFind);
                element.click(targetableClick);
            }


        // for the not allowing a live click
        if(command !== 'dig' && command !== 'murder')
            targetable.makeVisible(selectables);
        else if(currentAction)
            targetable.makeVisible(selectables);
        else if(command === 'dig' && (deadSelected.size() || !isAlive(playerName)))
            targetable.makeVisible(selectables);
        else if(command === 'murder')
            if(left)
                targetable.makeVisible(selectables);
            else{
                let showMurders = false;
                if(gameState.role && gameState.role.roleTeam)
                    gameState.role.roleTeam.forEach(val => {
                        if(playerName === val.teamAllyName)
                            showMurders = true;
                    });

                if(playerName === gameState.role.displayName)
                    showMurders = true;
                if(showMurders)
                    targetable.makeVisible(selectables);
            }
    }

    const actionCopy = copyAction(currentAction);

    if(actionCopy && getAction() === actionCopy.command.toLowerCase()){
        if(command === 'jail' && gameState.role.jailCleanings)
            colorJailExecution(actionCopy);
        else if(command === 'dig')
            colorDigList(actionCopy);
        else if(command === 'craft')
            colorCraftList(actionCopy);
        else if(command === 'charge' || command === 'control' || command === 'murder'
            || command === 'votesteal')
            colorChargeList(currentAction);
        else if(isOnOffCommand(command))
            colorOnOffList(actionCopy);
        else if(command === 'bounty')
            colorBountyList(actionCopy);
        else
            colorSingle(actionCopy);
    }else if(getAction() === 'recruitment'){
        selected.clear();
        if(gameState.role.acceptingRecruitment){
            selected.add('confirm');
            setButtonSelected('Confirm', J.TARGET_TYPE.FULLY_CLICKED);
        }else{
            selected.add('decline');
            setButtonSelected('Decline', J.TARGET_TYPE.FULLY_CLICKED);
        }
    }else if(command === 'dig'){
        colorDigList();
    }else if(left === right && left){
        setButtonSelected(left, J.TARGET_TYPE.WITCH_SELECTED);
    }else if(left || right){
        if(left)
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
        if(right)
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    }else if(deadSelected && deadSelected.size()){
        setButtonSelected(deadSelected.get(0), J.TARGET_TYPE.FULLY_CLICKED);
    }else if(selected && selected.size()){
        setButtonSelected(selected.get(0), J.TARGET_TYPE.FULLY_CLICKED);
    }

    if(isOnOffCommand(command))
        colorOnOffList(actionCopy);

    adjustActionPane();

    // new SimpleBar(selectables[0]);
}

function copyAction(cAction){
    if(!cAction)
        return cAction;
    const ret = {};
    ret.command = cAction.command;
    ret.option = cAction.option;
    ret.option2 = cAction.option2;
    ret.option3 = cAction.option3;
    ret.text = cAction.text;
    ret.playerNames = [];
    for(let i = 0; i < cAction.playerNames.length; i++)
        ret.playerNames[i] = cAction.playerNames[i];

    return ret;
}

const onOffCommands = new Set(['burn', 'vest', 'alert', 'reveal', 'commute', 'marshalllaw']);
function isOnOffCommand(command){
    return onOffCommands.has(command);
}

function getOnText(command){
    if(command === 'vest')
        return 'Use Vest';
    if(command === 'burn')
        return 'Burn';
    if(command === 'alert')
        return 'Guard house';
    if(command === 'reveal')
        return `Reveal as ${window.gameState.role.roleName}`;
    if(command === 'marshalllaw')
        return `Reveal as ${window.gameState.role.roleName}`;
    if(command === 'commute')
        return 'Leave Town';
}

function getOffText(command){
    if(command === 'vest')
        return 'Save Vest';
    if(command === 'burn')
        return "Don't Burn";
    if(command === 'alert')
        return "Don't kill visitors";
    if(command === 'reveal')
        return 'Stay hidden';
    if(command === 'marshalllaw')
        return 'Stay hidden';
    if(command === 'commute')
        return 'Stay in Town';
}

function colorJailExecution(cAction){
    const rights = parseInt(cAction.option || 0, 10);
    selected.clear();
    selected2.clear();
    let name;
    for(let i = 0; i < cAction.playerNames.length; i++){
        name = cAction.playerNames[i];
        if(i < rights){
            selected2.add(name);
            setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
        }else{
            selected.add(name);
            setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
        }
    }
}

function colorBountyList(cAction){
    if(!window.gameState.role.jokerKills)
        return colorSingle(cAction);
    selected.clear();
    let killStart = 0;

    if(!cAction.option){
        left = cAction.playerNames[0];
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
        killStart++;
    }

    cAction.playerNames.forEach((val, i) => {
        if(i < killStart)
            return;
        selected.add(val);
        setButtonSelected(val, J.TARGET_TYPE.RIGHT_SELECTED);
    });
}

function colorCraftList(cAction){
    if(cAction){
        const targets = cAction.playerNames;
        if(targets.length === 1){
            colorSingle(cAction);
        }else{
            left = targets[0];
            right = targets[1];
            setButtonSelected(targets[0], J.TARGET_TYPE.LEFT_SELECTED);
            setButtonSelected(targets[1], J.TARGET_TYPE.RIGHT_SELECTED);
        }

        if(cAction.option3){
            delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, cAction.option);
            delayedSpinnerSet(J.HTML_IDS.MID_SELECT, cAction.option2);
            delayedSpinnerSet(J.HTML_IDS.BOT_SELECT, cAction.option3);
        }else{
            delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, cAction.option);
            delayedSpinnerSet(J.HTML_IDS.MID_SELECT, cAction.option2);
        }
    }else if(left){
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    }else if(right){
        setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    }
}

function colorDigList(cAction){
    if(cAction){
        const targets = cAction.playerNames;
        deadSelected.add(targets[0], UNIQUE);
        setButtonSelected(targets[0], J.TARGET_TYPE.FULLY_CLICKED);

        savedOpts[0] = cAction.option;
        savedOpts[1] = cAction.option2;
        savedOpts[2] = cAction.option3;
        refreshSpinners();

        delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, cAction.option);
        delayedSpinnerSet(J.HTML_IDS.MID_SELECT, cAction.option2);
        delayedSpinnerSet(J.HTML_IDS.BOT_SELECT, cAction.option3);

        targets.splice(0, 1);
        if(isSelectedDigCommand('craft') || savedOpts[0] === 'Craft'){
            colorCraftList(cAction);
            return;
        }if(isSelectedDigCommand('control') || savedOpts[0] === 'control'
            || isSelectedDigCommand('murder') || savedOpts[0] === 'murder')
            colorChargeList(cAction);
        else
            colorSingle(cAction);


        targets.splice(0, 0, deadSelected.get(0));
    }

    setButtonSelected(deadSelected.get(0), J.TARGET_TYPE.FULLY_CLICKED);

    if(isSelectedDigCommand('craft'))
        colorCraftList();
    else
        colorSingle();

    // should just follow some other color protocol

    // if(left)
    //  setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    // if(right)
    //  setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    // if(deadSelected && deadSelected.size()){
    //  setButtonSelected(deadSelected.get(0), J.TARGET_TYPE.FULLY_CLICKED);
    // }
    // $.each(selected.backer, function(index, value){
    //  setButtonSelected(value, J.TARGET_TYPE.FULLY_CLICKED);
    // });
}

function delayedSpinnerSet(spinnerID, val){
    if(val)
        setTimeout(() => {
            if($(`.${spinnerID}`).val() !== val)
                $(`.${spinnerID}`).val(val);
        }, 100);
}

// don't edit currentAction, this is NOT a copy! (if its coming from electro)
// could also come from witch
function colorChargeList(action){
    if(action){
        const targets = action.playerNames;
        const role = window.gameState.role;
        const actions = window.gameState.actions;
        if(targets.length === 1){
            if(role.electroExtraCharge && actions.actionList[0] !== action){
                selected.add(targets[0]);
                setButtonSelected(targets[0], J.TARGET_TYPE.FULLY_CLICKED);
            }else if(!right){
                left = targets[0];
                setButtonSelected(targets[0], J.TARGET_TYPE.LEFT_SELECTED);
            }else{
                right = targets[0];
                setButtonSelected(targets[0], J.TARGET_TYPE.RIGHT_SELECTED);
            }
        }else if(targets[0] === targets[1]){
            left = right = targets[0];
            setButtonSelected(targets[0], J.TARGET_TYPE.WITCH_SELECTED);
        }else{
            left = targets[0];
            right = targets[1];
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }
    }else if(getAction() === 'control'){
        if(left)
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
        else if(right)
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
    }
}

function colorOnOffList(cAction){
    const action = getAction();
    let text;

    if(cAction)
        text = getOnText(action);
    else
        text = getOffText(action);

    selected.clear();
    selected.add(text);
    setButtonSelected(text, J.TARGET_TYPE.FULLY_CLICKED);
}

function colorSingle(cAction){
    let targets;
    let opt1;
    let opt2;
    if(cAction){
        targets = cAction.playerNames;
        opt1 = cAction.option;
        opt2 = cAction.option2;
    }else if(selected){
        targets = selected.backer;
        opt1 = savedOpts[0];
        opt2 = savedOpts[1];
    }else{
        selected = new ArrayList();
    }
    targets.forEach(target => {
        selected.add(target, UNIQUE);
        setButtonSelected(target, J.TARGET_TYPE.FULLY_CLICKED);
    });

    if(opt1){
        savedOpts[0] = opt1;
        delayedSpinnerSet(J.HTML_IDS.TOP_SELECT, opt1);
    }
    if(opt2){
        savedOpts[1] = opt2;
        delayedSpinnerSet(J.HTML_IDS.MID_SELECT, opt2);
    }
}

function targetableClick(){
    const e = $(this);
    const name = e.attr('name');
    resolveClick(name, e.hasClass('right_targetable_click'));
}


function singleTargetAction(){
    const command = getAction();
    if(command === 'control')
        return false;
    if(command === 'votesteal')
        return false;
    if(command === 'murder')
        return false;
    if(command === 'bounty')
        return false;
    if(command === 'craft')
        return false;
    if(command === 'dig')
        return false;
    if(command !== 'charge')
        return true;

    // handling electrocutioner initial charge


    // at this point the command is 'charge'
    const actions = window.gameState.actions;
    if(!actions)
        return true;
    let action;
    for(let i = 0; i < actions.length; i++){
        action = actions[i];
        if(actions.playerNames > 1 && command === action.command)
            return i === actionIndex;
    }


    return !window.gameState.role.electroExtraCharge;
}

function resolveClick(name, rclick){
    if(window.gameState.endedNight() || !window.gameState.started)
        return;
    const command = getAction();

    if(command === 'send')
        return handleSendClick(name);
    if(command === 'jail' && !window.gameState.isDay())
        if(window.gameState.role.jailCleanings){
            handleCleanedExecutionClick(name, rclick);
        }else
            handleSingleClick(name, 1, window.gameState.visiblePlayers.length);
    else if(singleTargetAction())
        if(command !== 'swap' && command !== 'build' && command !== 'switch'){
            handleSingleClick(name);
        }else{ // if swap
            if(selected.contains(name)){
                if(selected.size() === 2)
                    cancelAction();

                selected.remove(name);
                return setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
            }
            if(selected.size() === 2){
                setButtonSelected(selected.get(1), J.TARGET_TYPE.UNCLICKED);
                selected.backer[1] = name;
                setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            }else{
                selected.add(name);
                setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            }
            if(selected.size() === 2)
                sendAction();
        }
    else if(command === 'charge')
        handleDoubleElectroCharge(name, rclick);
    else if(command === 'dig')
        handleDigClick(name, rclick);
    else if(command === 'craft')
        handleCraftClick(name, rclick);
    // these are now split acitons.
    else if(command === 'control')
        handleControlClick(name, rclick);
    else if(command === 'murder')
        handleMurderClick(name, rclick);
    else if(command === 'bounty')
        handleBountyClick(name, rclick);
    else
        handleSplitClick(name, rclick);
}

function handleBountyClick(name, rclick){
    if(!window.gameState.role.jokerKills)
        return handleSingleClick(name);

    if(!rclick){ // bounty submission
        if(left === name){
            left = null;
            if(selected.contains(name))
                setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
            else
                setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
        }else if(left){
            if(selected.contains(left))
                setButtonSelected(left, J.TARGET_TYPE.RIGHT_SELECTED);
            else
                setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
            left = name;
            if(selected.contains(name))
                selected.remove(name);
            setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
        }else{
            left = name;
            if(selected.contains(name))
                selected.remove(name);
            setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
        }
    }else{ // kill submission
        const jKills = window.gameState.role.jokerKills;
        if(selected.size() === jKills && !selected.contains(name)){
            const toRemove = selected.removeLast();
            setButtonSelected(toRemove, J.TARGET_TYPE.UNCLICKED);

            if(left === name)
                left = null;
            setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
            selected.add(name);
        }else if(selected.contains(name)){
            if(left === name)
                setButtonSelected(name, J.TARGET_TYPE.LEFT_SELECTED);
            else
                setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
            selected.remove(name);
        }else{
            if(left === name)
                left = null;
            setButtonSelected(name, J.TARGET_TYPE.RIGHT_SELECTED);
            selected.add(name);
        }
    }
    if(left || selected.size())
        sendAction();
    else
        cancelAction();
}

function handleCleanedExecutionClick(name, rclick){
    let list;
    let color;
    let otherList;
    if(rclick){
        list = selected2;
        otherList = selected;
        color = J.TARGET_TYPE.RIGHT_SELECTED;
        if(!selected2.contains(name) && selected2.size() >= window.gameState.role.jailCleanings){
            const last = selected2.removeLast();
            setButtonSelected(last, J.TARGET_TYPE.UNCLICKED);
        }
    }else{
        list = selected;
        otherList = selected2;
        color = J.TARGET_TYPE.LEFT_SELECTED;
    }
    if(list.contains(name)){
        list.remove(name);
        setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
    }else{
        list.add(name);
        if(otherList.contains(name))
            otherList.remove(name);
        setButtonSelected(name, color);
    }

    if(list.size() || otherList.size())
        sendAction();
    else
        cancelAction();
}

function handleControlClick(name, rclick){
    if(rclick || name !== window.gameState.profile.name)
        handleSplitClick(name, rclick);
}

function handleMurderClick(name, rclick){
    if(rclick)
        handleSplitClick(name, rclick);


    let doClick = false;
    if(window.gameState.role && window.gameState.role.roleTeam)
        window.gameState.role.roleTeam(val => {
            if(name === val.teamAllyName || name === window.gameState.role.displayName)
                doClick = true;
        });

    if(doClick){
        handleSplitClick(name, rclick);
        refreshPlayers();
    }
}

function handleSendClick(name){
    const maxSize = 1; // window.gameState.role.maxSends || 1;  NEED TO BE ADDRESSED
    if(selected.contains(name))
        selected.remove(name);
    else if(selected.size() === maxSize)
        selected.backer[maxSize - 1] = name;
    else
        selected.add(name);


    if(selected.size())
        sendAction();
    else
        cancelAction();
}


function handleCraftClick(name, rclick){
    let gOpt;
    let vOpt;
    if(getAction() === 'dig'){
        gOpt = $(`.${J.HTML_IDS.MID_SELECT}`).val();
        vOpt = $(`.${J.HTML_IDS.BOT_SELECT}`).val();
    }else{
        gOpt = $(`.${J.HTML_IDS.TOP_SELECT}`).val();
        vOpt = $(`.${J.HTML_IDS.MID_SELECT}`).val();
    }

    if(gOpt === 'no' || vOpt === 'no'){
        if(gOpt !== vOpt)
            handleSingleClick(name);
    }else if((name === left && rclick) || (name === right && !rclick)){
        const hadAction = left && right;
        setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
        setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
        right = left = null;
        if(rclick)
            right = name;
        else
            left = name;

        if(hadAction){
            cancelAction(); // safe while canceling actions dont trigger player refresh
        }else{
            setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }
    }else{
        handleSplitClick(name, rclick);
    }
}

function handleDriverClick(name){
    handleSingleClick(name, 2);
}

function handleSingleClick(name, needed, limit){
    if(!needed)
        needed = 1;
    if(!limit)
        limit = needed;
    if(window.gameState.actions.actionList.length)
        currentAction = window.gameState.actions.actionList[actionIndex];

    let submitting;
    const command = getAction();
    if(isOnOffCommand(command)){
        if(name === selected.get(0))
            return;
        selected.clear();
        selected.add(name);
        if(name === getOnText(command))
            sendAction();
        else
            cancelAction();

        return;
    }

    if(selected.contains(name))
        submitting = selected.size() > needed;
    else
        submitting = selected.size() + 1 >= needed;

    if(submitting){
        if(selected.size() === limit)
            selected.remove(selected.get(limit - 1));
        selected.add(name);
        sendAction();
    }else if(selected.contains(name)){
        selected.remove(name);
        if(selected.size() + 1 >= needed)
            cancelAction();
        else
            setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
    }else{
        selected.add(name);
        setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
    }
}

function handleSplitClick(name, rclick){
    if(rclick){
        if(right && left){
            if(right === name){
                right = null;
                cancelAction();
            }else{
                right = name;
                sendAction();
            }
        }else if(left){
            right = name;
            sendAction();
        }else if(right === name){
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
            right = null;
        }else if(right){
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
            right = name;
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }else{
            right = name;
            setButtonSelected(right, J.TARGET_TYPE.RIGHT_SELECTED);
        }
    }else
    if(left && right){
        if(left === name){
            left = null;
            cancelAction();
        }else{
            left = name;
            sendAction();
        }
    }else if(right){
        left = name;
        sendAction();
    }else if(left === name){
        setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
        left = null;
    }else if(left){
        setButtonSelected(left, J.TARGET_TYPE.UNCLICKED);
        left = name;
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    }else{
        left = name;
        setButtonSelected(left, J.TARGET_TYPE.LEFT_SELECTED);
    }
}

function handleDoubleElectroCharge(name, rclick){
    if(window.gameState.actions)
        if(actionIndex < window.gameState.actions.actionList.length)
            currentAction = window.gameState.actions.actionList[actionIndex];

    let actionHandled = false;
    window.gameState.actions.actionList.forEach((action, index) => {
        if(action.command === 'charge' && action.playerNames.length === 2 && index !== 0)
            actionHandled = true;
    });
    if(actionHandled)
        return handleSingleClick(name);

    if(rclick){
        if(right && left){
            if(right === name){
                right = null;
                sendAction();
            }else{
                right = name;
                sendAction();
            }
        }else if(left){
            right = name;
            sendAction();
        }else if(right !== name){
            right = name;
            sendAction();
        }else{
            right = null;
            cancelAction();
        }
    }else
    if(left && right){
        if(left === name){
            left = null;
            sendAction();
        }else{
            left = name;
            sendAction();
        }
    }else if(right){
        left = name;
        sendAction();
    }else if(left !== name){
        left = name;
        sendAction();
    }else{
        left = null;
        cancelAction();
    }
}

function getDeadPerson(deadName){
    let deadPerson;
    for(let i = 0; i < window.gameState.graveyard.length; i++){
        deadPerson = window.gameState.graveyard[i];
        if(deadPerson.name === deadName)
            return deadPerson;
    }
    return null;
}

const deadNoTargetActions = new Set(['burn', 'alert', 'commute']);
function handleDigClick(name, rclick){
    function clearLiveSelection(){
        if(right)
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
        if(left)
            setButtonSelected(right, J.TARGET_TYPE.UNCLICKED);
        for(let i = 0; i < selected.size(); i++)
            setButtonSelected(selected.get(i), J.TARGET_TYPE.UNCLICKED);

        selected.clear();
        left = right = null;
    }

    let deadPerson = getDeadPerson(name);


    if(deadPerson){ // was clicked
        if(deadSelected.size() === 0){
            deadSelected.add(name);

            refreshPlayers();
            let selectedAction = savedOpts[0];
            if(selectedAction)
                selectedAction = selectedAction.toLowerCase();

            if(deadNoTargetActions.has(selectedAction))
                sendAction();
        }else{
            if(deadSelected.contains(name))
                deadSelected.clear();
            else
                deadSelected.backer[0] = name;

            clearLiveSelection();
            if(window.gameState.actions.actionList.length - 1 === actionIndex)
                cancelAction();
            else
                refreshPlayers();
        }
    }else{ // live person was clicked
        if(deadSelected.size() === 0) // this should never happen now
            return;
        deadPerson = getDeadPerson(deadSelected.get(0));

        if(isSelectedDigCommand('craft')){
            handleCraftClick(name, rclick);
        }else if(isSelectedDigCommand('control')){
            handleSplitClick(name, rclick);
        }else if(isSelectedDigCommand('swap') || isSelectedDigCommand('switch')){
            handleDriverClick(name);
        }else if(isSelectedDigCommand('burn', 'alert', 'commute')){
            if(!selected.contains(name)){
                selected.add(name);
                sendAction();
            }else{
                selected.remove(name);
                cancelAction();
            }
        }else if(selected.size() === 0){ // all other roles
            selected.add(name);
            setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            sendAction();
        }else if(selected.contains(name)){
            selected.clear();
            setButtonSelected(name, J.TARGET_TYPE.UNCLICKED);
            cancelAction();
        }else{
            setButtonSelected(selected.get(0), J.TARGET_TYPE.UNCLICKED);
            setButtonSelected(name, J.TARGET_TYPE.FULLY_CLICKED);
            selected.backer[0] = name;
            sendAction();
        }
        // targeting logic except for witch
    } // end live clicked person
}

function handlePuppetPunch(name){
    let puppet = getAction();
    puppet = puppet.toLowerCase().replace(' punch', '');
    const message = `vpunch ${puppet} ${name}`;
    webSend({
        message,
    });
}

function cancelAction(){
    submitActionToServer(false);
}

function sendAction(){
    submitActionToServer(true);
}

const noTargetCommands = new Set(['burn', 'alert', 'vest', 'marshalllaw', 'reveal', 'commute']);
const doubleTargetCommands = new Set(['swap', 'switch', 'build']);
const optionCommands = new Set(['frame', 'drug', 'gun', 'armor']);
function submitActionToServer(submitAction){
    let message;
    let command = getAction();
    if(command.toLowerCase().endsWith(' punch'))
        return handlePuppetPunch(selected.get(0));
    if(command === 'recruitment'){
        message = selected.get(0).toLowerCase();
    }else if(command === 'assassinate'){
        message = `${command} ${selected.get(0)}`;
    }else if(command === 'shoot' && window.gameState.isDay()){
        message = `${command} ${selected.get(0)}`;
    }else if(noTargetCommands.has(command)){
        submitAction = selected.get(0) === window.gameState.visiblePlayers[0].playerName;
        if(submitAction)
            message = command;
    }

    if(typeof(message) === 'string')
        return actionService.submit(message).then(refreshPlayers);

    const newAction = {
        command,
    };

    if(!submitAction){
        newAction.message = 'cancelAction';
        if(command === 'send')
            command = window.gameState.actions.actionList.length - 1;
        else
            newAction.oldAction = actionIndex;
        if(!doubleTargetCommands.has(command) && singleTargetAction()){
            if(command !== 'send')
                actionIndex = Math.max(0, actionIndex - 1);
            selected.clear();
        }
        return actionService.cancel(actionIndex, command);
    }

    const topSpinner = `.${J.HTML_IDS.TOP_SELECT}`;
    const midSpinner = `.${J.HTML_IDS.MID_SELECT}`;
    const botSpinner = `.${J.HTML_IDS.BOT_SELECT}`;

    if(optionCommands.has(command)
        || (command === 'convert' && window.gameState.getSetupModifierValue('CULT_PROMOTION'))){
        newAction.option = $(topSpinner).val();
        newAction.targets = [selected.get(0)];
    }else if(command === 'bounty'){
        if(window.gameState.role.jokerKills)
            if(!left){
                newAction.targets = selected.backer;
                newAction.option = 'kill';
            }else{
                newAction.targets = [left];
                selected.backer.forEach(newAction.targets.push);
            }
        else
            newAction.targets = selected.backer;
    }else if(command === 'spyability'){
        newAction.option = selected.get(0);
        newAction.targets = [];
    }else if(command === 'suit'){
        newAction.targets = [selected.get(0)];
        newAction.option = $(topSpinner).val();
        newAction.option2 = $(midSpinner).val();
    }else if(command === 'craft'){
        newAction.option = $(topSpinner).val().replace(' gun', '');
        newAction.option2 = $(midSpinner).val();
        if(left || right)
            newAction.targets = [left, right];
        else
            newAction.targets = selected.backer;
    }else if(command === 'dig'){
        if(deadSelected.size() < 1)
            return;
        newAction.targets = [deadSelected.get(0)];

        newAction.option = $(topSpinner).val() || null;
        newAction.option2 = $(midSpinner).val() || null;
        newAction.option3 = $(botSpinner).val() || null;

        if(isSelectedDigCommand('control') || (isSelectedDigCommand('craft') && (left || right))){
            if(!left || !right)
                return;
            newAction.targets.push(left);
            newAction.targets.push(right);
        }else if(newAction.option === 'Burn'
            || newAction.option === 'Commute'
            || newAction.option === 'Alert'){
            // no work needed
        }else{
            if(selected.size() < 1)
                return;
            for(let i = 0; i < selected.size(); i++)
                newAction.targets.push(selected.get(i));
        }
    }else if(command === 'jail' && window.gameState.role.jailCleanings && submitAction){
        newAction.targets = selected2.backer.concat(selected.backer);
        if(selected2.size())
            newAction.option = selected2.size().toString();
    }else if(left || right){
        newAction.targets = [];
        if(left)
            newAction.targets.push(left);
        if(right)
            newAction.targets.push(right);
    }else if(selected.size){
        newAction.targets = selected.backer;
    }

    const submittedAction = window.gameState.actions.actionList[actionIndex];
    const tryingToReplaceSend = submittedAction && submittedAction.command.toLowerCase() === 'send';

    const actionList = window.gameState.actions.actionList;
    if(actionIndex < actionList.length && command !== 'send' && !tryingToReplaceSend){
        if(command !== 'charge')
            left = right = selected = deadSelected = selected2 = null; // if i'm going to replace the action, i don't need to save my old stuff
        newAction.oldAction = actionIndex;
    }

    actionService.submit(newAction);
}

J.TARGET_TYPE = {
    UNCLICKED: 0,
    FULLY_CLICKED: 1,
    RIGHT_SELECTED: 2,
    LEFT_SELECTED: 3,
    WITCH_SELECTED: 4,
};

function setButtonSelected(name, mode){
    if(!name)
        return;
    const targetable = playerView.getTargetable(name);

    targetable.removeClass(true);
    targetable.removeClass(false);

    if(mode === J.TARGET_TYPE.FULLY_CLICKED){
        targetable.addClass(false, selectOptions.RIGHT_REGULAR_SELECT);
        targetable.addClass(true, selectOptions.LEFT_REGULAR_SELECT);
    }else if(mode === J.TARGET_TYPE.WITCH_SELECTED){
        targetable.addClass(true, selectOptions.LEFT_REGULAR_SELECT);
        targetable.addClass(false, selectOptions.WITCH_TARGET);
    }else if(mode === J.TARGET_TYPE.RIGHT_SELECTED){
        targetable.addClass(true, selectOptions.TARGETABLE_LEFT_DIV);
        targetable.addClass(false, selectOptions.RIGHT_REGULAR_SELECT);
    }else if(mode === J.TARGET_TYPE.LEFT_SELECTED){
        targetable.addClass(true, selectOptions.LEFT_REGULAR_SELECT);
        targetable.addClass(false, selectOptions.TARGETABLE_RIGHT_DIV);
    }else if(mode === J.TARGET_TYPE.UNCLICKED){
        targetable.addClass(true, selectOptions.TARGETABLE_LEFT_DIV); // is left
        targetable.addClass(false, selectOptions.TARGETABLE_RIGHT_DIV); // is right
    }
}

function setMultiCommandButtons(){
    const len = window.gameState.playerLists.type.length;
    if(len < 2){
        $('#arrow1').hide();
        $('#arrow2').hide();
    }else{
        const lt = $('#arrow1');
        const gt = $('#arrow2');
        lt.show();
        gt.show();
        lt.unbind();
        gt.unbind();
        lt.click(() => {
            window.gameState.commandsIndex = (window.gameState.commandsIndex + len - 1) % len;
            selected = null;
            selected2 = null;
            deadSelected = null;
            savedOpts = [null, null, null];
            refreshPlayers();
        });
        gt.click(() => {
            selected = null;
            selected2 = null;
            deadSelected = null;
            savedOpts = [null, null, null];
            window.gameState.commandsIndex = (window.gameState.commandsIndex + 1) % len;

            refreshPlayers();
        });
    }
}

function getAction(){
    let command = $('#playerList_header span').text().toLowerCase();
    if(command === 'give gun')
        command = 'gun';
    else if(command === 'give armor')
        command = 'armor';
    else if(command === 'spy on')
        command = 'spyability';
    else if(command === 'build room')
        command = 'build';
    else if(command === 'death reveal')
        command = 'plan';
    else if(command === 'order hit')
        command = 'murder';
    else if(command === 'execute')
        command = 'jail';
    else if(command === 'redirect vote')
        command = 'votesteal';
    else if(command === 'compare alignment')
        command = 'compare';
    return command;
}

function hasAbility(abilityName){
    abilityName = abilityName.replace(' ', '');
    const abilities = window.gameState.role.abilities;
    return abilities && abilities.indexOf(abilityName) !== -1;
}

function isAlive(playerName){
    if(!playerName)
        return false;
    return window.gameState.deadPlayers.indexOf(playerName) === -1;
}

function visibleAbilityIs(ability){
    if(!ability || typeof(ability) !== 'string')
        return false;
    const visibleAbility = getAction();
    return ability.toLowerCase() === visibleAbility;
}

function adjustActionPane(){
    const actionPane = $('#action_nav_pane');
    actionPane.removeClass('pl_no_actions');
    actionPane.removeClass('pl_show_actions');
    actionPane.removeClass('pl_only_button');
    actionPane.removeClass('pl_show_actions_no_button');
    $('#end_night_button').css('display', '');


    try{
        if(window.gameState.isOver())
            actionPane.addClass('pl_only_button');
        else if(!window.gameState.isAlive || window.gameState.isObserver)
            actionPane.addClass('pl_no_actions');
        else if(window.gameState.isDay())
            if(window.gameState.profile.isPuppeted || window.gameState.profile.isBlackmailed){
                actionPane.addClass('pl_no_actions');
            }else if(visibleAbilityIs('Jail') || visibleAbilityIs('Build')){
                actionPane.addClass('pl_show_actions_no_button');
            }else if(!visibleAbilityIs('Assassinate') && window.gameState.hasDayAction){
                actionPane.addClass('pl_only_button');
            }else{
                actionPane.addClass('pl_no_actions');
            }
        else if(window.gameState.endedNight())
            actionPane.addClass('pl_show_actions');
        else if(window.gameState.role && window.gameState.role.breadCount)
            actionPane.addClass('pl_show_actions');
        else if(window.gameState.playerLists.type.length > 1)
            actionPane.addClass('pl_show_actions');
        else if(visibleAbilityIs('Craft')
            || visibleAbilityIs('Dig')
            || visibleAbilityIs('Murder')
            || visibleAbilityIs('Control')
            || (visibleAbilityIs('Bounty') && window.gameState.role.jokerKills))
            actionPane.addClass('pl_show_actions');
        else
            actionPane.addClass('pl_only_button');
    }catch(err){
        helpers.log(err);
        actionPane.addClass('pl_show_actions');
    }
}

function setDayLabel(label){
    $('#day').html(label);
}

function setDayIcon(){
    const iconElement = $('#nav_tab_1 .navbarIcon');
    if(window.gameState.isDay()){
        iconElement.removeClass('fa fa-moon');
        iconElement.addClass('fa fa-sun');
    }else{
        iconElement.removeClass('fa fa-sun');
        iconElement.addClass('fa fa-moon');
    }
}

function setTimer(){
    let timeLeft;
    if(window.gameState.timer !== null){
        timeLeft = window.gameState.timer - new Date().getTime();
        if(timeLeft < 0)
            timeLeft = null;
    }else{
        timeLeft = null;
    }

    if(timeLeft !== null){
        let seconds = Math.floor(timeLeft / 1000);
        let minutes = Math.floor(seconds / 60);
        const hour = Math.floor(minutes / 60);

        let text = '';
        if(hour > 0){
            text += `${hour}:`;
            minutes %= 60;
            if(minutes < 10)
                minutes = `0${minutes}`;
        }
        text += `${minutes}:`;
        seconds = (seconds % 60).toString();
        if(seconds < 10)
            seconds = `0${seconds}`;

        text += seconds;


        $('.timerText').text(text);
    }else{
        $('.timerText').text('0:00');
    }
    if(getUnread() !== null){
        if($('#chat_ticker_expander i').hasClass('unread_ticker'))
            $('#chat_ticker_expander i').removeClass('unread_ticker');
        else
            $('#chat_ticker_expander i').addClass('unread_ticker');

        tabBlinker();

        Object.keys(window.gameState.unreadChats).forEach(name => {
            let eleID = name;
            eleID = eleID.replace(/ /g, '');
            eleID = eleID.replace('{', '\\{');
            eleID = eleID.replace('}', '\\}');
            eleID = eleID.replace('[', '\\[');
            eleID = eleID.replace(']', '\\]');
            eleID = eleID.replace('^', '\\^');
            eleID = eleID.replace("'", "\\'");
            eleID = eleID.replace('/', '\\/');
            eleID = eleID.replace('$', '\\$');
            const ele = $(`#${eleID}`);
            if(!window.gameState.unreadChats[name])
                ele.removeClass('unread_ticker');
            else
                ele.addClass('unread_ticker');
        });
    }else{
        resetTitle();
    }
    setTimeout(setTimer, 500);
}

function getHiddenProp(){
    const prefixes = ['webkit', 'moz', 'ms', 'o'];

    // if 'hidden' is natively supported just return it
    if('hidden' in document)
        return 'hidden';

    // otherwise loop over all the known prefixes until we find one
    for(let i = 0; i < prefixes.length; i++)
        if((`${prefixes[i]}Hidden`) in document)
            return `${prefixes[i]}Hidden`;


    // otherwise it's not supported
    return null;
}

function isHidden(){
    const prop = getHiddenProp();
    if(!prop)
        return false;

    return document[prop];
}

function getUnread(){
    let ret = null;
    Object.keys(window.gameState.unreadChats).forEach(val => {
        if(window.gameState.unreadChats[val] !== 0)
            if(!ret)
                ret = val;
            else if(window.gameState.unreadChats[val] > window.gameState.unreadChats[ret])
                ret = val;
    });

    return ret;
}

let blinked = 0;

function tabBlinker(){
    if(blinked === 0){
        blinked = 1;
    }else if(blinked === 1){
        blinked = 2;
        const unread = getUnread();
        setTitle(`${unread} (${window.gameState.unreadChats[unread]})`);
    }else if(blinked === 2){
        blinked = 3;
    }else{
        blinked = 0;
        resetTitle();
    }
}

function setTrim(color){
    if(!window.gameState.getGameModifierValue('CHAT_ROLES'))
        color = $('.nav_button_text').css('color');
    const trimObjects = $('.trim');

    trimObjects.css('color', color);
    // trimObjects.css('border-color', color);
    trimObjects.css('-moz-text-decoration-color', color);
    trimObjects.css('text-decoration-color', color);

    $('#chat_header').css('background', color);
    if(color === '#FFFF00')
        $('#chat_header h2').css('color', 'black');


    // const navBarEls = $('.nav_button .fa, .nav_button span');
    // if(window.gameState.started)
    //     navBarEls.css('color', color);
    // else
    //     navBarEls.css('color', NORM_TEXT_COLOR);
}
setTrim(NORM_TEXT_COLOR);

function refreshActionButtonText(){
    if(window.gameState.isOver())
        setButtonText('Exit Game');
    else if(window.gameState.isDay() && window.gameState.role)
        setButtonText('Assassinate');
    else if(window.gameState.endedNight())
        setButtonText('I\'m not Ready!');
    else
        setButtonText('Skip to Daytime');
}

function setButtonText(text){
    const button = $('#end_night_button');
    button.text(text);
}

function setLastWillContainer(){
    const lwContainer = $('#your_exp_pane');
    let chosenLWClass;
    const generalLW = window.gameState.getSetupModifierValue('LAST_WILL');
    if(window.gameState.isAlive)
        if(generalLW && hasTeamLastWill()){
            chosenLWClass = 'lw_double';
            if(hasTeamLastWill()){
                const teamName = window.gameState.factions[window.gameState.role.roleColor].name;
                lwContainer.find('.settings_off').text(teamName);
            }
        }else if(generalLW || hasTeamLastWill()){
            chosenLWClass = 'lw_single';
        }else{
            chosenLWClass = 'lw_none';
        }
    else
        chosenLWClass = 'lw_none';


    lwContainer.removeClass('lw_single lw_double lw_none');
    lwContainer.addClass(chosenLWClass);
}

function hasTeamLastWill(){
    if(!window.gameState.role)
        return false;
    const color = window.gameState.role.roleColor;
    const factions = Object.values(window.gameState.setup.factionMap);
    const faction = factions.find(f => f.color === color);
    if(!faction)
        return false;
    const modifier = faction.modifiers.find(m => m.name === 'LAST_WILL');
    if(!modifier)
        return false;
    return modifier.value;
}

function setIsAlive(livingStatus){
    window.gameState.isAlive = livingStatus;

    if(!livingStatus)
        $('.last_will_item').hide();

    if(!window.gameState.isAlive || window.gameState.isOver())
        $('#leaveGameButton').show();
    else
        $('#leaveGameButton').hide();
}

function setRoleInfo(){
    if(!window.gameState.started)
        return;

    const roleInfo = window.gameState.role;

    setIsAlive(roleInfo.isAlive);

    const roleImageName = 'citizen'; // roleInfo.roleBaseName.toLowerCase();
    setImage($('#your_exp_pane'), roleImageName);

    setTrim(roleInfo.roleColor);
}

function prepObserverMode(){
    if(!window.gameState.isObserver)
        return;

    $('#time h2').css('color', 'black');
    $('#day h2').css('color', 'black');
    $('#action_nav_pane').hide();

    $('#main_table').css('left', '35%');
    $('#main_table').css('width', '35%');

    $('#your_info_wrapper').css('width', '31%');
}

function refreshActionUL(){
    const actions = window.gameState.actions;

    function actionClick(e){
        e = $(this);
        const index = parseInt(e.attr('name'), 10);
        const ability = actions.actionList[index].command.toLowerCase();

        window.gameState.playerLists.type.forEach(value => {
            if(value.toLowerCase() === ability)
                window.gameState.commandsIndex = index;
        });
        if(ability !== 'send')
            actionIndex = index;


        selected = null;
        selected2 = null;
        deadSelected = null;
        left = null;
        right = null;
        savedOpts = [null, null, null];

        refreshActionUL();
        refreshPlayers();
    }

    const submittedActions = actions.actionList.map((action, i) => ({
        text: action.text,
        italicized: (actionIndex === i && action.command.toLowerCase() !== 'send'),
        nameAttr: i.toString(),
    }));

    actionIndex = Math.min(actionIndex, submittedActions.length);


    $('#add_action')
        .off('click')
        .click(() => {
            left = null;
            right = null;
            selected = null;
            selected2 = null;
            deadSelected = null;
            savedOpts = [null, null, null];
            actionIndex++;
            refreshPlayers();
            $('#add_action').hide();
        });
    adjustActionPane();


    const actionContainer = $('#actions_content ol');
    actionContainer.empty();

    for(let i = 0; i < actions.actionList.length; i++){
        const action = submittedActions[i];
        const li = $('<li>');
        let text = action.text.replace('You will ', '');
        text = text.charAt(0).toUpperCase() + text.substr(1);
        li.html(text);
        if(action.italicized)
            li.css('font-style', 'italic');
        li.attr('name', action.nameAttr);
        li.click(actionClick);
        actionContainer.append(li);
    }
}

function refreshHost(){
    if(window.gameState.isHost())
        $('body').addClass('isHost');
    else
        $('body').removeClass('isHost');


    if(isFirstPhase())
        $('body').addClass('isFirstPhase');
    else
        $('body').removeClass('isFirstPhase');

    const navBar = $('.nav_bar');
    navBar.removeClass('fourButtonNav');
}

function goToGlobalLobbyPage(){
    $('#main').hide();
    $('.nav_bar').hide();
    $('#chat_ticker_expander').hide();
    $('#chat_ticker_pane').hide();
    store.dispatch(onLobbyLeaveAction());
    $('#root').css('z-index', 0);
}

function goToSetupPage(){
    bodySet();
    $('.nav_bar').hide();
    $('#chat_ticker_expander').hide();
    $('#chat_ticker_pane').hide();
    soundService.play();
    if(helpers.storageEnabled()){
        localStorage.removeItem('seenFeedbacks');
        seenFeedbacks.length = 0; // clears array
    }
}

async function goToGamePlayPage(){
    // eslint-disable-next-line max-len
    window.gameState.activeUserIDs = await gameUserService.getActiveUserIDs(window.gameState.gameID);
    $('#root').css('z-index', -1);
    $('#main').show();

    $('.nav_bar').show();
    $('.fourButtonNav').removeClass('fourButtonNav');
    if(helpers.isMobile())
        $('.chat_ticker').show();
    else
        $('.chat_ticker').hide();

    clearNavBarClasses();
    if(isTablet())
        $('body').addClass('tablet_game');
    else if(helpers.isMobile())
        $('body').addClass('mobile');
    bodySet();

    if(helpers.isMobile())
        $('#nav_tab_1 span').addClass('timerText');
    else
        $('#nav_tab_1 span').text('Info');

    $('#nav_tab_2 span').text('Role');
    $('#nav_tab_3 span').text('Voting');
    $('#nav_tab_4 span').text('Actions');
    if(!window.gameState.isOver()){
        $('#nav_tab_5 span').text('Settings');
    }else{
        $('#nav_tab_5 span').text('Exit');
        $('body').addClass('gameOver');
    }

    setDayIcon();
    $('#nav_tab_2 i').addClass('fa fa-book');
    $('#nav_tab_3 i').addClass('fa fa-edit');
    $('#nav_tab_4 i').addClass('fa fa-bolt');
    $('#nav_tab_5 i').addClass('fa fa-cog');

    $('.leaveButton').unbind();
    $('.leaveButton').click(() => {
        leaveGame();
        resetGameState();
    });

    changeableDisplayClick('game_info');
    changeableDisplayClick('your_info');
    changeableDisplayClick('vote_recap');
    changeableDisplayClick('action_nav');
    $('.settings_button').unbind();
    if(!window.gameState.isOver())
        changeableDisplayClick('settings');
    else
        $('.settings_button').click(leaveGame);

    SETTINGS_KEYS.forEach(value => {
        if(helpers.storageEnabled()){
            $(`.${value}`).removeClass('selected_toggle');
            if(!soundService.defaultSound(value))
                $(`.${value}.left_toggle_border`).addClass('selected_toggle');
            else
                $(`.${value}.right_toggle_border`).addClass('selected_toggle');
        }
        $(`.${value}`).click(function(e){
            e = $(this);
            if(e.hasClass('selected_toggle'))
                return;
            $(`.${value}`).removeClass('selected_toggle');
            e.addClass('selected_toggle');

            setSettingValue(value, e.hasClass('right_toggle_border'));
            if(value.indexOf('_music_enabled') !== -1)
                if(e.hasClass('right_toggle_border'))
                    soundService.play();
                else if((value === 'day_music_enabled' && window.gameState.isDay())
                    || (value === 'night_music_enabled' && !window.gameState.isDay())){
                    soundService.pause();
                }
        });
    });

    $('.puppetClick').off('click')
        .on('click', () => {
            $('#puppetList').show();
        });


    $('#chat_ticker_pane, #chat_ticker_expander').unbind();
    $('#chat_ticker_pane').click(function(){
        const chatTab = $(`#${$(this).attr('name')}`);
        chatTab.click();
    });

    $('#chat_ticker_expander').click(() => {
        if(!isOnMain())
            return;
        hideLeftPane();
        const prevSelected = $('.selected_nav');
        prevSelected.removeClass('selected_nav');
        prevSelected.addClass('unselected_nav');
        $('.chat_pane').hide();
        $('#puppetList').hide();
        chatView.hideChatInput();
        $('#chat_tabs_wrapper').show();
        $('.chat_ticker').hide();
    });

    function visChange(){
        if(window.gameState.scrollToBottom)
            pushServerUnread($('.activeTab').attr('name'));
    }

    const visProp = getHiddenProp();
    if(visProp){
        const evtname = `${visProp.replace(/[H|h]idden/, '')}visibilitychange`;
        document.addEventListener(evtname, visChange);
    }


    $('#nav_tab_3').click();
    soundService.play();

    new SimpleBar($('#messages')[0]); // eslint-disable-line no-new
}

function bodySet(){
    const body = $('body');
    body.removeClass('isFirstPhase');
}

function clearNavBarClasses(){
    $('body').removeClass('tablet_home tablet_setup tablet_game mobile gameOver');
}

function hasType(key, type){
    if(type)
        return (type.indexOf(key) >= 0);

    return false;
}

function isFirstPhase(){
    if(!window.gameState.started)
        return false;
    if(window.gameState.dayNumber === 0)
        return true;
    if(!window.gameState.isDay())
        return false;
    return (window.gameState.dayNumber === 1
        && window.gameState.getSetupModifierValue('DAY_START'));
}

let receivedFirstObject = false;
async function handleObject(object){
    const funcs = [];
    delete object.name;
    if(object.event)
        return require('./services/eventService').handle(object);
    if(object.guiUpdate){
        const gameState = window.gameState;
        if(!gameState.setup)
            await gameService.getGameStateAndChat();
        if(gameState.profile)
            roleInfoView.refresh();

        if(object.gameStart !== undefined){
            if(object.gameStart === true && gameState.started === false){
                store.dispatch(onHideEntityAction());
                gameState.isStarted = true;
            }
            gameState.started = object.gameStart;

            if(gameState.started && !isOnMain())
                funcs.push(goToGamePlayPage);
        }

        if(object.moderatorIDs){
            gameState.moderatorIDs = new Set(object.moderatorIDs);
            funcs.push(refreshHost);
        }

        if(object.isObserver !== undefined){
            gameState.isObserver = object.isObserver;
            if(gameState.isObserver)
                prepObserverMode();
        }
        if(object.joinID && gameState.joinID !== object.joinID)
            gameState.joinID = object.joinID;

        if(object.activeTeams !== undefined)
            gameState.activeTeams = object.activeTeams;

        if(hasType('rules', object.type)){
            // checks if new team is created
            gameState.factions = object.factions;

            if(gameState.started)
                funcs.push(setLastWillContainer);
        }

        if(hasType('roleInfo', object.type)){
            gameState.role = object.roleInfo;
            funcs.push(setRoleInfo);
            receivedFirstObject = hasType('rules', object.type) && !receivedFirstObject;
            receivedFirstObject = true;
        }

        if(hasType('graveYard', object.type)){
            gameState.graveyard = object.graveYard;
            funcs.push(setGraveYard);
        }

        if(object.isDay !== undefined){
            if(gameState.isDay() !== object.isDay){
                selected = null;
                selected2 = null;
                left = null;
                right = null;
                deadSelected = null;
                savedOpts = [null, null, null];
                setDayIcon();
            }
            refreshActionButtonText();
            if(!gameState.isDay()){
                $('#actions_pane').show();
                $('#forceEndNightButton').text('Force End Night');
            }else{
                $('#forceEndNightButton').text('Force End Day');
            }
        }

        refreshActionButtonText();

        if(hasType(J.activeTeams, object.type))
            gameState.activeTeams = object[J.activeTeams];

        if(hasType('playerLists', object.type)){
            if(hasType('actions', object.type))
                gameState.actions = object.actions;

            gameState.playerLists = object.playerLists;
            funcs.push(setMultiCommandButtons);
            funcs.push(refreshPlayers);
        }

        if(object.hasDayAction)
            gameState.hasDayAction = object.hasDayAction;

        if(hasType('dayLabel', object.type)){
            setDayLabel(object.dayLabel);
            gameState.dayNumber = object.dayNumber;
            $('#nav_tab_1 i').text(` ${gameState.dayNumber}`);
        }
        if(object.timer !== undefined)
            gameState.timer = object.timer;

        if(hasType('actions', object.type)){
            gameState.actions = object.actions;
            refreshActionUL();
        }

        if(object.lastWill){
            gameState.lastWill = object.lastWill;
            if(!hasTeamLastWill() || $('#your_exp_pane .settings_on').hasClass('selected_toggle')){
                const lwArea = $('#last_will_area');
                if(!lwArea.val().length)
                    lwArea.val(object.lastWill);
            }
        }

        if(object.teamLastWill){
            gameState.teamLastWill = object.teamLastWill;
            if(!gameState.getSetupModifierValue('LAST_WILL')
                || $('#your_exp_pane .settings_off').hasClass('selected_toggle')){
                const lwArea = $('#last_will_area');
                if(!lwArea.val().length)
                    lwArea.val(object.teamLastWill);
            }
        }

        if(object.ping)
            $('#ping')[0].play();

        for(let i = 0; i < funcs.length; i++)
            await funcs[i]();
        return;
    }
    if(object.speechContent)
        return soundService.speak(object.speechContent);
    if(object.warning)
        return setWarning(object.warning);
    if(object.unreadUpdate){
        const lookingAtChat = (isOnMain() && $('.activeTab')
            .attr('name') === object.unreadUpdate);
        if(window.gameState.scrollToBottom && lookingAtChat && $('.chat_pane')
            .is(':visible')
            && !isHidden() && object.count)
            pushServerUnread(object.unreadUpdate);
        else
            window.gameState.unreadChats[object.unreadUpdate] = object.count;
        return;
    }

    // resetchat
    if(object.chatReset){
        $('#chat_tabs').empty();
        window.gameState.chats = {};
        if(window.gameState.started)
            window.gameState.chatKeys = {};
        let chatKeys = object.chatReset;
        chatKeys = addOtherDayChats(chatKeys);

        let chatName;
        let chatObject;
        for(let c = 0; c < chatKeys.length; c++){
            chatObject = chatKeys[c];
            chatName = chatObject.chatName;
            if(chatObject.chatKey && window.gameState.started)
                window.gameState.chatKeys[chatName] = chatObject.chatKey;
            addTab(chatObject);
        }

        const element = $('#messages');
        element.empty();

        Object.keys(window.gameState.chats).forEach(chatKey => {
            window.gameState.chats[chatKey] = {
                voteCount: -1,
                messages: [],
            };
        });

        chatService.chatQueue.length = 0;
    }

    // var feedbackSize = window.gameState.feedback.length;

    let ret; // returns where the id of the chat tab that this was added to
    if(typeof(object.message) === 'string'){
        let chat;
        if(!window.gameState.isAlive && !hasAbility('Ghost'))
            chat = 'Dead Chat';
        else
            chat = 'Clues';
        ret = addToChat({
            text: object.message,
            chat: [chat],
            day: window.gameState.dayNumber,
            isDay: window.gameState.isDay(),
        });
    }else{
        let tempMessage;
        for(let i = 0; i < object.message.length; i++){
            tempMessage = addToChat(object.message[i]);
            if(object.message[i].mID)
                pushFeedback(object.message[i]);
            if(tempMessage)
                ret = tempMessage;
        }
        if(window.gameState.started && $('.activeTab').length === 0 && !helpers.isMobile())
            if(window.gameState.isAlive === 'Ghost')
                $('#chat_tabs div').last().click();
            else
                $('#chat_tabs div:nth-last-child(2)').last().click();
        // else
    }
    if(ret){
        if(!ret[0])
            return helpers.log('bad message', object);
        const chatID = ret[0].replace(' ', '');
        const message = ret[1];
        $('#chat_ticker_pane').attr('name', chatID);
        $('#chat_ticker_new_message').empty();
        chatView.fillEleWithMessage($('#chat_ticker_new_message'), message);
    }
}

function addOtherDayChats(chatKeys){
    chatKeys.reverse();
    for(let i = window.gameState.dayNumber - 1; i > 0; i--)
        chatKeys.push({
            chatName: `Day ${i}`,
            isDay: true,
            chatType: 'fas fa-sun',
        });


    if(!helpers.isMobile())
        chatKeys.reverse(); // unreverse it
    return chatKeys;
}

function setWarningCancel(){
    if(window.gameState.warningResolve)
        window.gameState.warningResolve();
}

function setWarning(text, length){
    if(!length)
        length = 4000;
    window.gameState.warning = window.gameState.warning.then(() => {
        $('#warningPopup').stop();
        $('#warningPopup').css('opacity', 1);
        $('#warningPopup').show();
        $('#warningPopup span').text(text);

        return new Promise((resolve => {
            setTimeout(resolve, length);
            if(!window.gameState.warningResolve)
                window.gameState.warningResolve = resolve;
            $('#warningPopup').fadeOut(length);
        })).then(() => {
            window.gameState.warningResolve = null;
            $('#warningPopup').hide();
            $('#warningPopup span').text('');
        });
    });
}

function nextPhase(){
    editTimer(0);
}

function editTimer(seconds){
    phaseService.setTimer(seconds);
}

async function leaveGame(){
    soundService.pause();

    try{
        await playerService.leave();
    }catch(response){
        return setWarning(response.errors[0], 6000);
    }
    resetGameState();
    setTrim(NORM_TEXT_COLOR);
    window.gameState.phase = null;
    refreshActionButtonText();

    goToGlobalLobbyPage();
    helpers.setPseudoURL('/');
}

// eslint-disable-next-line no-unused-vars
function peekRolesList(){
    const o = {};
    o.message = 'getRolesList';
    o.server = false;
    webSend(o);
}


function connectWebSocket(){
    if(window.socket)
        return Promise.resolve();

    if(socketConnectingPromise)
        return socketConnectingPromise;

    socketConnectingPromise = new Promise(resolve => {
        let host = window.location.origin.replace(/^http/, 'ws');
        let portNumber = '';
        if(host.includes('localhost'))
            portNumber = ':4502';
        else if(host.includes(':4000'))
            portNumber = ':4000';
        if(portNumber && !host.endsWith(portNumber))
            host += portNumber;
        const ws = new WebSocket(`${host}/napi`);

        ws.onmessage = msg => {
            if(!socketConnectingPromise){
                msg = JSON.parse(msg.data);
                handleObject(msg);
            }
        };

        ws.onopen = () => {
            requests.getBrowserAuthRequest()
                .then(authRequest => {
                    window.socket = ws;
                    webSend(authRequest);
                    socketConnectingPromise = null;
                    resolve();
                });
        };
        ws.onclose = () => {
            window.socket = null;
            setTimeout(connectWebSocket, 3000);
        };
    });
    return socketConnectingPromise;
}

function hideLeftPane(){
    $('.changeable_display').hide();
    const prevSelected = $('.selected_nav');
    prevSelected.removeClass('selected_nav');
    prevSelected.addClass('unselected_nav');
    if(helpers.isMobile() || isTablet()){
        $('#action_nav_pane').hide();
        $('#settings_pane').hide();
    }
}

function isFullScreen(){
    const doc = window.document;
    return doc.fullscreenElement
        || doc.mozFullScreenElement
        || doc.webkitFullscreenElement
        || doc.msFullscreenElement;
}

function toggleFullScreen(){ // variable was 'on'

    // const doc = window.document;
    // const docEl = doc.documentElement;
    //
    // const requestFullScreen = docEl.requestFullscreen
    //     || docEl.mozRequestFullScreen
    //     || docEl.webkitRequestFullScreen
    //     || docEl.msRequestFullscreen;
    // const cancelFullScreen = doc.exitFullscreen
    //     || doc.mozCancelFullScreen
    //     || doc.webkitExitFullscreen
    //     || doc.msExitFullscreen;
    //
    // if(on && requestFullScreen)
    //     requestFullScreen.call(docEl);
    // else if(cancelFullScreen)
    //     cancelFullScreen.call(doc);
    //
    //
    // if(isFullScreen())
    //     $('body').css('font-size', '40%');
    // else
    //     $('body').css('font-size', '100%');
}

function loadPreviousName(){
    const defaultName = 'guest';
    if(!helpers.storageEnabled())
        return defaultName;

    const prevName = localStorage.getItem('lastgameName');
    if(!prevName){
        if(user)
            return user.displayName;
        return defaultName;
    }

    user = {
        displayName: prevName,
    };

    return prevName;
}

function setTitle(x){
    document.title = x;
}

function getNormalTitle(){
    const url = window.location.href;
    if(url.indexOf('localhost') !== -1)
        return 'Dev Narrator';
    return 'The Narrator';
}

function resetTitle(){
    setTitle(getNormalTitle());
}

function setMiniIconBlue(){
    $('.miniIcon').addClass('miniIconHover');
    $('#miniIconVisual').attr('src', 'img/iconLogoLightBlue.png');
}

$(document).ready(() => {
    window.socket = null;
    $(document).on('mouseenter mouseleave', '.miniIcon', e => {
        e.stopPropagation();
        if(e.type === 'mouseenter'){
            setMiniIconBlue();
        }else{
            $('.miniIcon').removeClass('miniIconHover');
            $('#miniIconVisual').attr('src', 'img/iconLogoWhite.png');
        }
    });

    $('.miniIcon').mouseover(() => {
        $('.miniIcon').addClass('miniIconHover');
    });

    setTitle(getNormalTitle());
    setTimeout(() => {
        window.scrollTo(0, 1);
    }, 100);

    $('#chat_tabs').empty();

    $('#warningPopup i').click(setWarningCancel);

    if(helpers.isMobile() && $(document).height() / $(document).width() < 1.5)
        $('body').addClass('keyboardUp');


    $(window).resize(() => {
        if(helpers.isMobile() && $(document).height() / $(document).width() < 1.5)
            $('body').addClass('keyboardUp');
        else
            $('body').removeClass('keyboardUp');
    });

    // #game_info_pane


    $('.chat_input').keyup(function(e){
        if(parseInt(e.keyCode, 10) !== 13)
            return;
        e = $(this);
        let key;
        if(helpers.isOnLobby()){
            key = true;
        }else if(isOnMain()){
            const activeChat = $('.activeTab').attr('name');
            key = window.gameState.chatKeys[activeChat];
        }else{
            key = 'everyone';
            window.gameState.unreadChats = {};
        }

        const puppetText = $('#puppetText').text();
        const role = window.gameState.role;
        if(!key && hasPuppets())
            key = $('#puppetText').text();
        else if(role && puppetText !== role.displayName && puppetText)
            key = puppetText;


        if(e.val().length === 0 || window.gameState.isObserver || !key){
            e.val('');
            return false;
        }

        const message = `say ${key} ${e.val()}`;

        const o = {};
        o.action = false;
        o.message = message;
        webSend(o);

        e.val('');
        return false;
    });

    $('.lw_toggle').click(function(e){
        e = $(this);
        if(e.hasClass('selected_toggle') || $('#last_will_area').is(':visible'))
            return;
        $('.selected_toggle').removeClass('selected_toggle');
        e.addClass('selected_toggle');

        if(e.text() === 'Public')
            $('#last_will_area').val(window.gameState.lastWill || '');
        else
            $('#last_will_area').val(window.gameState.teamLastWill || '');
    });

    $('#last_will_button').click(function(e){
        e = $(this);
        const tArea = $('#last_will_area');
        if(tArea.is(':visible')){
            tArea.hide();
            e.text('Last Will');

            const lastWillText = tArea.val();

            let m;
            if($('#your_exp_pane').hasClass('lw_double'))
                if($('#your_exp_pane .selected_toggle').text() === 'Public'){
                    m = 'lastWill';
                }else{
                    m = 'teamLastWill';
                }
            else if(hasTeamLastWill())
                m = 'teamLastWill';
            else
                m = 'lastWill';


            window.gameState[m] = lastWillText;

            const o = {
                willText: lastWillText,
                message: m,
            };
            webSend(o);
        }else{
            tArea.show();
            e.text('Save');
        }
    });

    $('#end_night_button').on('click', () => {
        const button = $('#end_night_button');
        if(window.gameState.isOver())
            return leaveGame();
        if(window.gameState.isDay()){
            if(window.gameState.role.roleName === 'Mayor')
                webSend({
                    message: 'Reveal',
                });
            else
                webSend({
                    message: 'Burn',
                });

            button.hide();
        }else{
            webSend({
                message: 'end night',
            });
            const playerName = window.gameState.profile.name;
            const { playerMap } = window.gameState;
            const player = playerMap[playerName];
            player.endedNight = !player.endedNight;
            if(window.gameState.endedNight())
                setButtonText("I'm not ready");
            else
                setButtonText('Skip to Day');
            refreshPlayers();
        }
    });

    $('#forceEndNightButton').click(() => {
        nextPhase();
        $('#nav_tab_1').click();
    });

    chatView.refresh();

    new SimpleBar($('#chat_tabs')[0]); // eslint-disable-line no-new

    setTimer();

    $('body').click(() => {
        if(!isFullScreen()){
            if(helpers.isMobile())
                toggleFullScreen(true);
        }else if(!helpers.isMobile()){
            toggleFullScreen(false);
        }
        if(!soundService.isPlaying())
            soundService.speak(' ');

        soundService.play();
    });
});

function setSettingValue(key, val){
    if(helpers.storageEnabled())
        localStorage.setItem(key, val);
    else
        window.gameState[key] = val;
}

function changeableDisplayClick(elementID){
    $(`.${elementID}_button`).unbind();
    $(`.${elementID}_button`).click(() => {
        hideLeftPane();
        $(`.${elementID}_button`).removeClass('unselected_nav');
        $(`.${elementID}_button`).addClass('selected_nav');
        $(`#${elementID}_pane`).show();

        chatView.hideChatElements();
    });
}

function getFeedbackMinimizationToArea(mType){
    if(helpers.isMobile()){
        if(mType === 'DeathAnnouncement')
            return $('#nav_tab_1');
        if(mType === 'Feedback' || mType === 'SnitchAnnouncement' || mType === 'Happening')
            return $('#chat_ticker_expander_icon');
    }else{
        if(mType === 'Feedback' || mType === 'SnitchAnnouncement' || mType === 'Happening'){
            const ret = $('#Clues');
            if(ret.length)
                return ret;
            return $('#DeadChat');
        }if(mType === 'DeathAnnouncement')
            return $('#nav_tab_1');
    }

    return $('#nav_tab_2');
}

let feedbackPromise = Promise.resolve();

function minimizeFeedback(top, mID, mType){
    const options = {
        to: {
            width: 0,
            height: 0,
        },
        queue: false,
    };

    const toArea = getFeedbackMinimizationToArea(mType);
    const toX = toArea.offset().left + (toArea.width() / 2);
    const toY = toArea.offset().top + (toArea.height() / 2);

    const fromX = top.offset().left;
    const fromY = top.offset().top;

    feedbackPromise = feedbackPromise.then(() => new Promise((resolve => {
        top.effect('size', options, 700, resolve);
        top.animate({
            left: `+=${toX - fromX}`,
            top: `+=${toY - fromY}`,
            queue: false,
        }, 700);
    }))).then(() => {
        top.remove();

        if(helpers.storageEnabled()){
            let seen = localStorage.getItem('seenFeedbacks');
            if(!seen)
                seen = [];
            else
                seen = JSON.parse(seen);
            seen.push(mID);
            localStorage.setItem('seenFeedbacks', JSON.stringify(seen));
        }

        if(!$('.newFeedbackPopup').length)
            $('#feedbackBackdrop').hide();
    });
}

function pushRoleCardPopup(){
    if(!window.gameState.started)
        return;
    let bullets = ['Game has started.', 'Press \'Next\' when you\'re ready to see your role.'];
    getFeedbackPopup('Game has started!', 'citizen', bullets, 0, null);

    let rolePic;

    try{
        rolePic = window.gameState.profile.roleCard.abilities[0].command;
    }catch(err){
        rolePic = 'citizen';
    }

    bullets = roleInfoView.getRoleCardDetails();
    const { profile } = window.gameState;
    const teamName = window.gameState.setup.factionMap[profile.roleCard.factionID].name;
    const roleName = profile.roleCard.roleName;
    const title = `${teamName} ${roleName}`;
    getFeedbackPopup(title, rolePic, bullets, -1, null); // change -1 to roleCardUpdateCount
}

function pushFeedback(message){
    if(message.messageType === 'Happening')
        return;
    let title = '';
    if(message.isDay)
        title += 'Day';
    else
        title += 'Night';
    title += ' ';
    if(!message.isDay && message.messageType === 'Feedback')
        title += (message.day - 1);
    else
        title += message.day;

    const bullets = [message.text];
    if(message.extras)
        for(let i = 0; i < message.extras.length; i++)
            bullets.push(message.extras[i]);


    getFeedbackPopup(title, message.pic || 'citizen', bullets, message.mID, message.messageType);
}

function getFeedbackPopup(title, picture, bullets, mID, mType){
    if(storageService.getFeedbackWasSeen(mID))
        return;

    if(!roleImages.contains(picture))
        picture = 'citizen';

    if(seenFeedbacks.indexOf(mID) !== -1)
        return;
    seenFeedbacks.push(mID);
    $('#feedbackBackdrop').show();
    $('.feedbackNext').text('Next');
    const card = $('<div>').addClass('newFeedbackPopup');
    const header = $('<h1>').addClass('newFeedbackHeader').text(title);
    const pic = $('<img>')
        .addClass('feedbackPic')
        .attr('src', `rolepics/${picture}.png`)
        .attr('alt', 'Feedback pic');
    const bulletHolder = $('<ul>').addClass('newFeedbackContents');
    const doneButton = $('<button>').addClass('feedbackNext').text('Done');

    doneButton.click(() => {
        minimizeFeedback(card, mID, mType);
    });

    if(!bullets)
        bullets = ['really really really long test bullet point', 'shouldn\'t be on production'];


    let bullet;
    for(let i = 0; i < bullets.length; i++){
        bullet = bullets[i];
        if(!bullet)
            continue;
        if(typeof(bullet) === 'object'){
            let ret = '';
            for(let j = 0; j < bullet.length; j++){
                if(!j)
                    ret += ' ';
                const part = bullet[j];
                if(typeof(part) === 'string'){
                    ret += part;
                }else{
                    const ht = $('<span>');
                    ht.css('color', part.color);
                    ht.text(part.text);
                    ret += ht[0].outerHTML;
                }
            }
            bullet = ret;
        }
        bulletHolder.append($('<li>').html(bullet));
    }

    card.append(header).append(pic).append(bulletHolder).append(doneButton);

    card.insertAfter($('#feedbackBackdrop'));

    return card;
}

export {
    connectWebSocket,
    handleObject,
    webSend,
    loadPreviousName,

    goToGamePlayPage,
    goToGlobalLobbyPage,
    goToSetupPage,
    hideLeftPane,
    leaveGame,
    prepObserverMode,
    pushRoleCardPopup,
    refreshActionButtonText,
    refreshHost,
    refreshPlayers,
    setImage,
    setSettingValue,
    SETTINGS_KEYS,
    setWarning, // also known as setToast maybe?
};

window.addBots = playerService.addBots;
window.handleObject = handleObject;
window.editTimer = editTimer;
window.soundService = soundService;
window.storageService = storageService;
window.resetSetup = setupService.resetSetup;
