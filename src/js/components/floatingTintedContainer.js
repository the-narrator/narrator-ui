import React, { useEffect, useRef } from 'react';


export default function FloatingTintedContainer({ className = '', setClosed, children }){
    const focus = useRef(null);
    useEffect(() => {
        focus.current.focus();
    }, []);

    function handleKeyUp({ key }){
        if(key === 'Escape')
            return setClosed();
    }

    if(className)
        className += ' ';
    className += 'floatingTintedContainer';

    return (
        <div
            className="floatingTintedContainerBackground"
            onClick={setClosed}
            ref={focus}
            tabIndex={1} // should match the z-index property on floatingTintedContainerBackground?
            onKeyUp={handleKeyUp}
        >
            <div onClick={e => e.stopPropagation()} className={className}>
                {children}
            </div>
        </div>
    );
}
