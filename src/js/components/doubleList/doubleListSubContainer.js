import React from 'react';


export default function DoubleListSubContainer({ label, items }){
    return (
        <div className="doubleListSubContainer">
            <div className="doubleListHeader">{label}</div>
            <ul className="borderedSection doubleListContainerUL">
                {items.map(item => (
                    <div
                        key={item.value}
                        onClick={() => item.onClick(item.value)}
                        style={{ color: item.color }}
                    >{item.text}
                    </div>
                ))}
            </ul>
        </div>
    );
}
