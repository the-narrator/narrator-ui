import React from 'react';

import LabelAdornment from '../decorator/labelAdornment';


export default function LabeledSpan({ className = '', label, value }){
    const containerClassName = className ? [className] : [];
    containerClassName.push('labeledSpanContainer');

    return (
        <LabelAdornment className={className} label={label}>
            <div className="labeledSpanValue">
                {value}
            </div>
        </LabelAdornment>
    );
}
