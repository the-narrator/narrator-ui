import React, { Children, cloneElement } from 'react';
import { useHistory } from 'react-router-dom';

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';


export default function NavBar({ headerText, children }){
    const [isDrawerOpen, setIsDrawerOpen] = React.useState(false);
    const history = useHistory();

    children = Children.map(children,
        child => cloneElement(
            child, { closeDrawer: () => setIsDrawerOpen(false) },
        ));

    return (
        <div className="navBar">
            <div className="homeNav">
                <img
                    alt="Home"
                    className="navHomeIcon"
                    onClick={() => history.push('/')}
                    src="/img/iconLogoWhite.png"
                />
                <span className="navLabel">{headerText}</span>
            </div>
            <SwipeableDrawer
                anchor="right"
                open={isDrawerOpen}
                onClose={() => setIsDrawerOpen(false)}
                onOpen={() => setIsDrawerOpen(true)}
            >
                <div className="drawerNavBarItems">
                    {children}
                </div>
            </SwipeableDrawer>
            <i className="fas fa-bars navMenuIcon" onClick={() => setIsDrawerOpen(true)} />
            <div className="topNavBarItems">
                {children}
            </div>
        </div>
    );
}
