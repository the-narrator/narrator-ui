import React from 'react';

export default function NavBarItem({
    className, isOnWhite, iconClassName, onClick, children,
}){
    let borderClassName = 'navBarItem';
    if(isOnWhite)
        borderClassName += ' isOnWhite';
    if(className)
        borderClassName += ` ${className}`;

    let textClassName = 'navBarItemText';
    if(isOnWhite)
        textClassName += ' isOnWhite';

    const icon = iconClassName
        ? (<i className={`${iconClassName} navBarItemIcon`} />)
        : null;

    return (
        <div className={borderClassName} onClick={onClick}>
            {icon}
            <span className={textClassName}>{children}</span>
        </div>
    );
}
