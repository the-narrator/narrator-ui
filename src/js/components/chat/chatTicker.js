import React from 'react';

import PlayerIcon from '../playerIcon';


export default function ChatTicker({
    className = '', onExpand, sender, text,
}){
    if(className)
        className += ' ';
    return (
        <div className={`${className}chatTickerContainer`} onClick={onExpand}>
            <div className="chatTickerTextContainer">
                {sender && (
                    <>
                        <PlayerIcon color={sender.color} icon={sender.icon} />
                        <span
                            className="chatSpeakerName"
                            style={{ color: sender.color }}
                        >{sender.name}:
                        </span>
                        <span className="chatTickerText">{text}</span>
                    </>
                )}
            </div>
            {!sender && (<span className="chatTickerText">Be the first to say hello!</span>)}
            <i className="chatTickerMessageOpener fa fa-comments" />
        </div>
    );
}
