import React from 'react';

import PlayerIcon from '../playerIcon';


export default function ChatMessage({ createdAt, sender, text }){
    return (
        <div className="chatMessage">
            <PlayerIcon color={sender.color} icon={sender.icon} />
            <div className="chatMessageSpeakerTextContainer">
                <div className="chatSpeakerNameTime">
                    <span
                        className="chatSpeakerName"
                        style={{ color: sender.color }}
                    >{sender.name}
                    </span>
                    <span className="chatMessageTime">{getHourMinute(createdAt)}</span>
                </div>
                <span className="chatMessageText">{text}</span>
            </div>
        </div>
    );
}

// https://www.npmjs.com/package/javascript-time-ago
function getHourMinute(timeStamp){
    timeStamp = new Date(timeStamp);
    const minutes = pad(timeStamp.getMinutes());
    const hours = pad(timeStamp.getHours());
    return `[${hours}:${minutes}]`;
}

function pad(number){
    number = number.toString();
    if(number.length === 1)
        return `0${number}`;
    return number;
}
