import React from 'react';
import { connect } from 'react-redux';

import { onShowFactionRoleAction } from '../actions/payloads/panelViewActions';


function FactionRoleClickable({ factionRole, factionMap, showFactionRole }){
    const faction = factionMap[factionRole.factionID];
    return (
        <span
            className="roleHover"
            onClick={() => showFactionRole(factionRole.id, faction.id)}
            style={{ color: faction.color }}
        >
            {factionRole.name}
        </span>
    );
}

const mapStateToProps = ({ setupState }) => ({
    factionMap: setupState.factionMap,
});

const mapDispatchToProps = {
    showFactionRole: onShowFactionRoleAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(FactionRoleClickable);
