/* eslint-env browser */
const requests = require('../util/browserRequests');


async function setTimer(seconds){
    const args = {
        joinID: window.gameState.joinID,
        seconds,
    };
    const response = await requests.put('phases', args);
    window.gameState.timer = response.endTime;
}

module.exports = {
    setTimer,
};
