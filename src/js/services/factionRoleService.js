import store from '../reducers/store';
import {
    onFactionRoleCreateAction, onFactionRoleDeleteAction,
    onFactionRoleUpdateAction,
} from '../actions/payloads/setupActions';
import { MAX_PLAYER_COUNT } from '../util/constants';

const requests = require('../util/browserRequests');


export async function create(factionID, roleID){
    const args = { factionID, roleID };
    const serverResponse = await requests.post('factionRoles', args);
    const factionRole = serverResponse.response;
    transformFactionRole(factionRole);
    store.dispatch(onFactionRoleCreateAction(factionRole));
    return factionRole;
}

export async function deleteFactionRole(factionRoleID){
    await requests.delete(`factionRoles/${factionRoleID}`);
    store.dispatch(onFactionRoleDeleteAction(factionRoleID));
}

export function setFactionRoleReceived(factionRoleID, receivedFactionRoleID){
    return requests.put(`factionRoles/${factionRoleID}`, { receivedFactionRoleID });
}

export async function sync(factionRoleID){
    const serverResponse = await requests.get(`factionRoles/${factionRoleID}`);
    const factionRole = serverResponse.response;
    transformFactionRole(factionRole);
    store.dispatch(onFactionRoleUpdateAction(factionRole));
}

export function syncOtherRoles(filteredFactionRoleID, modifierName){
    return Object.values(store.getState().setupState.factionRoleMap)
        .filter(factionRole => factionRole.id !== filteredFactionRoleID)
        .filter(factionRole => factionRole.abilities
            .find(ability => ability.setupModifierNames.includes(modifierName)))
        .map(factionRole => sync(factionRole.id));
}

export function transformFactionRole(factionRole){
    factionRole.abilityMap = {};
    factionRole.abilities.forEach(ability => {
        factionRole.abilityMap[ability.id] = ability;
    });
}

export async function upsertAbilityModifier(factionRoleID, abilityID, modifierName, value){
    const args = {
        maxPlayerCount: MAX_PLAYER_COUNT,
        minPlayerCount: 0,
        name: modifierName,
        value,
    };
    await requests.post(
        `factionRoles/${factionRoleID}/abilities/${abilityID}/modifiers`, args,
    );
}

export async function upsertRoleModifier(factionRoleID, modifierName, value){
    const args = {
        maxPlayerCount: MAX_PLAYER_COUNT,
        minPlayerCount: 0,
        name: modifierName,
        value,
    };
    await requests.post(`factionRoles/${factionRoleID}/modifiers`, args);
}
