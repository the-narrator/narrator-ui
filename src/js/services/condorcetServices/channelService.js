/* eslint-env browser */
const helpers = require('../../util/browserHelpers');
const requests = require('../../util/browserRequests');

/*
 * Allowed params
 *
 * allowEveryone
 * allowSelfVote
 * allowSkipVote
 */

function getStanding(voterMetadata, options){
    return requests.post('condorcet', getCondorcetVoteRequest(voterMetadata, options));
}

async function getVotes(){
    const urlParams = helpers.getURLParameters();
    const threadID = urlParams.thread;
    try{
        const { response } = await requests.get(
            `channels/sc2mafia/condorcet?threadID=${threadID}`,
        );
        const { options, voterMetadata } = response;
        if(urlParams.allowSkipVote)
            options.push('Skip Day');
        if(!urlParams.allowSelfVote && !urlParams.allowEveryone)
            stopSelfVote(options, voterMetadata);
        Object.keys(voterMetadata).forEach(voter => {
            voterMetadata[voter].ranking = removeUnknowns(options, voterMetadata[voter].ranking)
                .filter(rank => rank.length);
        });

        return {
            options,
            serverResponse: await getStanding(voterMetadata, options),
            voterMetadata,
        };
    }catch(errors){
        window.alert(JSON.stringify(errors)); // eslint-disable-line no-alert
    }
}

module.exports = {
    getStanding,
    getVotes,
};

function getCondorcetVoteRequest(voterMetadata, options){
    const urlParams = helpers.getURLParameters();
    options = new Set(helpers.flattenList(options));
    const allowEveryone = urlParams.allowEveryone;

    const condorcetInput = {};
    Object.keys(voterMetadata)
        .forEach(voterName => {
            if(allowEveryone || options.has(voterName)){
                const { ranking } = voterMetadata[voterName];
                condorcetInput[voterName] = ranking;
            }
        });
    return condorcetInput;
}

function removeUnknowns(options, ranking){
    return ranking.map(rank => rank.filter(voteInRank => options.includes(voteInRank)));
}

function stopSelfVote(options, voterMetaData){
    options.forEach(option => {
        if(!voterMetaData[option])
            voterMetaData[option] = {
                ranking: [],
            };

        const voterData = voterMetaData[option];
        const submittedVotes = new Set(helpers.flattenList(voterData.ranking));
        const unsubmittedVotes = options.filter(candidate => candidate !== option
            && !submittedVotes.has(candidate));
        if(unsubmittedVotes.length)
            voterData.ranking.push(unsubmittedVotes);

        voterMetaData[option].ranking = voterMetaData[option].ranking
            .map(rank => rank.filter(voteInRank => voteInRank !== option));
        if(options.includes(option))
            voterMetaData[option].ranking.push([option]);
    });
}
