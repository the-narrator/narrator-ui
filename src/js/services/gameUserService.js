import store from '../reducers/store';

import { onGameUserActivityChange, onGameUserActivityLoad } from '../actions/payloads/gameActions';
/* eslint-env browser */
const requests = require('../util/browserRequests');

const playerView = require('../views/playerView');


export async function getActiveUserIDs(gameID){
    let userIDs;
    try{
        const serverResponse = await requests.get(
            `channels/browser/activeUserIDs?game_id=${gameID}`,
        );
        userIDs = new Set(serverResponse.response);
    }catch(err){
        userIDs = new Set();
    }
    store.dispatch(onGameUserActivityLoad(userIDs));
    return userIDs;
}

export function onUserStatusChange(obj){
    const { isActive, userID } = obj;
    const { activeUserIDs } = window.gameState;
    if(isActive)
        activeUserIDs.add(userID);
    else
        activeUserIDs.delete(userID);
    store.dispatch(onGameUserActivityChange(obj));
    const changedPlayer = Object.values(window.gameState.playerMap)
        .find(player => player.userID === userID);
    if(!changedPlayer)
        return;
    const targetable = playerView.getTargetable(changedPlayer.name);
    if(targetable)
        targetable.refreshActivityMarking();
}
