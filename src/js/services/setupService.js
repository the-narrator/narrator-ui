/* eslint-env jquery, browser */
import { keyBy } from 'lodash';
import store from '../reducers/store';
import { onSetupChangeAction } from '../actions/payloads/setupActions';

const requests = require('../util/browserRequests');


export async function getSetup(setupID){
    const serverResponse = await requests.get(`setups/${setupID}`);
    return serverResponse.response;
}

export async function getIteration(setupID, playerCount){
    const serverResponse = await requests.get(
        `setups/${setupID}/iterations?playerCount=${playerCount}`,
    );
    return serverResponse.response;
}

export async function getDefaultSetupID(userID){
    const userSetups = await getUserSetups(userID);
    if(userSetups.length)
        return userSetups.id;
    const newSetup = await createSetup();
    return newSetup.id;
}

export async function getFeaturedSetups(){
    const serverResponse = await requests.get('setups/featured');
    const setups = serverResponse.response;
    setups.sort((s1, s2) => s1.priority - s2.priority);
    return setups;
}

export async function getUserSetups(userID){
    const serverResponse = await requests.get(`setups?userID=${userID}`);
    return serverResponse.response;
}

export function isHostsSetup(){
    return window.gameState.setup.ownerID === window.gameState.userID;
}

export function postSetupChangeRefresh(setup){
    transformSetup(setup);
    store.dispatch(onSetupChangeAction(setup));
}

export async function resetSetup(gameID, userID){
    if(isHostsSetup()){
        const setupID = store.getState().setupsState.featuredSetups.find(s => s.id).id;
        await usePresetSetup(gameID, setupID);
    }
    const defaultSetupID = await getDefaultSetupID(userID);
    await deleteSetup(defaultSetupID);
    return useCustomSetup(gameID, userID);
}

export function transformSetup(setup){
    const factionRoleService = require('./factionRoleService');
    const roleMap = {};
    setup.roles.forEach(role => {
        roleMap[role.id] = role;
    });

    const factionMap = {};
    const factionRoleMap = {};
    setup.factions.forEach(faction => {
        factionMap[faction.id] = faction;
        faction.factionRoles.forEach(factionRole => {
            factionRoleMap[factionRole.id] = factionRole;
            factionRoleService.transformFactionRole(factionRole);
        });
    });

    const hiddenMap = keyBy(setup.hiddens, 'id');

    setup.factionMap = factionMap;
    setup.factionRoleMap = factionRoleMap;
    setup.hiddenMap = hiddenMap;
    setup.roleMap = roleMap;
    setup.modifiers = setup.setupModifiers;

    delete setup.setupModifiers;
    delete setup.factions;
    delete setup.hiddens;
    delete setup.roles;
    return setup;
}

export async function useCustomSetup(gameID, userID){
    const setupID = await getDefaultSetupID(userID);
    const serverResponse = await requests.put('setups', { setupID, gameID });
    postSetupChangeRefresh(serverResponse.response.setup);
    require('./factionService').openFirst();
}

export async function usePresetSetup(gameID, setupID){
    const payload = { setupID, gameID };
    const serverResponse = await requests.put('setups', payload);
    postSetupChangeRefresh(serverResponse.response.setup);
    require('./factionService').openFirst();
}

export async function createSetup(args = { name: 'My custom setup' }){
    const serverResponse = await requests.post('setups', args);
    return serverResponse.response;
}

export async function cloneSetup(setupID){
    const { response } = await requests.post(`setups/${setupID}/clone`);
    return response.id;
}

export async function createSetupIteration(setupID, spawns){
    return requests.post(`setups/${setupID}/iterations`, { spawns });
}

export function deleteSetup(setupID){
    return requests.delete(`setups/${setupID}`);
}
