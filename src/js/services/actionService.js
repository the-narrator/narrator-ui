import store from '../reducers/store';
import { onVotesUpdateAction } from '../actions/payloads/gameActions';
import { onActionRemoveAction, onActionsUpdateAction } from '../actions/payloads/profileActions';

const requests = require('../util/browserRequests');


export async function submit(action){
    if(typeof(action) === 'string')
        action = { message: action };
    const index = require('../index');
    let serverResponse;
    try{
        serverResponse = await requests.put('actions', action);
    }catch(err){
        return index.setWarning(err.errors[0]);
    }
    const { response } = serverResponse;
    store.dispatch(onActionsUpdateAction(response.actions));
    if(response.voteInfo)
        store.dispatch(onVotesUpdateAction(response.voteInfo));
    index.refreshPlayers();
}

export async function cancel(actionIndex, command){
    const index = require('../index');
    try{
        await requests.delete(`actions?actionIndex=${actionIndex}&command=${command}`);
    }catch(err){
        return index.setWarning(err.errors[0]);
    }
    store.dispatch(onActionRemoveAction(actionIndex));
    index.refreshPlayers();
}
