/* eslint-env jquery, browser */
const backupStorage = {};

export function getBoolean(key, defaultValue){
    if(!storageEnabled())
        return key in backupStorage ? backupStorage[key] : defaultValue;
    const value = localStorage.getItem(key);
    if(value === undefined)
        return defaultValue;
    return value === 'true';
}

export function getFeedbackWasSeen(feedbackID){
    const feedbacks = getJSON('seenFeedbacks') || [];
    return feedbacks.includes(feedbackID);
}

export function saveAdvancedConfigurationMode(isAdvanced){
    savePrimitiveToStorage('isSuperCustom', isAdvanced);
}

export function savePlayerName(playerName){
    savePrimitiveToStorage('lastGameName', playerName);
}

export function saveSeenFeedback(feedbackID){
    const feedbacks = getJSON('seenFeedbacks') || [];
    feedbacks.push(feedbackID);
    saveObjectToStorage('seenFeedbacks', feedbacks);
}

function getJSON(storageKey){
    let storageValue;
    if(storageEnabled()){
        storageValue = localStorage.getItem(storageKey);
        if(storageValue)
            return JSON.parse(storageValue);
    }else{
        return backupStorage[storageKey];
    }
}

function saveObjectToStorage(storageKey, storageValue){
    if(storageEnabled())
        localStorage.setItem(storageKey, JSON.stringify(storageValue));
    else
        backupStorage[storageKey] = storageValue;
}

function savePrimitiveToStorage(storageKey, storageValue){
    if(storageEnabled())
        localStorage.setItem(storageKey, storageValue);
    else
        backupStorage[storageKey] = storageValue;
}

function storageEnabled(){
    return typeof(Storage) !== 'undefined';
}
