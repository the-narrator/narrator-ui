import store from '../reducers/store';
import { onHostChangeAction } from '../actions/payloads/gameActions';

const requests = require('../util/browserRequests');


export function onHostChange(moderatorIDs){
    const index = require('../index');
    index.refreshHost();
    store.dispatch(onHostChangeAction(moderatorIDs));
}

export async function repick(repickTarget, gameID){
    const args = {
        repickTarget, // player name
        gameID,
    };
    const { response } = await requests.post('moderators/repick', args);
    return response;
}
