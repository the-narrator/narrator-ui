import store from '../reducers/store';
import {
    onSetupHiddenAddAction,
    onSetupHiddenRemoveAction,
} from '../actions/payloads/setupActions';
/* eslint-env browser */
const requests = require('../util/browserRequests');


export async function addSetupHidden(setupHidden){
    const serverResponse = await requests.post('setupHiddens', setupHidden);
    onSetupHiddenAdd(serverResponse.response);
    return serverResponse.response;
}

export function onSetupHiddenAdd(setupHidden){
    store.dispatch(onSetupHiddenAddAction(setupHidden));
}

export function onSetupHiddenRemove({ setupHiddenID }){
    store.dispatch(onSetupHiddenRemoveAction(setupHiddenID));
}

export async function deleteSetupHidden(setupHiddenID){
    await requests.delete(`setupHiddens/${setupHiddenID}`);
    onSetupHiddenRemove({ setupHiddenID });
}
