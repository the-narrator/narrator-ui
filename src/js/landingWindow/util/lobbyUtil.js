import React from 'react';
import { PHASE_NAME } from '../../util/constants';

const WIDE_HEADER = true;

export const LOBBY_TABLE_DEFINITION = joinAsPlayer => [
    ['Setup', game => game.setup.name],
    ['Size', getPlayerCountLabel],
    ['Time', getPhaseLengthLabel],
    ['Host', game => game.users.find(user => user.isModerator).name],
    ['Status', getGameStatus, WIDE_HEADER],
    ['Action', game => getActionIcon(game, joinAsPlayer)],
];

export function isFull(game){
    return game.players.length >= game.setup.maxPlayerCount;
}

function getPlayerCountLabel({ setup }){
    const { minPlayerCount, maxPlayerCount } = setup;
    if(minPlayerCount === maxPlayerCount)
        return minPlayerCount;
    return `${minPlayerCount} - ${maxPlayerCount}`;
}

function getPhaseLengthLabel({ modifiers }){
    const dayLength = modifiers.DAY_LENGTH_START.value / 60;
    const nightLength = modifiers.NIGHT_LENGTH.value / 60;
    return (
        <span className="lobbyRowPhaseLength">
            <i className="fas fa-sun" />
            {dayLength}
            {' '}
            /
            {' '}
            <i className="fas fa-moon" />
            {nightLength}
        </span>
    );
}

function getGameStatus({ phase, ...game }){
    if(phase.name === PHASE_NAME.UNSTARTED){
        if(isFull(game))
            return 'Full';
        return 'Not Started';
    }
    if(phase.name === PHASE_NAME.FINISHED)
        return 'Finished';
    return 'In progress';
}

function getActionIcon(game, joinAsPlayer){
    const canJoin = !game.isStarted && !isFull(game);
    const iconName = canJoin ? 'fas fa-sign-in-alt' : 'far fa-eye';
    const onClick = canJoin ? joinAsPlayer
        : () => {};
    return (
        <div
            onClick={onClick}
            className="lobbyMainActionContainer"
        >
            <i className={`${iconName} lobbyMainActionIcon`} />
        </div>
    );
}
