import React from 'react';

import Dropdown from '../../../components/baseElements/dropdown';


export default function SetupDropdown({
    className, setups, onChange, setupID,
}){
    const featuredSetupItems = setups.map(setup => ({
        label: setup.name,
        value: setup.id,
    }));

    return (
        <Dropdown
            className={className}
            label="Setup"
            items={featuredSetupItems}
            onChange={onChange}
            value={setupID}
        />
    );
}
