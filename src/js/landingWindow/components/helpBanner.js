import React, { useState } from 'react';
import Toast from '../../components/toast';

export default function HelpBanner(){
    const [isOpen, setOpen] = useState(true);
    return (
        <Toast
            isOpen={isOpen}
            setClosed={() => setOpen(false)}
            type="info"
            text="Looking for a designer! Check out discord for more info."
        />
    );
}
