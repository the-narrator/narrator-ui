import React from 'react';

export default function GlobalChatHeader({ openGlobalUsers }){
    return (
        <div className="globalChatHeader">
            <span>Global Chat</span>
            <div className="globalUsersContainer">
                <span>Online 1</span>
                <i className="fas fa-users globalUsersIcon" onClick={openGlobalUsers} />
            </div>
        </div>
    );
}
