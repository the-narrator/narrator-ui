import React, { useState } from 'react';
import Button from '../components/baseElements/button';

import EverestInputPanel from './components/everestInputPanel';
import SetupHiddensContainer from '../lobbyWindow/components/setupEditorPane/setupHiddensContainer';
import IterationContainer from './components/iterationContainer';

import {
    getSetup, transformSetup, getIteration, createSetupIteration,
} from '../services/setupService';


const initialSetupState = {
    hiddenMap: {},
    setupHiddens: [],
};

export default function EverestContainer(){
    const [setup, setSetup] = useState(initialSetupState);
    const [playerCount, setPlayerCount] = useState(14);
    const [iteration, setIteration] = useState([]);
    return (
        <>
            <h1>Setup Balancer</h1>
            <EverestInputPanel
                onSetupChange={async setupID => {
                    const newSetup = await getSetup(setupID);
                    transformSetup(newSetup);
                    setIteration([]);
                    setSetup(newSetup);
                    const newIteration = await getIteration(setupID, playerCount);
                    setIteration(newIteration[0]);
                }}
                playerCount={playerCount}
                setPlayerCount={async newPlayerCount => {
                    setIteration([]);
                    setPlayerCount(newPlayerCount);
                    const newIteration = await getIteration(setup.id, newPlayerCount);
                    setIteration(newIteration[0]);
                }}
            />
            <div className="setupViewer">
                <IterationContainer setup={setup} iteration={iteration} />
                <SetupHiddensContainer
                    className="darkenedSetupPanel"
                    playerCount={playerCount}
                    setup={setup}
                />
            </div>
            <div className="buttonContainer">
                <Button
                    text="Save"
                    onClick={async() => {
                        const savedIteration = [...iteration];
                        setIteration([]);
                        await createSetupIteration(setup.id, savedIteration);
                        const newIteration = await getIteration(setup.id, playerCount);
                        setIteration(newIteration[0]);
                    }}
                />
                <Button
                    color="secondary"
                    text="Next"
                    onClick={async() => {
                        const newIteration = await getIteration(setup.id, playerCount);
                        setIteration(newIteration[0]);
                    }}
                />
            </div>
        </>
    );
}
