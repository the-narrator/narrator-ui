export const DialogActions = {
    HIDE_ACTION_DIALOG: 'dialog/HIDE_ACTION_DIALOG',
    SHOW_ACTION_DIALOG: 'dialog/SHOW_ACTION_DIALOG',
};

export function showDialogAction(payload){
    return {
        type: DialogActions.SHOW_ACTION_DIALOG,
        payload,
    };
}

export function hideDialogAction(){
    return {
        type: DialogActions.HIDE_ACTION_DIALOG,
    };
}
