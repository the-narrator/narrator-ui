import { joinByGameID as joinByLobbyID } from '../services/playerService';
import { repick } from '../services/moderatorService';

import { onGameUpdate, onHostChangeAction } from './payloads/gameActions';

export function joinAsPlayerAction(lobbyID, playerName){
    return async dispatch => {
        const game = await joinByLobbyID(lobbyID, playerName);
        dispatch(onGameUpdate(game));
    };
}

export function repickAction(repickTargetName, gameID){
    return async dispatch => {
        const { moderatorIDs } = await repick(repickTargetName, gameID);
        dispatch(onHostChangeAction(new Set(moderatorIDs)));
    };
}
