import { CondorcetActions } from './condorcetActions';

export const initialState = {
    codeGenFocus: undefined,
    options: [],
    standing: [],
    voterMetadata: {},
};

export default function condorcetReducer(state = initialState, action){
    switch (action.type){
    case CondorcetActions.ON_CODE_GEN_CLICK:
        return { ...state, codeGenFocus: action.payload };
    case CondorcetActions.ON_NEW_ORDER:
        return updateVoterRanking(state, action.payload.voter, action.payload.newOrder);
    case CondorcetActions.ON_NEW_STANDING:
        return { ...state, standing: action.payload };
    case CondorcetActions.ON_THREAD_LOAD:
        return { ...state, ...action.payload };
    default:
        return state;
    }
}

function updateVoterRanking(state, updatedVoter, newRanking){
    return {
        ...state,
        voterMetadata: {
            ...state.voterMetadata,
            [updatedVoter]: {
                url: state.voterMetadata[updatedVoter].url,
                ranking: newRanking.map(x => [x]),
            },
        },
    };
}
