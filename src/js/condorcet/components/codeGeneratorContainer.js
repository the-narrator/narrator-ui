/* eslint-env browser */
import React, { useRef } from 'react';
import { connect } from 'react-redux';

import Button from '../../components/baseElements/button';


function CodeGeneratorContainer({ codeGenFocus, voterMetadata }){
    const textAreaRef = useRef();
    if(!codeGenFocus)
        return null;
    const spoilerContents = voterMetadata[codeGenFocus].ranking.join(',');
    const spoilerText = `[SPOILER=cvote]${spoilerContents}[/SPOILER]`;
    return (
        <div className="codeGeneratorSection">
            <textarea ref={textAreaRef} className="codeTextArea" readOnly value={spoilerText} />
            <Button
                className="codeTextButton"
                onClick={() => {
                    textAreaRef.current.select();
                    document.execCommand('copy');
                }}
                text="Copy to clipboard"
            />
        </div>
    );
}

const mapStateToProps = state => state.rankings;

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CodeGeneratorContainer);
