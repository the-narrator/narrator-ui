import React from 'react';

import Button from '../../components/baseElements/button';

import GradientList from './gradientList';


export default function GradientContainer({
    buttonClassName = '', containerClassName = '', onButtonClick, onNewOrder, standing, title,
}){
    return (
        <div className={`${containerClassName} gradientContainer`}>
            <span className="gradientHeader">{title}</span>
            <GradientList onNewOrder={onNewOrder} standing={standing} />
            {onNewOrder
                ? (
                    <Button
                        onClick={onButtonClick}
                        className={`${buttonClassName} gradientButton`}
                        text="Create Vote Code"
                    />
                )
                : ''}
        </div>
    );
}
