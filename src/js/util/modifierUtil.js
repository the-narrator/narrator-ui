/* eslint-env browser */


export function getGameModifierValue(name, gameState = window.gameState, defaultValue){
    const modifier = gameState.modifiers[name];
    return modifier ? modifier.value : defaultValue;
}

export function getSetupModifier(name, setup = window.gameState.setup){
    return setup.modifiers.find(modifier => modifier.name === name);
}

export function getSetupModifierValue(name, setup = window.gameState.setup, defaultValue){
    const modifier = setup.modifiers.find(m => m.name === name);
    if(modifier)
        return modifier.value;
    return defaultValue;
}
