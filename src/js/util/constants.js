export const SKIP_DAY = 'Skip Day';
export const NOT_VOTING = 'Not Voting';

export const MAX_PLAYER_COUNT = 255;

export const MESSAGE_TYPE = {
    LOBBY_CHAT_MESSAGE: 'lobbyChatMessage',
};

export const PANEL_VIEWS = {
    HIDDEN: 'hidden',
    FACTION: 'faction',
    FACTION_ROLE: 'factionRole',
    PLAYER: 'player',
};

export const VOTE_SORT_TYPES = {
    ALPHA: 'alpha',
    VOTE_COUNT: 'voteCount',
};

export const RANDOMS_SELECTED_FACTION_ID = -1;

export const VOTE_SYSTEM_PLURALITY = 1;
export const VOTE_SYSTEMS = [{
    label: 'Plurality',
    value: 1,
}, {
    label: 'Diminishing Pool',
    value: 2,
}, {
    label: 'Multi-Vote Pluraliity',
    value: 3,
}];

export const VOTE_VIEW_TYPES = {
    IN_SYNC: 'IN_SYNC',
    PENDING_CONFIRM: 'PENDING_CONFIRM',
    PENDING_CANCEL: 'PENDING_CANCEL',
};

export const PHASE_NAME = {
    FINISHED: 'FINISHED',
    NIGHT: 'NIGHT_ACTION_SUBMISSION',
    TRIAL: 'TRIAL_PHASE',
    VOTES_OPEN: 'VOTE_PHASE',
    UNSTARTED: 'UNSTARTED',
};

export const FORUMS_LINK = 'https://www.sc2mafia.com/forum/forumdisplay.php/442-Narrator-Discussion';
export const DISCORD_LINK = 'https://discord.gg/MDpPMaW';
