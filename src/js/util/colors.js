const background = '#222222';

module.exports = {
    background,

    green: '#06D6A0',
    white: '#F7F7F7',
    yellow: '#F9C22E',

    selectedIconColor: '#F9C22E',

    darkTextColor: background,
    whiteTextColor: '#F7EDE2',
};
