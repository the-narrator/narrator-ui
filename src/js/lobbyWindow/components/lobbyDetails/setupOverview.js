import React from 'react';
import { connect } from 'react-redux';

import LabeledSpan from '../../../components/nav/labledSpan';

import FeaturedSetupsDropdown from './featuredSetupsDropdown';

import { isModerator as getIsModerator } from '../../../util/gameUserUtil';


function SetupOverview({
    isModerator, maxPlayerCount, minPlayerCount, setupName, showSetup,
}){
    return (
        <>
            <div className="lobbyDetailsSection">
                {isModerator
                    ? <FeaturedSetupsDropdown className="lobbyOverviewSetupDropdown wideDropdown" />
                    : (
                        <LabeledSpan
                            className="lobbyWindowElement wideDropdown"
                            label="Setup"
                            value={setupName}
                        />
                    )}
                <i className="lobbyDetailsSetupExpander fas fa-search" onClick={showSetup} />
            </div>
            <div className="lobbyDetailsSection">
                <LabeledSpan
                    className="lobbyWindowElement"
                    label="Min player count"
                    value={minPlayerCount}
                />
                <LabeledSpan
                    className="lobbyWindowElement"
                    label="Max player count"
                    value={maxPlayerCount}
                />
            </div>
        </>
    );
}

const mapStateToProps = ({ gameState, session, setupState }) => ({
    isModerator: getIsModerator(session.userID, gameState.users),
    maxPlayerCount: setupState.maxPlayerCount,
    minPlayerCount: setupState.minPlayerCount,
    setupName: setupState.name,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SetupOverview);
