import React from 'react';

import LobbyDetailsTextSubsection from './lobbyDetailsTextSubsection';

import { getGameModifierValue } from '../../../util/modifierUtil';
import { getDurationObject } from '../../phaseLengthUtil';


export default function PhaseDetails({ isExpanded, gameModifiers, openEditor }){
    const items = [{
        isShowing: true,
        labelf: valueUnit => `${valueUnit} day.`,
        name: 'DAY_LENGTH_START',
        orElseLabel: 'No vote phase',
    }, {
        isShowing: true,
        labelf: valueUnit => `${valueUnit} night.`,
        name: 'NIGHT_LENGTH',
        orElseLabel: 'No night phase',
    }, {
        isShowing: true,
        labelf: valueUnit => `${valueUnit} of discussion.`,
        name: 'DISCUSSION_LENGTH',
        orElseLabel: 'No discussion',
    }, {
        isShowing: true,
        labelf: valueUnit => `${valueUnit} for trial defenses.`,
        name: 'TRIAL_LENGTH',
        orElseLabel: 'No trials',
    }, {
        isShowing: isExpanded
            && getGameModifierValue('DAY_LENGTH_DECREASE', { modifiers: gameModifiers }) !== 0,
        labelf: valueUnit => `Day length will always be at least ${valueUnit}.`,
        name: 'DAY_LENGTH_MIN',
    }, {
        isShowing: isExpanded
            && getGameModifierValue('DAY_LENGTH_DECREASE', { modifiers: gameModifiers }) !== 0,
        labelf: valueUnit => `Next day ${valueUnit} minutes shorter than the previous.`,
        name: 'DAY_LENGTH_DECREASE',
    }].filter(({ isShowing }) => isShowing)
        .map(modifier => ({
            key: modifier.name,
            text: getPhaseModifierText(modifier, gameModifiers),
        }));
    return (
        <LobbyDetailsTextSubsection label="Phase Settings" items={items} openEditor={openEditor} />
    );

    //     , {
    //     isShowing: isExpanded && isThiefSpawnable,
    //     labelf: valueUnit => `${valueUnit} for pregame role picking.`,
    //     name: 'ROLE_PICKING_LENGTH',
    // }];
}

function getPhaseModifierText({ orElseLabel, labelf, name }, modifiers){
    const gameModifier = modifiers[name];
    if(!gameModifier || gameModifier.value === 0)
        return orElseLabel;
    const duration = getDurationObject(gameModifier.value);
    const unit = duration.label;
    const value = gameModifier.value / duration.value;
    return labelf(`${value} ${unit}`);
}
