import React from 'react';


export default function LobbyDetailsTextSubsection({ items, label, openEditor }){
    return (
        <div className="lobbyDetailsTextSubsection">
            <div className="lobbyDetailsTextHeaderContainer">
                <span className="lobbyTextDetailsHeader">{label}</span>
                {openEditor && (<i className="fas fa-cogs" onClick={openEditor} />)}
            </div>
            <ul className="lobbyOverviewDetails">
                {items.map(({ key, text }) => (
                    <li key={key}>
                        <span>{text}</span>
                    </li>
                ))}
            </ul>
        </div>
    );
}
