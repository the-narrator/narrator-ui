import React from 'react';
import { connect } from 'react-redux';

import TextInput from '../../../components/baseElements/textInput';

import GameFormatDropdown from '../../../landingWindow/components/gameCreate/gameFormatDropdown';
import LobbyOverviewToggle from './lobbyEditorToggle';

import { upsert as upsertGameModifier } from '../../../services/gameModifierService';
import { getGameModifierValue, getSetupModifierValue } from '../../../util/modifierUtil';


function MiscModifiersEditor({ isShowingAdvanced, gameState }){
    return (
        <div className="lobbyOverviewMiscContainer">
            <GameFormatDropdown
                className="lobbyWindowElement wideDropdown"
                onChange={value => upsertGameModifier('CHAT_ROLES', value)}
                value={getGameModifierValue('CHAT_ROLES', gameState)}
            />
            <LobbyOverviewToggle label="Last Will" modifierName="LAST_WILL" />
            {isShowingAdvanced && (
                <>
                    <LobbyOverviewToggle label="Omniscient Dead" modifierName="OMNISCIENT_DEAD" />
                    <LobbyOverviewToggle label="Auto day parity" modifierName="AUTO_PARITY" />
                    <TextInput
                        label="Charge variance"
                        onChange={value => upsertGameModifier('CHARGE_VARIABILITY', value)}
                        type="number"
                        value={getSetupModifierValue('CHARGE_VARIABILITY', gameState.setup)}
                    />
                    {/* <TextInput */}
                    {/*    label="Chat rate" */}
                    {/*    onChange={value => upsertGameModifier('CHAT_RATE', value)} */}
                    {/*    type="number" */}
                    {/*    value={getGameModifierValue('CHAT_RATE', gameState)} */}
                    {/* /> */}
                </>
            )}
        </div>
    );
}

const mapStateToProps = ({ gameState }) => ({
    gameState,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(MiscModifiersEditor);
