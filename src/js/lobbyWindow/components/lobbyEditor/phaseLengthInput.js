import React, { useEffect, useState } from 'react';

import LabelAdornment from '../../../components/decorator/labelAdornment';
import TextInput from '../../../components/baseElements/textInput';
import Dropdown from '../../../components/baseElements/dropdown';

import { getDurationObject, TIME_LENGTH } from '../../phaseLengthUtil';
import { getGameModifierValue } from '../../../util/modifierUtil';


const items = Object.values(TIME_LENGTH);
items.sort((i1, i2) => i1.value - i2.value);

export default function PhaseLengthInput({
    modifierLabel, modifierName, timeGameModifiers, upsertGameModifier,
}){
    const seconds = getGameModifierValue(modifierName, { modifiers: timeGameModifiers });

    const [dropdownValue, setDropdownValue] = useState(TIME_LENGTH.MIN.value);
    const [inputValue, setInputValue] = useState(0);
    useEffect(() => {
        if(seconds === undefined)
            return;
        const { value } = getDurationObject(seconds);
        setDropdownValue(value);
        setInputValue(seconds / value);
    }, [seconds]);

    function onBlur(){
        if(isInRange(modifierName, inputValue * dropdownValue))
            return upsertGameModifier(
                modifierName, inputValue * dropdownValue,
            );
        setInputValue(seconds / dropdownValue);
    }

    const visibleInputValue = Number.isInteger(inputValue)
        ? inputValue : '';
    return (
        <LabelAdornment label={modifierLabel}>
            <div className="lobbyWindowElement phaseLengthEditorContainer">
                <TextInput
                    containerClassName="phaseLengthEditorInput"
                    disableUnderline
                    onBlur={onBlur}
                    onChange={setInputValue}
                    type="number"
                    value={visibleInputValue}
                />
                <Dropdown
                    disableUnderline
                    items={items}
                    onChange={newValue => {
                        let newSeconds = inputValue * newValue;
                        setDropdownValue(newValue);
                        if(!isInRange(modifierName, newSeconds)){
                            setInputValue(60 / newValue);
                            newSeconds = 60;
                        }
                        return upsertGameModifier(
                            modifierName, newSeconds,
                        );
                    }}
                    value={dropdownValue}
                />
            </div>
        </LabelAdornment>
    );
}

function isInRange(modifierName, value){
    if(!Number.isInteger(value))
        return false;
    if(['DAY_LENGTH_MIN', 'DAY_LENGTH_START'].includes(modifierName) && value < 60)
        return false;
    return modifierName !== 'ROLE_PICKING_LENGTH' || value > 30;
}
