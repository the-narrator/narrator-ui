import React from 'react';
import { connect } from 'react-redux';
import Dropdown from '../../../components/baseElements/dropdown';

import { VOTE_SYSTEMS } from '../../../util/constants';

import { updateGameModifierAction } from '../../../actions/gameActions';


function VoteSystemsDropdown({ className, updateGameModifier, voteSystemModifier = {} }){
    return (
        <Dropdown
            className={className}
            items={VOTE_SYSTEMS}
            label="Vote System"
            onChange={value => updateGameModifier('VOTE_SYSTEM', value)}
            value={voteSystemModifier.value || ''}
        />
    );
}

const mapStateToProps = ({ gameState }) => ({
    voteSystemModifier: gameState.modifiers.VOTE_SYSTEM,
});

const mapDispatchToProps = {
    updateGameModifier: updateGameModifierAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(VoteSystemsDropdown);
