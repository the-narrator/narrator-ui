import React, { useState } from 'react';
import { connect } from 'react-redux';

import { keyBy } from 'lodash';

import Toggle from '../../../components/baseElements/toggle';

import Toast from '../../../components/toast';

import { upsert as upsertGameModifier } from '../../../services/gameModifierService';
import { upsert as upsertSetupModifier } from '../../../services/setupModifierService';


function LobbyEditorToggle({
    className = '', isSetupEditable, label, gameModifiers, modifierName, setupModifiers,
}){
    const [error, setError] = useState();
    const isGameModifier = gameModifiers[modifierName];
    function onChange(value){
        if(isGameModifier)
            return upsertGameModifier(modifierName, value);

        if(isSetupEditable)
            return upsertSetupModifier(modifierName, value);
        setError('Cannot edit frozen setup.');
    }

    const value = getValue(modifierName, gameModifiers, setupModifiers);
    if(className)
        className += ' ';

    return (
        <>
            <Toggle
                className={`${className}lobbyWindowElement`}
                label={label}
                isRightSelected={value}
                onChange={onChange}
            />

            <Toast
                isOpen={!!error}
                setClosed={setError}
                text={error || ''}
                type="error"
            />
        </>
    );
}

const mapStateToProps = ({ gameState, setupState }) => ({
    isSetupEditable: setupState.isEditable,
    setupModifiers: keyBy(setupState.modifiers, 'name'),
    gameModifiers: gameState.modifiers,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LobbyEditorToggle);

function getValue(modifierName, gameModifiers, setupModifiers){
    if(gameModifiers[modifierName])
        return gameModifiers[modifierName].value;
    if(setupModifiers[modifierName])
        return setupModifiers[modifierName].value;
    return false;
}
