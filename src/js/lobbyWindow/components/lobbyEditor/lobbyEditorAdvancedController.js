import React, { Children, cloneElement, useState } from 'react';


export default function LobbyEditorAdvancedController({ children, hidden }){
    const [isExpanded, setIsExpanded] = useState(false);
    const [iconClassName, text] = isExpanded
        ? ['lobbyOverviewAdvancedIcon fas fa-chevron-up', 'Hide advanced']
        : ['lobbyOverviewAdvancedIcon fas fa-chevron-down', 'Show advanced'];
    return (
        <div className="lobbyEditorAdvancedController">
            <div>
                {Children.map(children, child => (
                    cloneElement(child, { isShowingAdvanced: isExpanded })
                ))}
            </div>
            {!hidden && (
                <span
                    className="lobbyShowAdvancedText"
                    onClick={() => setIsExpanded(!isExpanded)}
                >
                    {text}<i className={iconClassName} />
                </span>
            )}
        </div>
    );
}
