import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import ParticipantOverview from './participantOverview';

import { getActiveUserIDs } from '../../../services/gameUserService';

import { hashStringToInt } from '../../../util/browserHelpers';
import { getPlayerName, getUserColor, getUserIcon } from '../../../util/playerUtil';


function getOverviewColor(gameID, name, userID){
    if(userID)
        return getUserColor(gameID, userID);
    return getUserColor(gameID, hashStringToInt(name));
}

function getOverviewIcon(gameID, name, userID){
    if(userID)
        return getUserIcon(gameID, userID);
    return getUserIcon(gameID, hashStringToInt(name));
}

function ParticipantsContainer({
    activeUserIDs, gameID, playerMap, users,
}){
    useEffect(() => {
        if(gameID)
            getActiveUserIDs(gameID);
    }, [gameID]);

    const moderatorIDs = new Set(users.filter(u => u.isModerator).map(u => u.id));
    const players = Object.values(playerMap);
    const playerUserIDs = new Set(players.filter(p => p.userID).map(p => p.userID));
    const playerEntities = players.map(bot => ({
        color: getOverviewColor(gameID, bot.name, bot.userID),
        key: bot.userID ? `@${bot.userID}` : bot.name,
        icon: getOverviewIcon(gameID, bot.name, bot.userID),
        isActive: activeUserIDs.has(bot.userID),
        isModerator: moderatorIDs.has(bot.userID),
        id: bot.id,
        name: bot.name,
        userID: bot.userID,
    }));
    playerEntities.sort(
        (p1, p2) => p1.name.toLowerCase().localeCompare(p2.name.toLowerCase()),
    );

    const observers = users
        .filter(u => !playerUserIDs.has(u.id))
        .map(user => ({
            color: getUserColor(gameID, user.id),
            icon: getUserIcon(gameID, user.id),
            isActive: activeUserIDs.has(user.id),
            isBot: false,
            isModerator: user.isModerator,
            name: getPlayerName(user.id, playerMap, user),
            userID: user.id,
        }));

    return (
        <div className="playersOverview">
            {!!playerEntities.length && (
                <>
                    <div className="playersOverviewHeader">
                Players ({playerEntities.length})
                    </div>
                    {playerEntities.map(
                        playerEntity => (
                            <ParticipantOverview
                                key={playerEntity.key}
                                participant={playerEntity}
                            />
                        ),
                    )}
                </>
            )}
            {!!observers.length && (
                <>
                    <div className="playersOverviewHeader">
                Observers ({observers.length})
                    </div>
                    {observers.map(
                        observer => (
                            <ParticipantOverview
                                key={observer.userID}
                                participant={observer}
                            />
                        ),
                    )}
                </>
            )}
        </div>
    );
}

const mapStateToProps = ({ gameState, session }) => ({
    activeUserIDs: gameState.activeUserIDs,
    gameID: gameState.id,
    playerMap: gameState.playerMap,
    userID: session.userID,
    users: gameState.users,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ParticipantsContainer);
