import React from 'react';
import { connect } from 'react-redux';

import Button from '../../../components/baseElements/button';

import { repickAction } from '../../../actions/gameUserActions';
import { deletePlayer, kickUser } from '../../../services/playerService';

import { isModerator as getIsModerator } from '../../../util/gameUserUtil';


function ParticipantButtons({
    gameID, participant, repick, user,
}){
    function onKickClick(){
        if(participant.isBot)
            return deletePlayer(participant.id, participant.name, gameID);
        return kickUser(participant.userID);
    }

    const showRepickButton = !participant.isBot;
    const repickButtonText = user.isModerator ? 'Set Host' : 'Repick';

    const canShowKick = user.isModerator && user.id !== participant.userID;
    return (
        <span className="playerOverviewButtons">
            {canShowKick && (<Button onClick={onKickClick}>Kick</Button>)}
            {showRepickButton && (
                <Button
                    onClick={e => {
                        repick(participant.name, gameID);
                        e.stopPropagation();
                    }}
                >
                    {repickButtonText}
                </Button>
            )}
        </span>
    );
}

const mapStateToProps = ({ gameState, session }) => ({
    gameID: gameState.id,
    user: {
        id: session.userID,
        isModerator: getIsModerator(session.userID, gameState.users),
    },
});

const mapDispatchToProps = {
    repick: repickAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(ParticipantButtons);
