import React from 'react';
import DraggableList from '../../../components/draggableList';

import { upsert as upsertFactionModifier } from '../../../services/factionModifierService';
import FloatingTintedContainer from '../../../components/floatingTintedContainer';


export default function WinPriorityContainer({ factions, isShowing, setClosed }){
    if(!isShowing)
        return null;

    factions = [...factions].sort(winPrioritySorter);
    const standing = factions.map(f => ({
        className: 'winPriorityChild',
        label: f.name,
        styleOverrides: {
            color: f.color,
        },
        value: f.id.toString(),
    }));

    function updateRankings(factionIDs){
        factionIDs.forEach((factionID, index) => {
            factionID = parseInt(factionID, 10);
            setPriority(factionID, index + 1);
        });
    }

    return (
        <FloatingTintedContainer setClosed={setClosed}>
            <div className="winPriorityHeader">Win priority editor</div>
            <div className="winPrioritySubtext">
                    Drag to reorder. Higher on the list wins ties.
            </div>
            <div className="winPriorityDraggableList">
                <DraggableList
                    items={standing}
                    onNewOrder={updateRankings}
                    title="Win priority editor"
                />
            </div>
        </FloatingTintedContainer>
    );
}

function setPriority(factionID, priority){
    return upsertFactionModifier(priority, 'WIN_PRIORITY', factionID);
}

function winPrioritySorter(f1, f2){
    return getWinPriority(f1) - getWinPriority(f2)
        || f1.name.localeCompare(f2.name);
}

function getWinPriority(faction){
    return faction.modifiers.find(modifier => modifier.name === 'WIN_PRIORITY').value;
}
