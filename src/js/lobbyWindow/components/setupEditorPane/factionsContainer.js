import React, { useState } from 'react';
import { connect } from 'react-redux';
import {
    onHideEntityAction, onShowFactionAction, onShowHiddenAction,
} from '../../../actions/payloads/panelViewActions';

import TitledListContainer from '../../../components/titledListContainer';

import FactionDetails from './factionDetails';
import FactionEditorContainer from './factionEditor/factionEditorContainer';
import WinPriorityContainer from './winPriorityContainer';

import { PANEL_VIEWS, RANDOMS_SELECTED_FACTION_ID } from '../../../util/constants';
import { getFactionRoleIDs } from '../../../util/hiddenUtils';
import { white } from '../../../util/colors';
import Button from '../../../components/baseElements/button';


function FactionsContainer({
    isAdvancedMode, disabled, setupID, panelViewState, factionMap, factionRoleMap,
    hiddenMap, showFaction, hideEntity, showHidden,
}){
    const [isWinPriorityShowing, setIsWinPriorityShowing] = useState(false);
    const [factionEditorID, setFactionEditorID] = useState();
    const activeFactionID = getFactionID(panelViewState);
    const canEditSetup = !disabled && isAdvancedMode;
    const focusClass = panelViewState.entityType === PANEL_VIEWS.FACTION
        ? 'rSetupFocused' : 'rSetupBG';
    const factions = Object.values(factionMap);
    const factionsWithRandom = [...factions]
        .sort((f1, f2) => f1.name.localeCompare(f2.name))
        .map(f => ({
            ...f,
            iconClass: canEditSetup ? 'fa-pencil' : 'fa-search',
            className: getFactionFocusClass(f, activeFactionID, focusClass),
        }));

    const nonSingleHiddens = Object.values(hiddenMap)
        .filter(hidden => {
            const factionRoleIDs = getFactionRoleIDs(hidden);
            if(factionRoleIDs.length !== 1)
                return true;

            const factionRole = factionRoleMap[factionRoleIDs[0]];
            return hidden.name !== factionRole.name;
        });
    if(nonSingleHiddens.length)
        factionsWithRandom.push({
            id: RANDOMS_SELECTED_FACTION_ID,
            color: white,
            name: 'Randoms',
            iconClass: '',
            className: activeFactionID === RANDOMS_SELECTED_FACTION_ID ? focusClass : '',
        });

    return (
        <div className="factionsContainer darkenedSetupPanel">
            <TitledListContainer
                containerClassName="factionsList"
                items={factionsWithRandom}
                onActionIconClick={faction => setFactionEditorID(faction.id)}
                onItemClick={faction => {
                    const factionID = faction.id;
                    if(activeFactionID === factionID
                    && panelViewState.entityType === PANEL_VIEWS.FACTION)
                        return hideEntity();

                    if(factionID !== RANDOMS_SELECTED_FACTION_ID)
                        return showFaction(factionID);
                    return showHidden(Object.keys(hiddenMap)[0]);
                }}
                titleText="Factions"
            >
                {canEditSetup && (
                    <div className="setupEntityListButtons">
                        <Button
                            className="advCustomButton"
                            onClick={() => setFactionEditorID(null)}
                        >
                            New Team
                        </Button>
                        <Button
                            className="advCustomButton"
                            onClick={() => setIsWinPriorityShowing(true)}
                        >
                            Tie Resolver
                        </Button>
                    </div>
                )}
            </TitledListContainer>
            { [PANEL_VIEWS.FACTION, PANEL_VIEWS.FACTION_ROLE]
                .includes(panelViewState.entityType) && <FactionDetails disabled={disabled} />}
            <WinPriorityContainer
                factions={factions}
                isShowing={isWinPriorityShowing}
                setClosed={() => setIsWinPriorityShowing(false)}
            />
            <FactionEditorContainer
                disabled={disabled}
                faction={factionEditorID === null ? null : factionMap[factionEditorID]}
                setFactionID={setFactionEditorID}
                setupID={setupID}
            />
        </div>
    );
}

const mapStateToProps = ({
    panelViewState, setupState,
}) => ({
    ...setupState,
    panelViewState,
    setupID: setupState.id,
});

const mapDispatchToProps = {
    hideEntity: onHideEntityAction,
    showFaction: onShowFactionAction,
    showHidden: onShowHiddenAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(FactionsContainer);

function getFactionID(panelViewState){
    switch (panelViewState.entityType){
    case PANEL_VIEWS.FACTION:
        return panelViewState.entityID;
    case PANEL_VIEWS.FACTION_ROLE:
        return panelViewState.factionID;
    case PANEL_VIEWS.HIDDEN:
        return RANDOMS_SELECTED_FACTION_ID;
    default:
        return null;
    }
}

function getFactionFocusClass(faction, activeFactionID, focusClass){
    return `${faction.id === activeFactionID ? `${focusClass} ` : ''}`;
}
