import React from 'react';
import { connect } from 'react-redux';

import DoubleListContainer from '../../../../components/doubleList/doubleListContainer';

import FactionEditorStep from './factionEditorStep';

import { addEnemy, deleteEnemy } from '../../../../services/factionService';


function FactionEditorEnemies({
    disabled, faction, factions, setupID,
}){
    // eslint-disable-next-line no-nested-ternary
    const onLeftClick = disabled
        ? () => {}
        : enemyID => addEnemy(faction.id, enemyID);
    const list1 = {
        label: 'Allies',
        items: factions
            .filter(f => !faction.enemyIDs.includes(f.id))
            .map(f => ({
                color: f.color,
                onClick: onLeftClick,
                text: f.name,
                value: f.id,
            })),
    };
    // eslint-disable-next-line no-nested-ternary
    const onRightClick = disabled
        ? () => {}
        : enemyID => deleteEnemy(setupID, faction.id, enemyID);
    const list2 = {
        label: 'Enemies',
        items: factions
            .filter(f => faction.enemyIDs.includes(f.id))
            .map(f => ({
                color: f.color,
                onClick: onRightClick,
                text: f.name,
                value: f.id,
            })),
    };
    return (
        <FactionEditorStep>
            <DoubleListContainer
                list1={list1}
                list2={list2}
            />
        </FactionEditorStep>
    );
}

const mapStateToProps = ({ setupState }) => ({
    factions: Object.values(setupState.factionMap),
    setupID: setupState.id,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FactionEditorEnemies);
