import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import Button from '../../../components/baseElements/button';
import TextInput from '../../../components/baseElements/textInput';

import FloatingTintedContainer from '../../../components/floatingTintedContainer';
import DoubleListContainer from '../../../components/doubleList/doubleListContainer';

import {
    createHiddenAction, createHiddenSpawnsAction, deleteHiddenSpawnAction,
} from '../../../actions/hiddenActions';


const INITIAL_HIDDEN_DETAILS = { name: '', spawns: [] };

function HiddenEditorContainer({
    createHidden, createHiddenSpawns, deleteHiddenSpawn, factionMap, factionRoleMap, hidden,
    setHiddenID, setupID,
}){
    const [view, setView] = useState(INITIAL_HIDDEN_DETAILS);
    useEffect(() => {
        if(hidden)
            setView(hidden);
        else
            setView(INITIAL_HIDDEN_DETAILS);
    }, [hidden]);

    if(hidden === undefined)
        return null;

    function createHiddenSpawn(hiddenID, factionRoleID){
        createHiddenSpawns([{ hiddenID, factionRoleID }]);
    }

    async function handleButtonClick(){
        const newHidden = await createHidden(view.name, setupID);
        await Promise.all(view.spawns.map(
            ({ factionRoleID }) => createHiddenSpawn(newHidden.id, factionRoleID),
        ));
        setHiddenID(newHidden.id);
    }

    function handleSpawnCreate(factionRoleID){
        if(hidden)
            return createHiddenSpawn(hidden.id, factionRoleID);
        return setView({
            ...view,
            spawns: [
                ...view.spawns,
                { id: factionRoleID, factionRoleID },
            ],
        });
    }

    function handleSpawnDelete(spawnID){
        if(hidden)
            return deleteHiddenSpawn(hidden.id, spawnID);
        return setView({
            ...view,
            spawns: view.spawns.filter(sp => sp.id !== spawnID),
        });
    }

    const spawningFactionRoleIDs = new Set(view.spawns.map(sp => sp.factionRoleID));
    const notSpawningFactionRoles = Object.values(factionRoleMap)
        .filter(fr => !spawningFactionRoleIDs.has(fr.id))
        .map(fr => ({
            color: factionMap[fr.factionID].color,
            onClick: handleSpawnCreate,
            text: fr.name,
            value: fr.id,
        }));

    const spawns = view.spawns.map(spawn => ({
        color: factionMap[factionRoleMap[spawn.factionRoleID].factionID].color,
        onClick: handleSpawnDelete,
        text: factionRoleMap[spawn.factionRoleID].name,
        value: spawn.id,
    }));

    return (
        <FloatingTintedContainer
            className="hiddenEditorContainer"
            setClosed={() => setHiddenID()}
        >
            <TextInput
                className="hiddenEditorTextInput"
                disabled={!!hidden}
                label="Name"
                onChange={name => setView({ ...view, name })}
                // onKeyUp={handleKeyUp} going to be used for editing name
                value={view.name}
            />

            <DoubleListContainer
                list1={{ items: spawns, label: 'Spawning' }}
                list2={{ items: notSpawningFactionRoles, label: 'Not Spawning' }}
            />
            {!hidden && (
                <Button
                    className="entityEditorButton"
                    disabled={!view.name}
                    onClick={handleButtonClick}
                >Create
                </Button>
            )}
        </FloatingTintedContainer>
    );
}

const mapStateToProps = ({ setupState }) => ({
    factionMap: setupState.factionMap,
    factionRoleMap: setupState.factionRoleMap,
    setupID: setupState.id,
});

const mapDispatchToProps = {
    createHidden: createHiddenAction,
    createHiddenSpawns: createHiddenSpawnsAction,
    deleteHiddenSpawn: deleteHiddenSpawnAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(HiddenEditorContainer);
