import React from 'react';
import { connect } from 'react-redux';

import LabeledCheckbox from '../../../components/labeledCheckbox';
import LabeledNumberInput from '../../../components/labeledNumberInput';

import { upsert as upsertFactionModifier } from '../../../services/factionModifierService';


function FactionModifiersEditor({ faction }){
    return (
        <ul className="factionRoleEditor">
            {
                getFactionModifiers(faction).map(({ label, value, onChange }, index) => {
                    if(typeof(value) === 'number')
                        return (
                            // eslint-disable-next-line react/no-array-index-key
                            <li key={index}>
                                <LabeledNumberInput
                                    label={label}
                                    value={value}
                                    onChange={onChange}
                                />
                            </li>
                        );
                    return (
                        // eslint-disable-next-line react/no-array-index-key
                        <li key={index}>
                            <LabeledCheckbox
                                label={label}
                                value={value}
                                onChange={onChange}
                            />
                        </li>
                    );
                })
            }
        </ul>
    );
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FactionModifiersEditor);

function getFactionModifiers(faction){
    return faction.modifiers
        .filter(modifier => modifier.name !== 'WIN_PRIORITY')
        .map(modifier => ({
            label: modifier.label,
            value: modifier.value,
            onChange: newValue => upsertFactionModifier(newValue, modifier.name, faction.id),
        }));
}
