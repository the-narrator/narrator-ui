import React from 'react';

import Button from '../../../components/baseElements/button';
import TitledListContainer from '../../../components/titledListContainer';

import { deleteSetupHidden } from '../../../services/setupHiddenService';

import { getColor as getHiddenColor, getFactionRoleIDs } from '../../../util/hiddenUtils';
import { filterSetupHiddensByPlayerCount } from '../../../util/setupHiddenUtil';


export default function SetupHiddensContainer({
    className = '', showClonePrompt, playerCount, titleText,
    showHidden, showFactionRole, onStartClick, canRemoveSetupHiddens, setup,
}){
    const { hiddenMap } = setup;
    const setupHiddens = filterSetupHiddensByPlayerCount(playerCount, setup.setupHiddens)
        .map(sh => ({
            ...sh,
            color: getHiddenColor(hiddenMap[sh.hiddenID], setup),
            name: hiddenMap[sh.hiddenID].name,
            iconClass: canRemoveSetupHiddens && 'fa-minus-square',
        }));
    setupHiddens.sort(
        (sh1, sh2) => sh1.color.localeCompare(sh2.color) || sh1.name.localeCompare(sh2.name),
    );

    if(className)
        className += ' ';

    return (
        <TitledListContainer
            containerClassName={`${className}setupHiddensContainer`}
            items={setupHiddens}
            onActionIconClick={setupHidden => attemptDelete(setupHidden.id, setup,
                showClonePrompt)}
            onItemClick={setupHidden => {
                const hidden = hiddenMap[setupHidden.hiddenID];
                const factionRoleIDs = getFactionRoleIDs(hidden);
                if(factionRoleIDs.length !== 1)
                    return showHidden(hidden.id);
                const [factionRoleID] = factionRoleIDs;
                const factionID = setup.factionRoleMap[factionRoleID].factionID;
                showFactionRole(factionRoleID, factionID);
            }}
            titleText={titleText}
        >
            {onStartClick && (
                <Button className="startButton">
                    <div onClick={onStartClick}>Start</div>
                </Button>
            )}
        </TitledListContainer>
    );
}

function attemptDelete(setupHiddenID, setup, showClonePrompt){
    if(setup.isEditable)
        return deleteSetupHidden(setupHiddenID);
    showClonePrompt();
}
