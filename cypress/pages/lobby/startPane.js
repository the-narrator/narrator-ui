function failStart(errorText){
    cy.route({
        method: 'POST',
        url: 'api/games/*/start',
        status: 422,
        response: {
            errors: [errorText],
        },
    });
    cy.contains('Start').click();
}

module.exports = {
    failStart,
};
