import { GAME_ID } from '../fixtures/fakeConstants';


function assertOnHome(){
    cy.get('.lobbyContainer').should('not.be.visible');
    cy.get('.lobbiesContainer').should('be.visible');
    cy.url().should('not.include', GAME_ID);
}

function assertOnLobby(){
    cy.get('.lobbyContainer').should('be.visible');
    cy.get('.lobbiesContainer').should('not.be.visible');
}

module.exports = {
    assertOnHome,
    assertOnLobby,
};
