import { HIDDEN_SPAWN_ID } from '../../fixtures/fakeConstants';

const hiddenModel = require('../../fixtures/baseModels/hiddenModel');
const hiddenSpawnModel = require('../../fixtures/baseModels/hiddenSpawnModel');


function clickLeftList(text){
    clickDoubleListContainer(text, 0);
}

function clickRightList(text){
    clickDoubleListContainer(text, 1);
}

function createHidden(){
    cy.route({
        method: 'POST',
        status: 200,
        url: 'api/hiddens',
        response: { response: hiddenModel.get({ spawns: [] }) },
    }).as('hiddenCreateResponse');

    withinEditor(() => {
        cy.get('.entityEditorButton').click();
    });

    cy.wait(['@hiddenCreateResponse']);
}

function createHiddenAndSpawnable(){
    waitForSpawnableCreate(createHidden);
}

function createSpawnable(factionRoleName){
    waitForSpawnableCreate(() => {
        clickRightList(factionRoleName);
    });
}

function deleteSpawnable(factionRoleName){
    cy.route({
        method: 'DELETE',
        status: 204,
        url: `api/hiddenSpawns/${HIDDEN_SPAWN_ID}`,
        response: '',
    }).as('hiddenSpawnDeleteResponse');

    clickLeftList(factionRoleName);

    cy.wait(['@hiddenSpawnDeleteResponse']);
}

function hideByEsc(){
    cy.get('body').type('{esc}');
}

function setName(name, keyCode = 13){ // enter){
    withinEditor(() => {
        cy.get('.hiddenEditorTextInput input')
            .clear()
            .type(name);
        cy.get('.hiddenEditorTextInput input')
            .trigger('keyup', { keyCode });
    });
}

function assertHidden(){
    cy.get('.hiddenEditorContainer').should('not.exist');
}

function assertIsVisible(){
    cy.get('.hiddenEditorContainer').should('exist');
    cy.get('.floatingTintedContainer').should('be.visible');
}

function assertListItemColor(factionRoleName, color){
    withinEditor(() => {
        cy.contains(factionRoleName)
            .should('have.css', 'color')
            .and('be.colored', color);
    });
}

function assertButtonDisabled(){
    withinEditor(() => {
        cy.get('.entityEditorButton').should('be.disabled');
    });
}

function assertNameNotEditable(){
    withinEditor(() => {
        cy.get('.hiddenEditorTextInput input').should('be.disabled');
    });
}

function assertNoButton(){
    withinEditor(() => {
        cy.get('.entityEditorButton').should('not.exist');
    });
}

function assertNotSpawnable(factionRoleName){
    assertRightContains(factionRoleName);
}

function assertSpawnable(factionRoleName){
    assertLeftContains(factionRoleName);
}

module.exports = {
    clickLeftList,
    clickRightList,
    createHidden,
    createHiddenAndSpawnable,
    createSpawnable,
    deleteSpawnable,
    hideByEsc,
    setName,

    assertButtonDisabled,
    assertHidden,
    assertIsVisible,
    assertListItemColor,
    assertNameNotEditable,
    assertNoButton,
    assertNotSpawnable,
    assertSpawnable,
};

function withinEditor(func){
    cy.get('.hiddenEditorContainer').within(func);
}

function clickDoubleListContainer(text, index){
    withinEditor(() => {
        cy.get('.doubleListContainerUL').eq(index).within(() => {
            cy.contains(text).click();
        });
    });
}

function assertDoubleListContains(text, index1, index2){
    withinEditor(() => {
        cy.get('.doubleListContainerUL').eq(index1)
            .should('contain', text);
        cy.get('.doubleListContainerUL').eq(index2)
            .should('not.contain', text);
    });
}

function assertLeftContains(text){
    assertDoubleListContains(text, 0, 1);
}

function assertRightContains(text){
    assertDoubleListContains(text, 1, 0);
}

function ensurePayloadSchema({ request }){
    const { body } = request;
    expect(body).to.be.a('array');
}

function waitForSpawnableCreate(triggerFunc){
    cy.route({
        method: 'POST',
        status: 200,
        url: 'api/hiddenSpawns',
        response: { response: [hiddenSpawnModel.get()] },
        onRequest: ensurePayloadSchema,
    }).as('hiddenSpawnCreateResponse');

    triggerFunc();

    cy.wait(['@hiddenSpawnCreateResponse']);
}
