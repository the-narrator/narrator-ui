import gameRequests from '../../requests/gameRequests';
import setupRequests from '../../requests/setupRequests';


function customizeSetup(){
    setupRequests.cloneSetup(() => {
        gameRequests.updateSetup(() => {
            cy.contains('Customize').click();
        });
    });
}

function setAdvancedToggle(isOn){
    const [selectedText, unselectedText] = isOn ? ['ON', 'OFF'] : ['OFF', 'ON'];
    cy.contains('Advanced Mode').parent().parent().within(() => {
        cy.contains(selectedText).click();
        cy.contains(selectedText).should('have.class', 'selectedToggle');
        cy.contains(unselectedText).should('not.have.class', 'selectedToggle');
    });
}

function assertNoCustomizeButton(){
    cy.contains('Customize').should('not.be.visible');
}

module.exports = {
    customizeSetup,
    setAdvancedToggle,

    assertNoCustomizeButton,
};
