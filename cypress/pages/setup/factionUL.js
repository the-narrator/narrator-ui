function click(factionName){
    cy.contains('Factions').parent().within(() => {
        cy.contains(factionName).click();
    });
}

function clickHiddens(){
    click('Randoms');
}

function clickNewFaction(){
    cy.contains('New Team').click();
}

function assertActiveFaction(factionName){
    cy.contains('Factions').parent().within(() => {
        cy.contains(factionName).parent().should('have.class', 'rSetupFocused');
    });
}

function assertBackgroundFaction(factionName){
    cy.contains('Factions').parent().within(() => {
        cy.contains(factionName).parent().should('have.class', 'rSetupBG');
    });
}

function assertHiddenBackgroundSelected(){
    assertBackgroundFaction('Randoms');
}

function assertNoActiveFactions(){
    cy.get('#team_catalogue .rSetupFocused').should('not.exist');
    cy.get('#team_catalogue .rSetupBG').should('not.exist');
}

function assertSize(size){
    cy.contains('Factions').parent().within(() => {
        cy.get('.titledListItem').should('have.length', size);
    });
}

function assertTeamDoesNotExist(name){
    cy.contains('Factions').parent().within(() => {
        cy.contains(name).should('not.exist');
    });
}

function shouldLookEditable(){
    cy.contains('Factions').parent().within(() => {
        cy.get('.titledListItemActionIcon').should('have.class', 'fa-pencil');
    });
}

module.exports = {
    click,
    clickHiddens,
    clickNewFaction,

    assertActiveFaction,
    assertBackgroundFaction,
    assertHiddenBackgroundSelected,
    assertNoActiveFactions,
    assertSize,
    assertTeamDoesNotExist,
    shouldLookEditable,
};
