import abilityModel from '../../fixtures/baseModels/abilityModel';
import factionModel from '../../fixtures/baseModels/factionModel';

import {
    ABILITY_ID,
    FACTION_ID, FACTION_NAME, FACTION_ROLE_ID, SETUP_ID,
} from '../../fixtures/fakeConstants';

import factionRoleResponse from '../../fixtures/responses/factionRoleResponse';


function addAlly(factionName, args = {}){
    cy.route({
        method: 'DELETE',
        url: `api/setups/${SETUP_ID}/factions/${FACTION_ID}/enemies/${FACTION_ID}`,
        response: '',
        onRequest: args.onRequest,
    }).as('factionEnemyDeleteResponse');
    clickRightListItem(factionName);
    cy.wait(['@factionEnemyDeleteResponse']);
}


function addAbility(abilityName, args = {}){
    cy.route({
        method: 'POST',
        url: `api/factions/${FACTION_ID}/abilities`,
        response: { response: abilityModel.get() },
        onRequest: args.onRequest,
    }).as('factionAbilityCreateResponse');
    clickRightListItem(abilityName);
    cy.wait(['@factionAbilityCreateResponse']);
}

function addEnemy(factionName, args = {}){
    cy.route({
        method: 'POST',
        url: `api/factions/${FACTION_ID}/enemies/${FACTION_ID}`,
        response: '',
        onRequest: args.onRequest,
    }).as('factionEnemyPostResponse');
    clickLeftListItem(factionName);
    cy.wait(['@factionEnemyPostResponse']);
}

function addSpawnable(roleName){
    cy.route({
        method: 'POST',
        url: 'api/factionRoles',
        response: factionRoleResponse.post(),
    }).as('factionRolePostResponse');
    clickRightListItem(roleName);
    cy.wait(['@factionRolePostResponse']);
}

function clickAbilitiesNav(){
    withinEditor(() => {
        cy.get('.factionEditorNav').within(() => {
            cy.contains('Abilities').click();
        });
    });
}

function clickAndCreate(args = {}){
    cy.route({
        method: 'POST',
        status: args.statusCode || 200,
        url: 'api/factions',
        response: args.response || { response: factionModel.get() },
        onRequest: args.onRequest,
    }).as('factionCreateRequest');
    clickNext('Create');
    cy.wait(['@factionCreateRequest']);
}

function clickNext(text = 'Next'){
    withinEditor(() => {
        cy.get('.entityEditorButton').should('have.text', text).click();
    });
}

function deleteFaction(){
    cy.route({
        method: 'DELETE',
        url: `api/factions/${FACTION_ID}`,
        response: '',
    }).as('factionDeleteResponse');
    withinEditor(() => {
        cy.contains('Delete').click();
    });
    cy.wait(['@factionDeleteResponse']);
}

function deleteFactionRole(roleName){
    cy.route({
        method: 'DELETE',
        url: `api/factionRoles/${FACTION_ROLE_ID}`,
        response: '',
    }).as('factionRoleDeleteResponse');
    clickLeftListItem(roleName);
    cy.wait(['@factionRoleDeleteResponse']);
}

function editFactionName(factionName, args = {}){
    cy.route({
        method: 'PUT',
        status: args.statusCode || 204,
        url: `api/setups/${SETUP_ID}/factions/${FACTION_ID}`,
        response: args.response || '',
        onRequest: args.onRequest,
    }).as('factionUpdateResponse');
    setFactionName(factionName, args.keyCode);
    cy.wait(['@factionUpdateResponse']);
}

function hideByBackgroundClick(){
    cy.get('.floatingTintedContainerBackground').click({ position: 'topLeft', force: true });
}

function hideByEsc(){
    cy.get('body').type('{esc}');
}

function removeAbility(roleName){
    cy.route({
        method: 'DELETE',
        url: `api/factions/${FACTION_ID}/abilities/${ABILITY_ID}`,
        response: '',
    }).as('factionAbilityDeleteResponse');
    clickLeftListItem(roleName);
    cy.wait(['@factionAbilityDeleteResponse']);
}

function setFactionColor(factionColor){
    withinEditor(() => {
        cy.get('.factionEditorDetailsTextInput').eq(1)
            .type(factionColor);
    });
}

function setFactionName(factionName, keyCode = 13){ // enter
    withinEditor(() => {
        cy.get('.factionEditorDetailsTextInput input').eq(0)
            .clear()
            .type(factionName);
        cy.get('.factionEditorDetailsTextInput input').eq(0)
            .trigger('keyup', { keyCode });
    });
}

function show(factionName = FACTION_NAME){
    cy.contains('Factions').parent().within(() => {
        cy.contains(factionName).parent().within(() => {
            cy.get('.titledListItemActionIcon').click();
        });
    });
}

function assertAlly(factionName){
    assertLeftContains(factionName);
}

function assertButtonDisabled(){
    withinEditor(() => {
        cy.get('.entityEditorButton').should('be.disabled');
    });
}

function assertButtonText(buttonText){
    withinEditor(() => {
        cy.get('.entityEditorButton').should('have.text', buttonText);
    });
}

function assertColorNotEditable(){
    withinEditor(() => {
        cy.get('.factionEditorDetailsTextInput input').eq(1)
            .should('be.disabled');
    });
}

function assertContainsFactionRole(roleName){
    assertLeftContains(roleName);
}

function assertFactionName(factionName){
    withinEditor(() => {
        cy.get('.factionEditorDetailsTextInput input').eq(0)
            .should('have.value', factionName);
    });
}

function assertHasAbility(abilityType){
    assertLeftContains(abilityType);
}

function assertHidden(){
    cy.get('.factionEditorContainer').should('not.exist');
}

function assertNoAbility(abilityType){
    assertRightContains(abilityType);
}

function assertNoDeleteButton(){
    withinEditor(() => {
        cy.contains('Delete').should('not.exist');
    });
}

function assertNotContainsFactionRole(roleName){
    assertRightContains(roleName);
}

function assertOnAbilities(){
    assertOnTab('Abilities');
    withinEditor(() => {
        cy.get('.doubleListContainer .doubleListHeader').eq(0)
            .should('contain', 'Available');
        cy.get('.doubleListContainer .doubleListHeader').eq(1)
            .should('contain', 'Unavailable');
    });
}

function assertOnDetails(){
    assertOnTab('Details');
}

function assertOnRoles(){
    assertOnTab('Roles');
    withinEditor(() => {
        cy.get('.doubleListContainer .doubleListHeader').eq(0)
            .should('contain', 'Spawnable');
        cy.get('.doubleListContainer .doubleListHeader').eq(1)
            .should('contain', 'Not Spawnable');
    });
}

function assertSharedAbility(abilityType){
    assertLeftContains(abilityType);
}

function assertSpawnableColor(color){
    withinEditor(() => {
        cy.get('.doubleListContainerUL div')
            .should('have.css', 'color')
            .and('be.colored', color);
    });
}

function assertEnemy(factionName){
    assertRightContains(factionName);
}

function assertVisible(){
    cy.get('.floatingTintedContainer').find('.navBarItem')
        .should('have.length', 4);
}

module.exports = {
    addAlly,
    addAbility,
    addEnemy,
    addSpawnable,
    clickAbilitiesNav,
    clickAndCreate,
    clickNext,
    deleteFaction,
    deleteFactionRole,
    editFactionName,
    hideByBackgroundClick,
    hideByEsc,
    removeAbility,
    setFactionColor,
    setFactionName,
    show,

    assertAlly,
    assertButtonDisabled,
    assertButtonText,
    assertColorNotEditable,
    assertContainsFactionRole,
    assertEnemy,
    assertFactionName,
    assertHasAbility,
    assertHidden,
    assertNoAbility,
    assertNoDeleteButton,
    assertNotContainsFactionRole,
    assertOnAbilities,
    assertOnDetails,
    assertOnRoles,
    assertSharedAbility,
    assertSpawnableColor,
    assertVisible,
};

function withinEditor(func){
    cy.get('.factionEditorContainer').within(func);
}

function clickLeftListItem(itemName){
    withinEditor(() => {
        cy.get('.doubleListContainerUL').eq(0).within(() => {
            cy.contains(itemName).click();
        });
    });
}

function clickRightListItem(itemName){
    withinEditor(() => {
        cy.get('.doubleListContainerUL').eq(1).within(() => {
            cy.contains(itemName).click();
        });
    });
}

function assertDoubleListContains(text, index1, index2){
    withinEditor(() => {
        cy.get('.doubleListContainerUL').eq(index1)
            .should('contain', text);
        cy.get('.doubleListContainerUL').eq(index2)
            .should('not.contain', text);
    });
}

function assertLeftContains(text){
    assertDoubleListContains(text, 0, 1);
}

function assertOnTab(navTabName){
    cy.get('.factionEditorNav').within(() => {
        cy.contains(navTabName).parent()
            .should('have.class', 'selectedFactionEditorNav');
    });
}

function assertRightContains(text){
    assertDoubleListContains(text, 1, 0);
}
