import setupHiddenResponse from '../../fixtures/responses/setupHiddenResponse';
import hiddenResponse from '../../fixtures/responses/hiddenResponse';
import hiddenSpawnResponse from '../../fixtures/responses/hiddenSpawnResponse';
import { SETUP_ID } from '../../fixtures/fakeConstants';


function addCachedRole(role, { onRequest }){
    cy.route({
        method: 'POST',
        url: 'api/setupHiddens',
        response: setupHiddenResponse.post(),
        onRequest,
    }).as('setupHiddenResponse');
    clickAddIcon(role);
    cy.wait(['@setupHiddenResponse']);
}

function addHidden(name){
    cy.route({
        method: 'POST',
        url: 'api/setupHiddens',
        response: setupHiddenResponse.post(),
    }).as('setupHiddenResponse');
    cy.contains('Randoms').click();
    clickAddIcon(name);
    cy.wait(['@setupHiddenResponse']);
}

function addUncachedRole(roleName){
    cy.route({
        method: 'POST',
        url: 'api/setupHiddens',
        response: setupHiddenResponse.post(),
    }).as('setupHiddenResponse');
    cy.route({
        method: 'POST',
        url: 'api/hiddens',
        response: hiddenResponse.post(),
        onResponse: ({ request }) => {
            const { body } = request;
            expect(body.name).to.be.equal(roleName);
            expect(body.setupID).to.be.equal(SETUP_ID);
        },
    }).as('hiddenResponse');
    cy.route({
        method: 'POST',
        url: 'api/hiddenSpawns',
        response: hiddenSpawnResponse.post(),
    }).as('hiddenSpawnResponse');
    clickAddIcon(roleName);
    cy.wait(['@setupHiddenResponse', '@hiddenResponse', '@hiddenSpawnResponse']);
}

function click(factionRoleName){
    cy.contains('Faction Roles').parent().within(() => {
        cy.contains(factionRoleName).click();
    });
}

function assertColor(color){
    cy.contains('Faction Roles').parent().within(() => {
        cy.get('.titledListItem span:first')
            .should('have.css', 'color')
            .and('be.colored', color);
    });
}

function assertFocusedText(text){
    cy.contains('Faction Roles').parent().within(() => {
        cy.contains(text).parent().should('have.class', 'rSetupFocused');
    });
}

function assertSize(size){
    cy.contains('Faction Roles').parent().within(() => {
        cy.get('.titledListItem').should('have.length', size);
    });
}

function openHiddenCreator(){
    cy.contains('New Hidden').click();
}

module.exports = {
    addCachedRole,
    addHidden,
    addUncachedRole,
    click,
    openHiddenCreator,

    assertColor,
    assertFocusedText,
    assertSize,
};

function clickAddIcon(name){
    cy.contains('Faction Roles').parent().within(() => {
        cy.contains(name).parent().within(() => {
            cy.get('.titledListItemActionIcon').click();
        });
    });
}
