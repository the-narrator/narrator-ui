import factionModifierResponse from '../../fixtures/responses/factionModifierResponse';


function setChecked(value){
    cy.route({
        method: 'POST',
        url: 'api/factions/*/modifiers',
        response: factionModifierResponse.get(),
    }).as('factionModifierResponse');
    if(value)
        cy.get('.factionsContainer .factionRoleEditor input').check();
    else
        cy.get('.factionsContainer .factionRoleEditor input').uncheck();
    cy.wait(['@factionModifierResponse']);
}

function setInputValue(value){
    cy.route({
        method: 'POST',
        url: 'api/factions/*/modifiers',
        response: factionModifierResponse.get(),
    }).as('factionModifierResponse');
    cy.get('.factionsContainer .factionRoleEditor input').clear();
    cy.get('.factionsContainer .factionRoleEditor input').type(value);
    cy.wait(['@factionModifierResponse']);
}

function assertHeader(factionName){
    cy.get('.teamPickerEditor .roleCardHeader').should('contain', factionName);
}

function assertHeaderMobile(factionName){
    cy.get('.setupEditorPane > .entityDetailsContainer .entityDetailsHeaderText')
        .should('contain', factionName);
}

function assertInputValue(value){
    cy.get('.factionsContainer .factionRoleEditor input').should('have.value', value.toString());
}

function assertInvisible(){
    // this assertion could be better, saying whether factions are visible, role view is visible
    // or if they're both invisible
    cy.get('#setup_page .team_settings_pane').should('not.be.visible');
}

function assertIsChecked(){
    cy.get('.factionsContainer .factionRoleEditor input').should('be.checked');
}

function assertIsUnchecked(){
    cy.get('#setup_page .team_settings_pane input').should('not.be.checked');
}

function assertModifierNotVisible(text){
    cy.get('#setup_page .team_settings_pane').should('not.contain', text);
}

function assertVisible(){
    cy.get('.factionsContainer .entityDetailsContainer').should('be.visible');
}

function assertVisibleMobile(){
    cy.get('.setupEditorPane > .entityDetailsContainer').should('exist');
    cy.get('.setupEditorPane > .entityDetailsContainer').should('be.visible');
}

function assertVisibleModifiers(modifiers){
    modifiers.forEach(modifier => {
        cy.get('.factionsContainer .entityDetailsContainer').should('contain', modifier);
    });
}

module.exports = {
    setChecked,
    setInputValue,

    assertHeader,
    assertHeaderMobile,
    assertInputValue,
    assertInvisible,
    assertIsChecked,
    assertIsUnchecked,
    assertModifierNotVisible,
    assertVisible,
    assertVisibleMobile,
    assertVisibleModifiers,
};
