import { FACTION_ID } from '../../fixtures/fakeConstants';

function setPriority(startIndex, endIndex, { response, onRequest }){
    cy.route({
        method: 'POST',
        url: `api/factions/${FACTION_ID}/modifiers`,
        response: { response },
        onRequest,
    }).as('tieResponse');

    cy.get('.winPriorityChild:first').then($el1 => {
        const sourceRect = $el1[0].getBoundingClientRect();
        // const yPos1 = $el1[0].getBoundingClientRect().y;
        cy.get('.winPriorityChild:nth-child(2)').then($el2 => {
            const yPos2 = $el2[0].getBoundingClientRect().y;
            cy.get('.winPriorityChild:first')
                .trigger('mousedown', { which: 1 });
            cy.wait(1000);
            cy.get('.winPriorityChild:first')
                .trigger('mousemove', { clientX: sourceRect.x, clientY: yPos2 });
            cy.wait(1000);
            cy.get('.winPriorityChild:nth-child(2)')
                .trigger('mouseup', { force: true });
        });
    });

    cy.wait(['@tieResponse']);
}

function show(){
    cy.contains('Tie Resolver').click();
}

function assertRanking(factionName, index){
    cy.get('.winPriorityChild').eq(index).contains(factionName);
}

module.exports = {
    setPriority,
    show,

    assertRanking,
};
