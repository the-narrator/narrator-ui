function assertTextOld(text){
    cy.get('#warningPopup span').contains(text);
}

function assertHiddenOld(){
    cy.get('#warningPopup', { timeout: 1000 }).should('not.be.visible');
}

function assertTitle(titleText){
    cy.contains(titleText).should('be.visible');
}

module.exports = {
    assertHiddenOld,
    assertTextOld,
    assertTitle,
};
