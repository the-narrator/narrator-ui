import gameModel from '../../fixtures/baseModels/gameModel';

import userStateSetupResponse from '../../fixtures/userStateSetupReponse';

import { GAME_ID } from '../../fixtures/fakeConstants';
import gameModifierResponse from '../../fixtures/responses/gameModifierResponse';


function clickCreateGame(){
    cy.contains('Create Game').click();
}

function createGame({ onRequest }){
    clickCreateGame();

    cy.route({
        method: 'POST',
        url: 'api/games',
        response: { response: gameModel.get() },
        onRequest,
    }).as('createGameResponse');
    cy.route({
        method: 'POST',
        url: 'api/gameModifiers',
        response: {
            response: gameModifierResponse.get({
                name: 'CHAT_ROLES',
                value: true,
            }),
        },
    }).as('gameModifierUpdate');
    cy.route({
        method: 'GET',
        url: 'api/games/userState',
        response: { response: userStateSetupResponse.get() },
    }).as('userStateResponse');
    cy.route({
        method: 'GET',
        url: `api/chats/${GAME_ID}/lobby`,
        response: { response: [] },
    }).as('chatResponse');

    cy.get('.gameCreatorLayoutContainer').within(() => {
        cy.contains('Create Game').click();
    });

    cy.wait(['@createGameResponse', '@userStateResponse', '@chatResponse',
        '@gameModifierUpdate']);
}

function joinGame(){
    const responseObj = userStateSetupResponse.get({
        isStarted: false,
    });

    cy.route({
        method: 'GET',
        url: `api/chats/${GAME_ID}/lobby`,
        response: { response: [] },
    }).as('chatResponse');
    cy.route({
        method: 'POST',
        url: 'api/players',
        response: { response: gameModel.get() },
    }).as('playerJoinResponse');
    cy.route({
        method: 'GET',
        url: 'api/games/userState',
        response: { response: responseObj },
    });

    cy.get('.lobbyMainActionContainer').click();

    cy.wait(['@playerJoinResponse', '@chatResponse']);
}

function assertDescriptionBulletCount(count){
    cy.get('.gameHostSetupDescription').find('li').should('have.length', count);
}

function assertCreateGameButtonVisible(){
    withinLobbiesContainer(() => {
        cy.contains('Create Game').should('be.visible');
    });
}

function assertCreateGameModalOpen(){
    cy.get('.gameCreatorLayoutContainer').should('be.visible');
}

module.exports = {
    clickCreateGame,
    createGame,
    joinGame,

    assertDescriptionBulletCount,
    assertCreateGameButtonVisible,
    assertCreateGameModalOpen,
};

function withinLobbiesContainer(func){
    cy.get('.lobbiesContainer').within(func);
}
