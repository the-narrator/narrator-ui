function click(text){
    cy.get('#game_info_pane .setupHiddensContainer').within(() => {
        cy.contains(text).click();
    });
}

function clickFirst(){
    cy.get('#game_info_pane .setupHiddensContainer .titledListItem:first').click();
}

function assertSetupHiddenCount(count){
    cy.get('#game_info_pane .setupHiddensContainer').find('.titledListItem')
        .should('have.length', count);
}

module.exports = {
    click,
    clickFirst,

    assertSetupHiddenCount,
};
