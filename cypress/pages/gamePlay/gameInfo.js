function clickHidden(hiddenName){
    cy.get('#game_info_pane .entityDetailsContainer').contains(hiddenName).click();
}

function clickRole(roleName){
    cy.get('#game_info_pane .entityDetailsContainer').contains(roleName).click();
}

function clickTeamSubheader(){
    cy.get('#game_info_pane .entityDetailsSubheaderText').click();
}

function assertContainsDeath(){
    cy.get('#game_info_pane .entityDetailsList').should('contain', 'Died on');
}

function assertEnemiesShown(factionName){
    cy.get('#game_info_pane .entityDetailsList').contains('Must eliminate');
    cy.get('#game_info_pane .entityDetailsList').contains(factionName);
}

function assertFactionClicked(factionName){
    cy.get('#game_info_pane .entityDetailsHeaderText').contains(factionName);
}

function assertFactionRoleShown(roleName){
    cy.get('#game_info_pane .entityDetailsList').contains('Possible roles');
    cy.get('#game_info_pane .entityDetailsList').contains(roleName);
}

function assertGraveClicked(playerName){
    cy.get('#game_info_pane .entityDetailsHeaderText').should('be.visible');
    cy.get('#game_info_pane .entityDetailsHeaderText').contains(playerName);
}

function assertHiddenClicked(hiddenName){
    cy.get('#game_info_pane .entityDetailsHeaderText').contains(hiddenName);
}

function assertImageVisible(){
    cy.get('#game_info_pane .entityDetailsContainer img').should('be.visible');
}

function assertNotContains(text){
    cy.get('#setupPublicDescriptionContainer').should('not.contain', text);
}

function assertRoleClicked(roleName){
    cy.get('#game_info_pane .entityDetailsHeaderText').contains(roleName);
}

function assertSetupHiddenName(hiddenName){
    cy.get('#game_info_pane .entityDetailsHeaderText').contains(hiddenName);
    cy.get('#game_info_pane .entityDetailsHeaderText').should('be.visible');
}

function assertSpawnedByCount(count){
    cy.get('#game_info_pane .entityDetailsList').find('.roleHover').should('have.length', count);
}

function assertSubheaderHidden(){
    cy.get('#setupPublicDescriptionContainer .roleCardSubHeader').should('not.be.visible');
}

module.exports = {
    clickHidden,
    clickRole,
    clickTeamSubheader,

    assertContainsDeath,
    assertEnemiesShown,
    assertFactionClicked,
    assertGraveClicked,
    assertHiddenClicked,
    assertImageVisible,
    assertFactionRoleShown,
    assertNotContains,
    assertRoleClicked,
    assertSetupHiddenName,
    assertSpawnedByCount,
    assertSubheaderHidden,
};
