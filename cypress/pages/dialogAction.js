function close(){
    cy.contains('Disagree').click();
}

function assertIsHidden(){
    cy.get('#alert-dialog-slide-title').should('not.be.visible');
}

function assertClonePrompt(){
    assertTitle('Clone Setup?');
}

module.exports = {
    close,

    assertClonePrompt,
    assertIsHidden,
};

// you probably want the toast
function assertTitle(titleText){
    cy.contains(titleText).should('be.visible');
}
