const playerAbilityModel = require('../../fixtures/baseModels/playerAbilityModel');
const profileModel = require('../../fixtures/baseModels/profileModel');

const dayStartEvent = require('../../fixtures/events/dayStartEvent');

const actionPane = require('../../pages/gamePlay/actionPane');
const tabs = require('../../pages/gamePlay/tabs');


describe('Start test', () => {
    context('On phase start', () => {
        it.skip('Should show actions', () => {
            const dayStartObj = dayStartEvent.get({
                profile: profileModel.getInGame({
                    abilities: [
                        playerAbilityModel.get({
                            command: 'vote',
                        }),
                    ],
                }),
            });
            cy.goToSetup();

            cy.window().then(async window => {
                await window.handleObject(dayStartObj);
                cy.wait(2000);

                tabs.clickActions();

                actionPane.assertHeader('Vote');
            });
        });
    });
});
