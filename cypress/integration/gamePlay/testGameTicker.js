import ticker from '../../pages/gamePlay/ticker';

import {
    PLAYER_COLOR, PLAYER_ICON,
} from '../../fixtures/fakeConstants';

import chatModel from '../../fixtures/baseModels/chatModel';
import messageModel from '../../fixtures/baseModels/messageModel';
import phaseModel from '../../fixtures/baseModels/phaseModel';


describe('Game Ticker Visualizations', () => {
    beforeEach(() => {
        cy.mobile();
    });

    context('When game messages are present', () => {
        it('It will show messages in the ticker', () => {
            const chats = chatModel.get({
                messages: [messageModel.getPlayerMessage()],
            });

            cy.goToGame({
                chats,
                phase: phaseModel.getNight(),
            });

            ticker.assertChatIconColor(PLAYER_COLOR);
            ticker.assertChatIcon(PLAYER_ICON);
        });
    });
});
