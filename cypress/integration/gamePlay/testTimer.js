import timerUpdateEvent from '../../fixtures/events/timerUpdateEvent';

import timer from '../../pages/gamePlay/timer';


describe('Timer', () => {
    context('Timer visuals', () => {
        it('Should update the timer', () => {
            const endTime = new Date().getTime() + 61000;
            const timerUpdateEventObj = timerUpdateEvent.get({ endTime });
            cy.goToGame();

            cy.window().then(async window => {
                await window.handleObject(timerUpdateEventObj);

                timer.assertTime('0:5');
            });
        });
    });
});
