import phaseModel from '../../fixtures/baseModels/phaseModel';
import playerModel from '../../fixtures/baseModels/playerModel';
import voteInfoModel from '../../fixtures/baseModels/voteInfoModel';

import playerUpdateEvent from '../../fixtures/events/playerUpdateEvent';

import votePane from '../../pages/gamePlay/votePane';


describe('Test Player update event', () => {
    context('Will respond to player update events', () => {
        it('Will update vote counts on player update events', () => {
            const newVotePower = 2;
            const player = playerModel.getInGame();
            const updatedPlayer = { ...player, votePower: newVotePower };
            const voteInfo = voteInfoModel.get({
                allowedTargets: [player.name],
                voterToVotes: {
                    [player.name]: [player.name],
                },
            });
            const phase = phaseModel.getDay();
            cy.goToGame({ phase, players: [player], voteInfo });
            const playerUpdateEventObj = playerUpdateEvent.get({
                player: updatedPlayer,
            });

            cy.window().then(async window => {
                await window.handleObject(playerUpdateEventObj);
                votePane.assertVoteCount(player.name, newVotePower);
            });
        });
    });
});
