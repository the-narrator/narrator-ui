import deprecatedAbilitiesModel from '../../../fixtures/baseModels/deprecatedAbilitiesModel';
import phaseModel from '../../../fixtures/baseModels/phaseModel';

import actionPane from '../../../pages/gamePlay/actionPane';
import tabs from '../../../pages/gamePlay/tabs';

import { COLOR, FACTION_NAME, PLAYER_NAME2 } from '../../../fixtures/fakeConstants';


function getAbilities(){
    return deprecatedAbilitiesModel.get('Frame', {
        [COLOR]: {
            val: COLOR,
            color: COLOR,
            name: FACTION_NAME,
            map: 0,
        },
    });
}

describe('Test frame visualisation', () => {
    context('On action submit', () => {
        it('Will submit a frame action to the server', () => {
            cy.goToGame({
                abilities: getAbilities(),
                phase: phaseModel.getNight(),
            });
            tabs.clickActions();

            actionPane.clickLeftSubmit(PLAYER_NAME2, {
                onRequest: request => {
                    const action = request.request.body;
                    expect(action.option).to.equal(COLOR);
                },
            });
        });
    });
});
