import { PLAYER_NAME2, PLAYER_NAME3 } from '../../fixtures/fakeConstants';

import playerModel from '../../fixtures/baseModels/playerModel';

import playerResponses from '../../fixtures/responses/playerResponses';

import participantUL from '../../pages/lobby/participantsPage';


describe('Bot Input', () => {
    context('On bot add', () => {
        it('Should show that bots were added', () => {
            const addBotResponse = playerResponses.addBots({
                players: [
                    playerModel.get(),
                    playerModel.getBot({ name: PLAYER_NAME2 }),
                    playerModel.getBot({ name: PLAYER_NAME3 }),
                ],
            });
            cy.goToLobby();
            cy.route({
                method: 'POST',
                url: 'api/players/bots',
                response: addBotResponse,
            }).as('botRequest');

            cy.window().then(async window => {
                await window.addBots(2);
                cy.wait(['@botRequest']);

                participantUL.click(PLAYER_NAME2);
                participantUL.click(PLAYER_NAME3);

                participantUL.assertPlayerHeaderCount(3);
                participantUL.assertIsComputer(PLAYER_NAME2);
                participantUL.assertIsComputer(PLAYER_NAME3);
                participantUL.assertNameInUL(PLAYER_NAME2);
                participantUL.assertNameInUL(PLAYER_NAME3);
            });
        });
    });
});
