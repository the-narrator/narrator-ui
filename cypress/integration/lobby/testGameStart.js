import startPane from '../../pages/lobby/startPane';
import toasts from '../../pages/toasts';


describe('Game Start', () => {
    context('Game start errors', () => {
        it('Will show a popup when game fails to start', () => {
            const errorText = 'Failed to start';
            cy.goToLobby();

            startPane.failStart(errorText);

            toasts.assertTitle(errorText);
        });
    });
});
