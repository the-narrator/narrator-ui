import chatContainer from '../../pages/lobby/chatContainer';

import messageModel from '../../fixtures/baseModels/messageModel';
import lobbyMessageEvent from '../../fixtures/events/lobbyMessageEvent';
import { CHAT_TEXT } from '../../fixtures/fakeConstants';


describe('Chat visualizations', () => {
    context('Sending', () => {
        it('Will submit a message', () => {
            cy.goToLobby();

            chatContainer.sendMessage(CHAT_TEXT, {
                onRequest: ({ request }) => {
                    expect(request.body.text).to.be.equal(CHAT_TEXT);
                },
            });

            chatContainer.assertChatInput('');
        });
    });

    context('Loading initial', () => {
        it('Will load messages', () => {
            const lobbyChatMessages = [
                messageModel.getLobby(),
                messageModel.getLobby2(),
            ];

            cy.goToLobby({
                lobbyChatMessages,
            });

            chatContainer.assertLength(2);
        });
    });

    context('Receiving', () => {
        it('Will update when a new message is received', () => {
            cy.goToLobby();
            const lobbyMessageObj = lobbyMessageEvent.get();

            cy.window().then(async window => {
                await window.handleObject(lobbyMessageObj);
                cy.wait(2000);

                chatContainer.assertLength(1);
            });
        });
    });
});
