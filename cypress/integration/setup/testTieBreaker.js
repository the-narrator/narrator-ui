import setupContainer from '../../pages/setup/setupContainer';
import tieBreaker from '../../pages/setup/tieBreaker';

import factionModel from '../../fixtures/baseModels/factionModel';
import setupModel from '../../fixtures/baseModels/setupModel';


const factionPriority = 1;
const faction = factionModel.get({
    modifiers: [{
        name: 'WIN_PRIORITY',
        value: factionPriority,
    }],
});
const faction2 = factionModel.get2({
    modifiers: [{
        name: 'WIN_PRIORITY',
        value: factionPriority + 1,
    }],
});
const setup = setupModel.get({ factions: [faction, faction2] });

describe('Tie Breaker Visualizations', () => {
    context('When the configurator is open', () => {
        it('Will show the rankings on mobile', () => {
            cy.goToSetup({ setup });
            setupContainer.turnOnFullCustomizability();

            tieBreaker.show();

            tieBreaker.assertRanking(faction.name, 0);
            tieBreaker.assertRanking(faction2.name, 1);
        });

        // const testCases = [
        //     [true, 'mobile'],
        //     // [false, 'tablet']
        // ];
        // testCases.forEach(([isMobileEnabled, displayTag]) => {
        //     it.only(`Will update the rankings on ${displayTag}`, () => {
        //         if(isMobileEnabled)
        //             cy.mobile();
        //         cy.goToSetup({ setup });
        //         if(isMobileEnabled)
        //             tabs.goToGameSettings();
        //         setupContainer.turnOnFullCustomizability();
        //         if(isMobileEnabled)
        //             tabs.goToRoles();
        //
        //         tieBreaker.show();
        //
        //         tieBreaker.setPriority(0, 1, {
        //             onRequest: requestResponse => {
        //                 const { body } = requestResponse.request;
        //                 expect(body.name).to.be.equal('WIN_PRIORITY');
        //                 expect(body.minPlayerCount).to.not.be(undefined);
        //                 expect(body.maxPlayerCount).to.not.be(undefined);
        //             },
        //             response: {
        //                 value: factionPriority + 1,
        //             },
        //         });
        //         //
        //         // tieBreaker.assertRanking(faction.name, 1);
        //         // tieBreaker.assertRanking(faction2.name, 0);
        //     });
        // });
    });
});
