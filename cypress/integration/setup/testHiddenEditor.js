import factionModel from '../../fixtures/baseModels/factionModel';
import factionRoleModel from '../../fixtures/baseModels/factionRoleModel';
import hiddenModel from '../../fixtures/baseModels/hiddenModel';
import hiddenSpawnModel from '../../fixtures/baseModels/hiddenSpawnModel';
import setupModel from '../../fixtures/baseModels/setupModel';

import factionsUL from '../../pages/setup/factionUL';
import hiddenEditor from '../../pages/setup/hiddenEditor';
import hiddenOverview from '../../pages/setup/hiddenOverview';
import factionRolesUL from '../../pages/setup/factionRolesUL';
import setupToolbar from '../../pages/setup/setupToolbar';
import { USER_ID2 } from '../../fixtures/fakeConstants';

const { HIDDEN_NAME } = require('../../fixtures/fakeConstants');


describe('Hidden Editor Visualizations', () => {
    const factionRole = factionRoleModel.get();
    const faction = factionModel.get({
        factionRoles: [factionRole],
    });
    function getSetupData(hiddens = []){
        return setupModel.get({
            hiddens,
            factions: [faction],
        });
    }

    context('Opening editor', () => {
        it('Will open the hidden editor', () => {
            const setup = getSetupData();
            cy.goToSetup({ setup });
            setupToolbar.setAdvancedToggle(true);

            factionRolesUL.openHiddenCreator();

            hiddenEditor.assertIsVisible();
            hiddenEditor.assertNotSpawnable(factionRole.name);
            hiddenEditor.assertListItemColor(factionRole.name, faction.color);
        });
    });

    context('Creating hidden', () => {
        beforeEach(() => {
            const setup = getSetupData();
            cy.goToSetup({ setup });
            setupToolbar.setAdvancedToggle(true);
            factionRolesUL.openHiddenCreator();
        });

        it('Will disable the create button when no text in name', () => {
            // given in beforeEach

            // when in beforeEach

            hiddenEditor.assertButtonDisabled();
        });

        it('Will create the hidden', () => {
            hiddenEditor.setName(HIDDEN_NAME);

            hiddenEditor.createHidden();

            hiddenEditor.assertNoButton();
            hiddenEditor.assertNameNotEditable();
        });

        it('Will create a hidden with a spawnable', () => {
            hiddenEditor.setName(HIDDEN_NAME);

            hiddenEditor.clickRightList(factionRole.name);

            hiddenEditor.assertSpawnable(factionRole.name);

            hiddenEditor.clickLeftList(factionRole.name);

            hiddenEditor.assertNotSpawnable(factionRole.name);

            hiddenEditor.clickRightList(factionRole.name);
            hiddenEditor.createHiddenAndSpawnable();

            hiddenEditor.assertSpawnable(factionRole.name);
        });
    });

    context('Editing faction roles', () => {
        it('Will open editor on existing hidden', () => {
            const hiddenSpawn = hiddenSpawnModel.get({
                factionRoleID: factionRole.id,
            });
            const hidden = hiddenModel.get({
                spawns: [hiddenSpawn],
            });
            const setup = getSetupData([hidden]);
            cy.goToSetup({ setup });
            setupToolbar.setAdvancedToggle(true);
            factionsUL.clickHiddens();

            hiddenOverview.openEditor();

            hiddenEditor.assertIsVisible();
            hiddenEditor.assertSpawnable(factionRole.name);
            hiddenEditor.assertListItemColor(factionRole.name, faction.color);
        });

        it('Will delete a spawn', () => {
            const hiddenSpawn = hiddenSpawnModel.get({
                factionRoleID: factionRole.id,
            });
            const hidden = hiddenModel.get({
                spawns: [hiddenSpawn],
            });
            const setup = getSetupData([hidden]);
            cy.goToSetup({ setup });
            setupToolbar.setAdvancedToggle(true);
            factionsUL.clickHiddens();
            hiddenOverview.openEditor();

            hiddenEditor.deleteSpawnable(factionRole.name);

            hiddenEditor.assertNotSpawnable(factionRole.name);
        });

        it('Will create a spawn', () => {
            const hidden = hiddenModel.get({
                spawns: [],
            });
            const setup = getSetupData([hidden]);
            cy.goToSetup({ setup });
            setupToolbar.setAdvancedToggle(true);
            factionsUL.clickHiddens();
            hiddenOverview.openEditor();

            hiddenEditor.createSpawnable(factionRole.name);

            hiddenEditor.assertSpawnable(factionRole.name);
        });

        it('Will hide the edit button for non setup owner', () => {
            const hidden = hiddenModel.get({
                spawns: [],
            });
            const setup = getSetupData([hidden]);
            cy.goToSetup({ setup }, USER_ID2);
            setupToolbar.setAdvancedToggle(true);

            factionsUL.clickHiddens();

            hiddenOverview.assertNoEditButton();
        });
    });

    context('Closing editor', () => {
        it('Will close the hidden editor', () => {
            const setup = getSetupData();
            cy.goToSetup({ setup });
            setupToolbar.setAdvancedToggle(true);
            factionRolesUL.openHiddenCreator();

            hiddenEditor.hideByEsc();

            hiddenEditor.assertHidden();
        });
    });
});
