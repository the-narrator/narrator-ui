import gameModel from '../baseModels/gameModel';
import phaseModel from '../baseModels/phaseModel';
import playerModel from '../baseModels/playerModel';
import setupModel from '../baseModels/setupModel';
import userModel from '../baseModels/userModel';

import { GAME_ID } from '../fakeConstants';


function getCreate(){
    return {
        event: 'lobbyCreate',
        modifiers: gameModel.getModifiers(),
        phase: phaseModel.getUnstarted(),
        players: [playerModel.get()],
        setup: setupModel.get(),
        users: [userModel.get({ isModerator: true })],
    };
}

function getDelete(){
    return {
        event: 'lobbyDelete',
        gameID: GAME_ID,
    };
}

module.exports = {
    getCreate,
    getDelete,
};
