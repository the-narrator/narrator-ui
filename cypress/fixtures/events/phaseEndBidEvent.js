function get(attributes = {}){
    return {
        event: 'phaseEndBid',
        players: attributes.players,
    };
}

module.exports = {
    get,
};
