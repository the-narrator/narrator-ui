function get({ endTime }){
    return {
        event: 'timerUpdate',
        endTime,
    };
}

module.exports = {
    get,
};
