function get(attributes = {}){
    return {
        event: 'voteUpdate',
        voterName: attributes.voterName,
        voteTargets: attributes.voteTargets,
    };
}

module.exports = {
    get,
};
