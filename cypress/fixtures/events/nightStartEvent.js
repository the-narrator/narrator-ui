import phaseModel from '../baseModels/phaseModel';
import profileModel from '../baseModels/profileModel';
import setupModel from '../baseModels/setupModel';
import voteInfoModel from '../baseModels/voteInfoModel';


function get(attributes = {}){
    return {
        event: 'nightStart',
        phase: phaseModel.getNight(),
        profile: attributes.profile || profileModel.getInGame(),
        setup: setupModel.getDefaultStarted(),
        voteInfo: voteInfoModel.get(),
    };
}

module.exports = {
    get,
};
