import setupHiddenModel from '../baseModels/setupHiddenModel';

import { SETUP_HIDDEN_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        event: 'setupHiddenAdd',
        setupHidden: setupHiddenModel.get({ id: attributes.setupHiddenID || SETUP_HIDDEN_ID }),
    };
}

module.exports = {
    get,
};
