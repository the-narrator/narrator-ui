function getAnonymousProfile(){
    return {
        ...getPreviousProfile(),
        isAnonymous: true,
    };
}

function getNoProfile(){
    return null;
}

function getPreviousProfile(){
    return { getIdToken: async() => '' };
}

module.exports = {
    getAnonymousProfile,
    getNoProfile,
    getPreviousProfile,
};
