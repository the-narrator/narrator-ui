import { MAX_PLAYER_COUNT } from '../../../src/js/util/constants';

import { HIDDEN_ID, SETUP_HIDDEN_ID } from '../fakeConstants';


function get(attributes = {}){
    return {
        hiddenID: HIDDEN_ID,
        id: attributes.id || SETUP_HIDDEN_ID,
        isExposed: false,
        mustSpawn: false,
        minPlayerCount: attributes.minPlayerCount || 0,
        maxPlayerCount: 'maxPlayerCount' in attributes
            ? attributes.maxPlayerCount : MAX_PLAYER_COUNT,
    };
}

module.exports = {
    get,
};
