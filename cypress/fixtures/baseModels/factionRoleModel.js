import {
    FACTION_ID,
    FACTION_ROLE_ID, FACTION_ROLE_ID2, ROLE_ID, ROLE_ID2, ROLE_NAME, ROLE_NAME2,
} from '../fakeConstants';


function get(attributes = {}){
    return {
        abilities: attributes.abilities || [],
        details: attributes.details || [],
        factionID: attributes.factionID || FACTION_ID,
        id: attributes.id || FACTION_ROLE_ID,
        modifiers: attributes.modifiers || [],
        name: attributes.name || ROLE_NAME,
        roleID: attributes.roleID || ROLE_ID,
    };
}

function get2(attributes = {}){
    attributes.name = attributes.name || ROLE_NAME2;
    attributes.id = attributes.id || FACTION_ROLE_ID2;
    attributes.roleID = attributes.roleID || ROLE_ID2;
    return get(attributes);
}

module.exports = {
    get,
    get2,
};
