import {
    ABILITY_DESCRIPTION, ABILITY_DETAIL, ABILITY_ID, ABILITY_NAME,
} from '../fakeConstants';


function get(args = {}){
    return {
        description: ABILITY_DESCRIPTION,
        details: [ABILITY_DETAIL],
        id: ABILITY_ID,
        modifiers: args.modifiers || [],
        name: ABILITY_NAME,
        setupModifierNames: [],
    };
}

module.exports = {
    get,
};
