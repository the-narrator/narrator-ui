import {
    CHAT_MESSAGE_ID, CHAT_MESSAGE_ID2,
    CHAT_NAME, CHAT_TEXT, MESSAGE_TEXT, PLAYER_NAME, USER_ID, USER_ID2,
} from '../fakeConstants';


function get(){
    return {
        chat: [CHAT_NAME],
        text: MESSAGE_TEXT,
    };
}

function getPlayerMessage(){
    return {
        chat: [CHAT_NAME],
        text: MESSAGE_TEXT,
        messageType: 'ChatMessage',
        speakerName: PLAYER_NAME,
    };
}

function getLobby(attributes = {}){
    return {
        createdAt: new Date().toString(),
        id: attributes.id || CHAT_MESSAGE_ID,
        text: CHAT_TEXT,
        userID: attributes.userID || USER_ID,
    };
}

function getLobby2(){
    return getLobby({
        id: CHAT_MESSAGE_ID2,
        userID: USER_ID2,
    });
}

module.exports = {
    get,
    getLobby,
    getLobby2,
    getPlayerMessage,
};
