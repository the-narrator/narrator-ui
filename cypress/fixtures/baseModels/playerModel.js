import {
    PLAYER_ID, PLAYER_ID2, PLAYER_ID3, PLAYER_NAME,
    PLAYER_NAME2, PLAYER_NAME3, USER_ID, USER_ID2, USER_ID3,
} from '../fakeConstants';

import graveModel from './graveModel';


function get(attributes = {}){
    return {
        endedNight: attributes.endedNight || false,
        flip: attributes.grave,
        id: attributes.id || PLAYER_ID,
        isComputer: false,
        name: attributes.name || PLAYER_NAME,
        userID: attributes.userID || USER_ID,
    };
}

function getInGame(attributes = {}){
    return {
        ...get(attributes),
        votePower: attributes.votePower || 1,
    };
}

function get2(attributes = {}){
    return get({
        name: attributes.name || PLAYER_NAME2,
        flip: attributes.flip,
        id: PLAYER_ID2,
        userID: attributes.userID || USER_ID2,
    });
}

function get3(attributes = {}){
    return get({
        ...attributes,
        id: PLAYER_ID3,
        name: PLAYER_NAME3,
        userID: USER_ID3,
    });
}

function getBot(attributes = {}){
    const player = get(attributes);
    delete player.userID;
    player.isComputer = true;
    return player;
}

function getDead(attributes = {}){
    return get({ ...attributes, grave: graveModel.get(attributes) });
}

module.exports = {
    get,
    getInGame,
    get2,
    get3,
    getBot,
    getDead,
};
