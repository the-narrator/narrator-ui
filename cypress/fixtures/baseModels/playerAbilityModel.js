function get(attributes = {}){
    return {
        command: attributes.command || 'heal',
        targets: attributes.targets || [],
    };
}

module.exports = {
    get,
};
