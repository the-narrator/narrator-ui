import { HIDDEN_ID, HIDDEN_NAME } from '../fakeConstants';

import hiddenSpawnModel from './hiddenSpawnModel';


function get(attributes = {}){
    return {
        id: attributes.id || HIDDEN_ID,
        name: attributes.name || HIDDEN_NAME,
        spawns: attributes.spawns || [hiddenSpawnModel.get()],
    };
}

module.exports = {
    get,
};
