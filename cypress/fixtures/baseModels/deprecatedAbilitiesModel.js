import { PLAYER_NAME2, PLAYER_NAME3 } from '../fakeConstants';


function get(abilityCommand, options = {}){
    return {
        [abilityCommand]: {
            canAddAction: true,
            options,
            players: [{
                playerName: PLAYER_NAME2,
            }, {
                playerName: PLAYER_NAME3,
            }],
        },
        type: [abilityCommand],
    };
}

function getEmpty(){
    return {
        type: [],
    };
}

module.exports = {
    get,
    getEmpty,
};
