import { FACTION_ID, PLAYER_NAME2 } from '../fakeConstants';


function get(attributes = {}){
    return {
        factionID: attributes.factionID || FACTION_ID,
        name: attributes.name || PLAYER_NAME2,
    };
}

module.exports = {
    get,
};
