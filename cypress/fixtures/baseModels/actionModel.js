import { ABILITY_NAME } from '../fakeConstants';


function get(attributes = {}){
    return {
        command: attributes.command || ABILITY_NAME,
        playerNames: attributes.targets || [],
        text: '',
    };
}

module.exports = {
    get,
};
