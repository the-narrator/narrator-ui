import {
    COLOR,
    COLOR2,
    FACTION_ID,
    FACTION_ID2,
    FACTION_NAME,
    FACTION_NAME2,
} from '../fakeConstants';

function get(attributes = {}){
    const id = attributes.id || FACTION_ID;
    const factionRoles = attributes.factionRoles || [];
    factionRoles.forEach(factionRole => {
        factionRole.factionID = id;
    });
    return {
        abilities: attributes.abilities || [],
        checkableIDs: attributes.checkableIDs || [],
        color: attributes.color || COLOR,
        description: '',
        details: attributes.details || [],
        enemyIDs: attributes.enemyIDs || [],
        id,
        modifiers: attributes.modifiers || [],
        name: attributes.name || FACTION_NAME,
        factionRoles,
    };
}

function get2(attributes = {}){
    attributes.color = attributes.color || COLOR2;
    attributes.id = attributes.id || FACTION_ID2;
    attributes.name = attributes.name || FACTION_NAME2;
    return get(attributes);
}

module.exports = {
    get,
    get2,
};
