module.exports = {
    ABILITY_DESCRIPTION: 'some ability description',
    ABILITY_DETAIL: 'some extra ability detail',
    ABILITY_ID: 9999,
    ABILITY_MODIFIER_NAME: 'charges',
    ABILITY_NAME: 'fakecheck',

    CHAT_NAME: 'Day 1',
    CHAT_MESSAGE_ID: 963000,
    CHAT_MESSAGE_ID2: 963002,
    CHAT_TEXT: 'some chat text',

    COLOR: '#6495ED',
    COLOR2: '#FF0000',

    FACTION_ID: 2222,
    FACTION_ID2: 6666,
    FACTION_MODIFIER_NAME: 'WIN_PRIORITY',
    FACTION_NAME: 'FakeMafia', // these need to be alphabetical for tests
    FACTION_NAME2: 'FakeTown',

    FACTION_ROLE_ID: 8888,
    FACTION_ROLE_ID2: 88188,

    LOBBY_ID: 'ABCD',

    GAME_ID: 112211,
    GAME_MODIFIER_NAME: 'GAME_SETTING_NAME',

    HIDDEN_NAME: 'FakeTown Hidden',
    HIDDEN_ID: 3333,
    HIDDEN_ID2: 4444,
    HIDDEN_SPAWN_ID: 9993331,
    HIDDEN_SPAWN_ID2: 9993332,

    MESSAGE_TEXT: 'I am sending a fake message',

    PLAYER_ICON: 'fab fa-android',
    PLAYER_ID: 9995551,
    PLAYER_ID2: 9995552,
    PLAYER_ID3: 9995553,
    PLAYER_COLOR: '#bd36f3',
    PLAYER_NAME: 'playerAlpha',
    PLAYER_NAME2: 'playerBeta',
    PLAYER_NAME3: 'playerCharlie',

    ROLE_ABILITY_MODIFIER_NAME: 'ROLE_ABILITY_MODIFIER',
    ROLE_ID: 1111,
    ROLE_ID2: 5555,
    ROLE_MODIFIER_NAME: 'ROLE_MODIFIER_NAME',
    ROLE_NAME: 'FakeCitizen',
    ROLE_NAME2: 'FakeGoon',

    SETUP_HIDDEN_ID: 777,

    SETUP_MODIFIER_NAME: 'DAY_START',
    SETUP_NAME: 'setupName',
    SETUP_ID: 444111444,
    SETUP_ID2: 444202444,
    SETUP_ID_FEATURED: 10001,

    USER_COLOR: '#ffba5c',
    USER_COLOR2: '#cbe1a6',
    USER_NAME: 'userName',
    USER_ICON: 'fas fa-eye',
    USER_ICON2: 'fas fa-fighter-jet',
    USER_ID: 1,
    USER_ID2: 2,
    USER_ID3: 3,
};
