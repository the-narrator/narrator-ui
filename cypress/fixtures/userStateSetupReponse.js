import profileModel from './baseModels/profileModel';
import setupModel from './baseModels/setupModel';

import { PLAYER_NAME, USER_ID } from './fakeConstants';


function get(attributes = {}){
    return {
        factions: {},
        gameStart: attributes.isStarted || false,
        guiUpdate: true,
        isPrivateInstance: true,
        moderatorIDs: attributes.moderatorIDs || [USER_ID],
        setup: setupModel.get(),
        type: ['rules'],
    };
}

function getStartedGame(attributes = {}){
    const response = get({
        isStarted: true,
    });
    if(!attributes.profile)
        response.profile = profileModel.getInGame({
            name: PLAYER_NAME,
        });
    return response;
}

module.exports = {
    get,
    getStartedGame,
};
