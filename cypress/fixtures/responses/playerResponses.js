import setupModel from '../baseModels/setupModel';
import { USER_ID } from '../fakeConstants';


function addBots(attributes = {}){
    return {
        response: {
            players: attributes.players,
            setup: setupModel.get(),
        },
    };
}

function kick(){
    return {
        response: {
            moderatorIDs: [USER_ID],
            setup: setupModel.get(),
        },
    };
}

module.exports = {
    addBots,
    kick,
};
