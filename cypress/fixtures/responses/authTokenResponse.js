function get(userID){
    return {
        errors: [],
        response: {
            userID,
        },
    };
}

module.exports = {
    get,
};
