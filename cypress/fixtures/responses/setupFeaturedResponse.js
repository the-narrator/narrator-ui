import setupFeaturedModel from '../baseModels/setupFeaturedModel';


function get(){
    return [setupFeaturedModel.get()];
}

module.exports = {
    get,
};
