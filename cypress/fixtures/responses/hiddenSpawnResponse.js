import hiddenSpawnModel from '../baseModels/hiddenSpawnModel';


function post(){
    return { response: [hiddenSpawnModel.get()] };
}

module.exports = {
    post,
};
