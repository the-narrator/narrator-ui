function repickHost(userID){
    return {
        response: {
            moderatorIDs: [userID],
        },
    };
}

module.exports = {
    repickHost,
};
