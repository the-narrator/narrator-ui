function clearLog() {
  $("ul").empty();
}


function displayLog(story) {
  clearLog();

  var ul = $("ul");
  var li;
  var span;
  for( var i = 0; i < story.length; i++) {
    li = $("<li>");
    li.html(story[i]);
    ul.append(li);
  }
}

function requestStory() {

  var url = "/requestStory";
  var index = window.location.href.indexOf('storyRecap/');
  var number = window.location.href.substring(index + 'storyRecap/'.length);


  var method = "POST";
  var async = true;
  var request = new XMLHttpRequest();
  request.open(method, url, async);
  request.onload = function() {
    var status = request.status;

    if(status === 404){
      alert('Story ID not found!');
      return;
    }else if(status !== 200){
      console.log(request.responseText);
      return;
    }
    var o = JSON.parse(request.responseText);
    //createVictimList(o.roles);
    //createKillerList(o.killers);
    displayLog(o.story);
  }

  var data = {'storyID': number};
  request.send(JSON.stringify(data));


}



$(document).ready(function(){
	requestStory();
});
